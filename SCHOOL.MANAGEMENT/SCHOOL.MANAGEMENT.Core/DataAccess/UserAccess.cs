﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using System.Data;
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class User Access for Data Access Layer
    /// </summary>
    public class UserAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccess"/> class.
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public UserAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccess"/> class.
        /// </summary>
        public UserAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new user
        /// </summary>
        /// <param name="userModel">Model object for user</param>
        /// <returns>returns the details of the added user</returns>
        public DataTable AddUser(UserModel userModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USERNAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.UserName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.Password
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_FIRST_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.FirstName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MIDDLE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.MiddleName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_LAST_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.LastName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userModel.IsActive
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_CREATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.CreatedBy
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_EMPLOYEE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.EmployeeId
                });
                return this._cmd.GetDataTable("ADD_USER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified user
        /// </summary>
        /// <param name="userModel">Model object for user</param>
        /// <returns>returns the details of the updated user</returns>
        public DataTable ModifyUser(UserModel userModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userModel.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USERNAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.UserName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.Password
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_FIRST_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.FirstName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MIDDLE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.MiddleName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_LAST_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.LastName
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userModel.IsActive
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_UPDATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_EMPLOYEE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.EmployeeId
                });
                return this._cmd.GetDataTable("MODIFY_USER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified user's password
        /// </summary>
        /// <param name="password">new password of the user</param>
        /// <param name="oldPassword">old password of the user</param>
        /// <returns>returns the details of the updated user</returns>
        public DataTable ModifyUserPassword(string password, string oldPassword)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_OLD_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = oldPassword
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = password
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("MODIFY_USER_PASSWORD", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user
        /// </summary>
        /// <param name="userId">id of the specified user</param>
        /// <returns>returns a data table containing the details of the specified user</returns>
        public DataTable GetUser(int userId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userId
                });
                return this._cmd.GetDataTable("GET_USER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user details
        /// </summary>
        /// <returns>returns a data table containing the details of all users found on the database</returns>
        public DataTable GetAllUser()
        {
            try
            {
                return this._cmd.GetDataTable("GET_ALL_USER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed modules
        /// </summary>
        /// <param name="userId">ID of the user</param>
        /// <returns>returns a data table containing the details of all allowed modules of the user</returns>
        public DataTable GetUserModules(int userId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = 0
                });
                return this._cmd.GetDataTable("GET_USER_MODULES", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets list of users under specified role
        /// </summary>
        /// <param name="role">role name</param>
        /// <returns>List of users under specified role</returns>
        public DataTable GetUsersByRole(string role)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = role
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int64,
                    Value = this._connect.OrganizationId
                });
                return this._cmd.GetDataTable("GET_USERS_BY_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user details
        /// </summary>
        /// <returns>returns a data table containing the details of all users found on the database</returns>
        public DataTable GetAPIAllUsers(int orgHrchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHrchyId
                });
                return this._cmd.GetDataTable("GET_API_USER_CREDENTIALS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user details
        /// </summary>
        /// <returns>returns a data table containing the details of all users found on the database</returns>
        public DataTable GetAPIAllUsers()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_API_USER_CREDENTIALS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}