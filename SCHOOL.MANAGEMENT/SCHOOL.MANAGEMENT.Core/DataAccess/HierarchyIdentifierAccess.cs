﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Hierarchy Identifier Access for Data Access Layer
    /// </summary>
    public class HierarchyIdentifierAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyIdentifierAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public HierarchyIdentifierAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyIdentifierAccess"/> class
        /// </summary>
        public HierarchyIdentifierAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierDesc">description for the new hierarchy identifier</param>
        /// <returns>returns the details of the hierarchy identifier added</returns>
        public DataTable AddHierarchyIdentifier(string hierarchyIdentifierDesc)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = hierarchyIdentifierDesc
                });
                return this._cmd.GetDataTable("ADD_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId">id of the selected hierarchy identifier</param>
        /// <param name="description">description of the hierarchy identifier</param>
        /// <returns>returns the details of the modified hierarchy identifier</returns>
        public DataTable ModifyHierarchyIdentifier(int hierarchyIdentifierId, string description)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_HRCHY_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = hierarchyIdentifierId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = description
                });
                return this._cmd.GetDataTable("MODIFY_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId">id of the hierarchy identifier</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveHierarchyIdentifier(int hierarchyIdentifierId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_HRCHY_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = hierarchyIdentifierId
                });
                return this._cmd.GetValue<int>("REMOVE_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId">id of the specified hierarchy identifier</param>
        /// <returns>returns a data table containing the details of the specified hierarchy identifier</returns>
        public DataTable GetHierarchyIdentifier(int hierarchyIdentifierId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_HRCHY_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = hierarchyIdentifierId
                });
                return this._cmd.GetDataTable("GET_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all hierarchy identifier
        /// </summary>
        /// <returns>returns a data table containing the details of all hierarchy identifier found on the database</returns>
        public DataTable GetAllHierarchyIdentifier()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all hierarchy identifier
        /// </summary>
        /// <returns>returns a data table containing the details of all hierarchy identifier found on the database</returns>
        public DataTable GetAllAPIHierarchyIdentifier()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_API_HRCHY_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}