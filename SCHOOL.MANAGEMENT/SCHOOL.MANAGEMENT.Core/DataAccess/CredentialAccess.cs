﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Credential Access for Data Access Layer
    /// </summary>
    public class CredentialAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Initializes a new instance of the <see cref="CredentialAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public CredentialAccess(ConnectionModel credits)
        {
            this._cmd = new Core.DatabaseConnection().Initialize(credits);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CredentialAccess"/> class
        /// </summary>
        public CredentialAccess()
        {
            this._cmd = new Core.DatabaseConnection().BaseInitialize();
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user
        /// </summary>
        /// <param name="username">username of the user</param>
        /// <param name="password">password of the user</param>
        /// <returns>returns a data table containing the details of the specified user</returns>
        public DataTable GetUserByCredentials(string username, string password)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USERNAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = username
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = password
                });
                return this._cmd.GetDataTable("GET_USER_BY_CREDENTIAL", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._cmd.GenericConnection.Instance.Dispose();
                this._cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets the Database Date
        /// </summary>
        /// <returns>Returns the Database Date</returns>
        public DataTable GetDatabaseDate()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_DATABASE_DATE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._cmd.GenericConnection.Instance.Dispose();
                this._cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed modules
        /// </summary>
        /// <param name="userId">ID of the user</param>
        /// <returns>returns a data table containing the details of all allowed modules of the user</returns>
        public DataTable GetUserModules(int userId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userId
                });
                return this._cmd.GetDataTable("GET_USER_MODULES", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._cmd.GenericConnection.Instance.Dispose();
                this._cmd.GenericConnection = null;
            }
        }
    }
}