﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class User Access Rights for Data Access Layer
    /// </summary>
    public class UserRightAccess
    {
        /// <summary>
        /// Command MySql Instance
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// mysql connection Instance
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public UserRightAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightAccess"/> class
        /// </summary>
        /// <param name="username">Username of user</param>
        /// <param name="password">Password of user</param>
        public UserRightAccess(string username, string password)
        {
            this._connect = new Core.DatabaseConnection(username, password);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightAccess"/> class
        /// </summary>
        public UserRightAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new user access
        /// </summary>
        /// <param name="modelUserAccess">Id of the creator</param>
        /// <returns>returns the details of the added user access</returns>
        public DataTable AddUserAccess(UserAccessModel modelUserAccess)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.RoleId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.OrgId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_CREATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("ADD_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified user access
        /// </summary>
        /// <param name="modelUserAccess">User Access Model</param>
        /// <returns>returns the details of the added user access</returns>
        public DataTable ModifyUserAccess(UserAccessModel modelUserAccess)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.UserAccessId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.RoleId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelUserAccess.OrgId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_UPDATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("MODIFY_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified user access
        /// </summary>
        /// <param name="userAccessId">id of the selected user access</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveUserAccess(int userAccessId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userAccessId
                });
                return this._cmd.GetValue<int>("REMOVE_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user access
        /// </summary>
        /// <returns>returns a data table containing the details of all user access found on the database</returns>
        public DataTable GetAllUserAccess()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user access
        /// </summary>
        /// <param name="userAccessId">id of the specified user access</param>
        /// <returns>returns a data table containing the details of the specified user access</returns>
        public DataTable GetUserAccess(int userAccessId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userAccessId
                });
                return this._cmd.GetDataTable("GET_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets Lists of Organization Access
        /// </summary>
        /// <returns>Lists of Organization Access</returns>
        public DataTable GetOrganizationAccess()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("GET_ORG_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed modules
        /// </summary>
        /// <returns>returns a data table containing the details of all allowed modules of the user</returns>
        public DataTable GetUserModules()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.OrganizationId
                });

                return this._cmd.GetDataTable("GET_USER_MODULES", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed module actions
        /// </summary>
        /// <param name="modulePage">module page</param>
        /// <returns>returns a data table containing the details of all allowed modules of the user</returns>
        public DataTable GetUserModuleActions(string modulePage)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.OrganizationId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_PAGE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = modulePage
                });
                return this._cmd.GetDataTable("GET_USER_MODULE_ACTIONS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed hidden modules
        /// </summary>
        /// <param name="userModel">User Model</param>
        /// <returns>returns a data table containing the details of all allowed actions in every module of the user</returns>
        public DataTable GetUsersModuleAccess(UserModel userModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USERNAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.UserName
                });

                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PASSWORD",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = userModel.Password
                });

                return this._cmd.GetDataTable("GET_USERS_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets all user access
        /// </summary>
        /// <returns>returns a data table containing the specific details of all user access found on the database</returns>
        public DataTable GetApiUserAccess(int orgHrchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHrchyId
                });
                return this._cmd.GetDataTable("GET_API_USER_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}