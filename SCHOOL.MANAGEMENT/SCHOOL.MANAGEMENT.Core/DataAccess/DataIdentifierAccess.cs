﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Data Identifier Access for Data Access Layer
    /// </summary>
    public class DataIdentifierAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataIdentifierAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public DataIdentifierAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataIdentifierAccess"/> class
        /// </summary>
        public DataIdentifierAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new data identifier
        /// </summary>
        /// <param name="dataIdentifierDesc">description for the new data identifier</param>
        /// <returns>returns the details of the data identifier added</returns>
        public DataTable AddDataIdentifier(string dataIdentifierDesc)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = dataIdentifierDesc
                });
                return this._cmd.GetDataTable("ADD_DATA_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified data identifier
        /// </summary>
        /// <param name="dataIdentifierId">id of the selected data identifier</param>
        /// <param name="description">description of the data identifier</param>
        /// <returns>returns the details of the modified data identifier</returns>
        public DataTable ModifyDataIdentifier(int dataIdentifierId, string description)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataIdentifierId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = description
                });
                return this._cmd.GetDataTable("MODIFY_DATA_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified data identifier
        /// </summary>
        /// <param name="dataIdentifierId">id of the data identifier</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveDataIdentifier(int dataIdentifierId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataIdentifierId
                });
                return this._cmd.GetValue<int>("REMOVE_DATA_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data identifier
        /// </summary>
        /// <param name="dataIdentifierId">id of the specified data identifier</param>
        /// <returns>returns a data table containing the details of the specified data identifier</returns>
        public DataTable GetDataIdentifier(int dataIdentifierId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataIdentifierId
                });
                return this._cmd.GetDataTable("GET_DATA_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data identifier
        /// </summary>
        /// <returns>returns a data table containing the details of all data identifier found on the database</returns>
        public DataTable GetAllDataIdentifier()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_DATA_IDENTIFIER", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}