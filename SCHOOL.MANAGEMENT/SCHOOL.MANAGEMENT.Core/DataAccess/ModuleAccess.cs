﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Module Access for Data Access Layer
    /// </summary>
    public class ModuleAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public ModuleAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleAccess"/> class
        /// </summary>
        public ModuleAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new module
        /// </summary>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the action added</returns>
        public DataTable AddModule(ModuleModel moduleModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Name
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Description
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_PAGE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.ModulePage
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.IsActive
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_SHOWN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.IsShown
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_SORT_NO",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.SortNo
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ICON",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Icon
                });
                return this._cmd.GetDataTable("ADD_MODULE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified module
        /// </summary>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the modified module</returns>
        public DataTable ModifyModule(ModuleModel moduleModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Name
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DESCRIPTION",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Description
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_PAGE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.ModulePage
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.IsActive
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_SHOWN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.IsShown
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_SORT_NO",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleModel.SortNo
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ICON",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = moduleModel.Icon
                });
                return this._cmd.GetDataTable("MODIFY_MODULE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified module
        /// </summary>
        /// <param name="moduleId">id of the module</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveModule(int moduleId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleId
                });
                return this._cmd.GetValue<int>("REMOVE_MODULE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module
        /// </summary>
        /// <param name="moduleId">id of the specified module</param>
        /// <returns>returns a data table containing the details of the specified module</returns>
        public DataTable GetModule(int moduleId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleId
                });
                return this._cmd.GetDataTable("GET_MODULE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all parent modules
        /// </summary>
        /// <returns>returns a data table containing the details of all parent modules found on the database</returns>
        public DataTable GetParentModules()
        {
            try
            {
                return this._cmd.GetDataTable("GET_PARENT_MODULES", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all modules
        /// </summary>
        /// <returns>returns a data table containing the details of all modules found on the database</returns>
        public DataTable GetAllModules()
        {
            try
            {
                return this._cmd.GetDataTable("GET_MODULES", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}