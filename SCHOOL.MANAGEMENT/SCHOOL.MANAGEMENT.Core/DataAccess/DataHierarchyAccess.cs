﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Data Hierarchy Access for Data Access Layer
    /// </summary>
    public class DataHierarchyAccess
    {
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataHierarchyAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public DataHierarchyAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataHierarchyAccess"/> class
        /// </summary>
        public DataHierarchyAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new data hierarchy
        /// </summary>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the data hierarchy added</returns>
        public DataTable AddDataHierarchy(DataHierarchyModel dataHierarchyModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_REFERENCE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.ReferenceId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.DataIdentifierId
                });
                return this._cmd.GetDataTable("ADD_DATA_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified data hierarchy
        /// </summary>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the modified data hierarchy</returns>
        public DataTable ModifyDataHierarchy(DataHierarchyModel dataHierarchyModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyModel.DataIdentifierId
                });
                return this._cmd.GetDataTable("MODIFY_DATA_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified data hierarchy
        /// </summary>
        /// <param name="dataHierarchyId">id of the data hierarchy</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveDataHierarchy(int dataHierarchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyId
                });
                return this._cmd.GetValue<int>("REMOVE_DATA_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data hierarchy
        /// </summary>
        /// <param name="dataHierarchyId">id of the specified data hierarchy</param>
        /// <returns>returns a data table containing the details of the specified data hierarchy</returns>
        public DataTable GetDataHierarchy(int? dataHierarchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataHierarchyId
                });
                return this._cmd.GetDataTable("GET_DATA_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data hierarchy
        /// </summary>
        /// <returns>returns a data table containing the details of all data hierarchy found on the database</returns>
        public DataTable GetAllDataHierarchy()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_DATA_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
    }
}