namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using Oracle.DataAccess.Types;
    using System.Configuration;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Component Access Class
    /// </summary>
    internal class ComponentAccess
    {
        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        internal ComponentAccess(ConnectionModel credits)
        {
            _connect = new Core.DatabaseConnection(credits);
            _cmd = _connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentAccess"/> class
        /// </summary>
        /// <param name="username">Username provided</param>
        /// <param name="password">Password provided</param>
        internal ComponentAccess(string username, string password)
        {
            _connect = new Core.DatabaseConnection(username, password);
            _cmd = _connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentAccess"/> class
        /// </summary>
        internal ComponentAccess()
        {
            _connect = new Core.DatabaseConnection();
            _cmd = _connect.Cmd;
        }

        /// <summary>
        /// Adds new component
        /// </summary>
        /// <param name="component">Component model</param>
        /// <returns>Retruns data added</returns>
        public DataTable AddComponent(ComponentModel component)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentName
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_DESC",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentDesc
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ICON",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentIcon
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_SHOWN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int16,
                    Value = component.IsShown
                });

                return _cmd.GetDataTable("ADD_COMPONENT", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Modifies component details
        /// </summary>
        /// <param name="component">Component Model</param>
        /// <returns>Returns modified data</returns>
        public DataTable ModifyComponent(ComponentModel component)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = component.ComponentId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentName
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_DESC",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentDesc
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ICON",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = component.ComponentIcon
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_SHOWN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int16,
                    Value = component.IsShown
                });

                return _cmd.GetDataTable("MODIFY_COMPONENT", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets Components
        /// </summary>
        /// <param name="componentId">Component Id</param>
        /// <param name="isShown">Is component shown</param>
        /// <returns>Returns fetch components</returns>
        public DataTable GetComponent(int componentId, int isShown)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = componentId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_SHOWN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int16,
                    Value = isShown
                });

                return _cmd.GetDataTable("GET_COMPONENT", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Adds new component data
        /// </summary>
        /// <param name="component">Component model list</param>
        /// <returns>Returns added component data</returns>
        public DataTable AddComponentData(object componentDataList)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_DATA_LIST",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.JSON,
                    Value = componentDataList
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_CREATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = _connect.UserId
                });

                return _cmd.GetDataTable("ADD_COMPONENT_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Modifies component data details
        /// </summary>
        /// <param name="component">Component model list</param>
        /// <returns>Returns modified component data</returns>
        public DataTable ModifyComponentData(string componentDataList)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_DATA_LIST",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.JSON,
                    Value = componentDataList
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_CREATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = _connect.UserId
                });

                return _cmd.GetDataTable("MODIFY_COMPONENT_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets Component data base on user organization hierarchy
        /// </summary>
        /// <param name="dataKeyId">Data Primary Key Id</param>
        /// <param name="componentId">Component Id</param>
        /// <returns>Returns fetch component data</returns>
        public DataTable GetComponentDataByUser(int dataKeyId, int componentId)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = componentId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_PRIMARY_KEY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataKeyId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_REF_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_IDN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = 1
                });

                return _cmd.GetDataTable("GET_COMPONENT_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets Component data base on organization hierarchy
        /// </summary>
        /// <param name="componentId">Components Identifier</param>
        /// <param name="orgHrchyId">organization hierarchy id</param>
        /// <returns>Returns fetch component data</returns>
        public DataTable GetComponentDataByOrg(int componentId, int orgHrchyId)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = componentId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_PRIMARY_KEY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = 0
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_REF_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHrchyId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_IDN",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = 0
                });

                return _cmd.GetDataTable("GET_COMPONENT_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Gets specific data
        /// </summary>
        /// <param name="dataKeyId">Data Primary Key Id</param>
        /// <param name="componentId">Component Id</param>
        /// <returns>Returns fetch specific data</returns>
        public DataTable GetSpecificData(int dataKeyId, int componentId)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_PRIMARY_KEY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataKeyId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = componentId
                });

                return _cmd.GetDataTable("GET_SPECIFIC_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Get Foreign Local Component Data
        /// </summary>
        /// <param name="componentId">Component Id</param>
        /// <param name="dataPrimaryKey">Data Primary Key</param>
        /// <returns>returns details of Foreign Local Component Data</returns>
        public DataTable GetForeignLocalData(int componentId, int dataPrimaryKey)
        {
            try
            {
                _cmd.GenericParameters.Clear();
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_COMPONENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = componentId
                });
                _cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_DATA_PRIMARYKEY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = dataPrimaryKey
                });

                return _cmd.GetDataTable("GET_FOREIGN_LOCAL_DATA", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                _connect.Cmd.GenericConnection.Instance.Dispose();
                _connect.Cmd.GenericConnection = null;
            }
        }
    }
}
