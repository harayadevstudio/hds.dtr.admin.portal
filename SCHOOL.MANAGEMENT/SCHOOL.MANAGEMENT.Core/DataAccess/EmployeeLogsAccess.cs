﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using MySql.Data.MySqlClient;
    using System.Data;
    public class EmployeeLogsAccess
    {
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public EmployeeLogsAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionAccess"/> class
        /// </summary>
        public EmployeeLogsAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        public DataTable GetEmployeeCount()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                //this._cmd.GenericParameters.Add(new MySqlParameter
                //{
                //    ParameterName = "I_ACTION_ID",
                //    Direction = ParameterDirection.Input,
                //    MySqlDbType = MySqlDbType.Int32,
                //    Value = actionId
                //});
                return this._cmd.GetDataTable("GET_EMPLOYEE_COUNT", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}
