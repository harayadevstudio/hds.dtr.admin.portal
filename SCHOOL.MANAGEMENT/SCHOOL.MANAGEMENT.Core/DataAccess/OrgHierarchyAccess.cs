﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Organization Hierarchy Access for Data Access Layer
    /// </summary>
    public class OrgHierarchyAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrgHierarchyAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public OrgHierarchyAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrgHierarchyAccess"/> class
        /// </summary>
        public OrgHierarchyAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the organization hierarchy added</returns>
        public DataTable AddOrgHierarchy(OrgHierarchyModel orgHierarchyModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_REFERENCE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.ReferenceId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_HRCHY_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.HierarchyIdentifierId
                });
                return this._cmd.GetDataTable("ADD_ORG_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the modified organization hierarchy</returns>
        public DataTable ModifyOrgHierarchy(OrgHierarchyModel orgHierarchyModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_PARENT_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.ParentId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_REFERENCE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.ReferenceId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_HRCHY_IDN_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyModel.HierarchyIdentifierId
                });
                return this._cmd.GetDataTable("MODIFY_ORG_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyId">id of the organization hierarchy</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveOrgHierarchy(int orgHierarchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyId
                });
                return this._cmd.GetValue<int>("REMOVE_ORG_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyId">id of the specified organization hierarchy</param>
        /// <returns>returns a data table containing the details of the specified organization hierarchy</returns>
        public DataTable GetOrgHierarchy(int orgHierarchyId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ORG_HRCHY_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = orgHierarchyId
                });
                return this._cmd.GetDataTable("GET_ORG_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all organization hierarchy
        /// </summary>
        /// <returns>returns a data table containing the details of all organization hierarchy found on the database</returns>
        public DataTable GetAllOrgHierarchy()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_ORG_HIERARCHY", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}