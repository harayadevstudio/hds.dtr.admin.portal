﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Role Right Access for Data Access Layer
    /// </summary>
    public class RoleRightAccess
    {
        /// <summary>
        /// Command MySql Instance
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// oracle Connection Instance
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRightAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public RoleRightAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRightAccess"/> class
        /// </summary>
        public RoleRightAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new module access
        /// </summary>
        /// <param name="moduleAccessModel">Model object for module access</param>
        /// <returns>returns the details of the added role access</returns>
        public DataTable AddModuleAccess(ModuleAccessModel moduleAccessModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleAccessModel.ModuleActionId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleAccessModel.UserRoleAccessId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_CREATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("ADD_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified module access
        /// </summary>
        /// <param name="modelModuleAccess">Model object for module access</param>
        /// <returns>returns the details of the modified role access</returns>
        public DataTable ModifyModuleAccess(ModuleAccessModel modelModuleAccess)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelModuleAccess.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelModuleAccess.ModuleActionId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = modelModuleAccess.UserRoleAccessId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_UPDATED_BY",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                return this._cmd.GetDataTable("MODIFY_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified module access
        /// </summary>
        /// <param name="moduleAccessId">id of the selected module access</param>
        /// <returns>status 1 if successful and null if not</returns>
        public DataTable RemoveModuleAccess(int moduleAccessId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleAccessId
                });
                return this._cmd.GetDataTable("REMOVE_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role access
        /// </summary>
        /// <param name="moduleAccessId">id of the specified module access</param>
        /// <returns>returns a data table containing the details of the specified role access</returns>
        public DataTable GetModuleAccess(int moduleAccessId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACCESS_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleAccessId
                });
                return this._cmd.GetDataTable("GET_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all role access
        /// </summary>
        /// <returns>returns a data table containing the details of all role access found on the database</returns>
        public DataTable GetAllModuleAccess()
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                return this._cmd.GetDataTable("GET_ALL_MODULE_ACCESS", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}