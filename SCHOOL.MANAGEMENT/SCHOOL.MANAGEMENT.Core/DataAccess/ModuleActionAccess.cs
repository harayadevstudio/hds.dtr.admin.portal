﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class Module Action Access for Data Access Layer
    /// </summary>
    public class ModuleActionAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleActionAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public ModuleActionAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleActionAccess"/> class
        /// </summary>
        public ModuleActionAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new module action
        /// </summary>
        /// <param name="moduleId">Id of the selected module</param>
        /// <param name="actionId">Id of the selected action</param>
        /// <param name="userId">Id of the creator</param>
        /// <returns>returns the details of the added module action</returns>
        public DataTable AddModuleAction(int moduleId, int actionId, int userId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = actionId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userId
                });
                return this._cmd.GetDataTable("ADD_MODULE_ACTION", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified module action
        /// </summary>
        /// <param name="moduleActionId">id of the module action</param>
        /// <param name="moduleId">id of the selected module</param>
        /// <param name="actionId">id of the selected action</param>
        /// <returns>returns the details of the modified module action</returns>
        public DataTable ModifyModuleAction(int moduleActionId, int moduleId, int actionId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleActionId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = actionId
                });
                return this._cmd.GetDataTable("MODIFY_MODULE_ACTION", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified module action
        /// </summary>
        /// <param name="moduleActionId">id of the module action</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveModuleAction(int moduleActionId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleActionId
                });
                return this._cmd.GetValue<int>("REMOVE_MODULE_ACTION", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module action
        /// </summary>
        /// <param name="moduleActionId">id of the specified module action</param>
        /// <returns>returns a data table containing the details of the specified module action</returns>
        public DataTable GetModuleAction(int moduleActionId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_MODULE_ACTION_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = moduleActionId
                });
                return this._cmd.GetDataTable("GET_MODULE_ACTION", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all module actions
        /// </summary>
        /// <returns>returns a data table containing the details of all module actions found on the database</returns>
        public DataTable GetAllModuleActions()
        {
            try
            {
                return this._cmd.GetDataTable("GET_ALL_MODULE_ACTION", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}