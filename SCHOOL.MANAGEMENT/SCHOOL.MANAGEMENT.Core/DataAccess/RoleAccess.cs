﻿namespace SCHOOL.MANAGEMENT.Core.DataAccess
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Oracle.DataAccess.Client;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Class User Role Access for Data Access Layer
    /// </summary>
    public class RoleAccess
    {
        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        /// <summary>
        /// Instance of the MySql Connection Class
        /// </summary>
        private Core.DatabaseConnection _connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleAccess"/> class
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        public RoleAccess(ConnectionModel credits)
        {
            this._connect = new Core.DatabaseConnection(credits);
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleAccess"/> class
        /// </summary>
        public RoleAccess()
        {
            this._connect = new Core.DatabaseConnection();
            this._cmd = this._connect.Cmd;
        }

        /// <summary>
        /// Adds new user role
        /// </summary>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the added user role</returns>
        public DataTable AddUserRole(RoleModel roleModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_DESC",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = roleModel.Description
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = roleModel.Name
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = roleModel.IsActive
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = roleModel.CreatedBy
                });
                return this._cmd.GetDataTable("ADD_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Updates the specified user role
        /// </summary>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the modified user role</returns>
        public DataTable ModifyUserRole(RoleModel roleModel)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = roleModel.Id
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_NAME",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = roleModel.Name
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_DESC",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.VarChar,
                    Value = roleModel.Description
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_USER_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = this._connect.UserId
                });
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_IS_ACTIVE",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = roleModel.IsActive
                });
                return this._cmd.GetDataTable("MODIFY_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Removes the specified role
        /// </summary>
        /// <param name="userRoleId">id of the selected role</param>
        /// <returns>status 1 if successful and null if not</returns>
        public int RemoveUserRole(int userRoleId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userRoleId
                });
                return this._cmd.GetValue<int>("REMOVE_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role
        /// </summary>
        /// <param name="userRoleId">id of the specified role</param>
        /// <returns>returns a data table containing the details of the specified role</returns>
        public DataTable GetUserRole(int userRoleId)
        {
            try
            {
                this._cmd.GenericParameters.Clear();
                this._cmd.GenericParameters.Add(new MySqlParameter
                {
                    ParameterName = "I_ROLE_ID",
                    Direction = ParameterDirection.Input,
                    MySqlDbType = MySqlDbType.Int32,
                    Value = userRoleId
                });
                return this._cmd.GetDataTable("GET_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all user roles
        /// </summary>
        /// <returns>returns a data table containing the details of all user roles found on the database</returns>
        public DataTable GetAllUserRoles()
        {
            try
            {
                return this._cmd.GetDataTable("GET_ALL_ROLE", CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
            finally
            {
                this._connect.Cmd.GenericConnection.Instance.Dispose();
                this._connect.Cmd.GenericConnection = null;
            }
        }
    }
}