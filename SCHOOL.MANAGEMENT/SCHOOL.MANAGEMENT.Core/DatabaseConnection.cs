﻿namespace SCHOOL.MANAGEMENT.Core
{
    using System;
    using System.Configuration;
    using System.Security.Cryptography;
    using System.Text;
    using SCHOOL.MANAGEMENT.Core.Model;
    using KCC.MIS.Core.ConHelper.Generic;
    using Newtonsoft.Json;
    using Oracle.DataAccess.Client;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Oracle Connection for Credential Access
    /// </summary>
    public class DatabaseConnection
    {
        /// <summary>
        /// Wrapper Class Get Connection String
        /// </summary>
        private GetConnectionString _config = new GetConnectionString();

        /// <summary>
        /// Initialization of Connection Model
        /// </summary>
        private ConnectionModel _connectionModel = new ConnectionModel();

        /// <summary>
        /// Instance of the Command Oracle
        /// </summary>
        private GenericDataAccess<MySqlConnection,
             MySqlCommand,
             MySqlParameter,
             MySqlDataReader,
             MySqlTransaction> _cmdOracle;

        /// <summary>
        /// Instance of the Command MySql
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> _cmd;

        #region "Initialize SQL Connection"

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseConnection"/> class
        /// </summary>
        public DatabaseConnection()
        {
            this.Cmd = this.BaseInitialize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseConnection"/> class
        /// </summary>
        /// <param name="username">Username Credential of the User</param>
        /// <param name="password">Password Credential of the User</param>
        public DatabaseConnection(string username, string password)
        {
            this._connectionModel.Username = username;
            this._connectionModel.Password = password;

            this.Cmd = this.Initialize(this._connectionModel);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseConnection"/> class
        /// </summary>
        /// <param name="credits">Connection Model of the User Details[UserId, Username, Password]</param>
        public DatabaseConnection(ConnectionModel credits)
        {
            string key = string.Empty;
            key = this.Decrypt(credits.Key);

            ConnectionModel connectModel = JsonConvert.DeserializeObject<ConnectionModel>(key);
            credits.UserId = connectModel.UserId;
            credits.Username = connectModel.Username;
            credits.OrganizationId = connectModel.OrganizationId;
            credits.Password = this.Decrypt(connectModel.DBPassword);

            this.UserId = credits.UserId;
            this.UserName = credits.Username;
            this.OrganizationId = credits.OrganizationId;
            this.Password = credits.Password;
            this.Cmd = this.Initialize(credits);
        }

        #endregion "Initialize Oracle Connection"

        /// <summary>
        /// Gets or sets ID for User Credentials
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets Organization Id
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Gets or sets Username for User Credentials
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets Password for User Credentials
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets Oracle Command Connection
        /// </summary>
        private GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> CmdOracle
        {
            get
            {
                if (this._cmdOracle == null)
                {
                    this._cmdOracle = new GenericDataAccess<MySqlConnection, MySqlCommand, MySqlParameter, MySqlDataReader, MySqlTransaction>();
                }

                return this._cmdOracle;
            }

            set
            {
                this._cmdOracle = value;
            }
        }

        /// <summary>
        /// Gets or sets MySql Command Connection
        /// </summary>
        public GenericDataAccess<MySqlConnection,
            MySqlCommand,
            MySqlParameter,
            MySqlDataReader,
            MySqlTransaction> Cmd
        {
            get
            {
                if (this._cmd == null)
                {
                    this._cmd = new GenericDataAccess<MySqlConnection, MySqlCommand, MySqlParameter, MySqlDataReader, MySqlTransaction>();
                }

                return this._cmd;
            }

            set
            {
                this._cmd = value;
            }
        }

        #region "User Credentials"

        /// <summary>
        /// Gets the User Credentials using the Encrypted Key
        /// </summary>
        /// <param name="credits">Connection Model of the User Details[UserId, Username, Password]</param>
        /// <returns>Returns Connection Model Credential Detials</returns>
        public ConnectionModel GetKeyDetails(ConnectionModel credits)
        {
            string key = string.Empty;
            key = this.Decrypt(credits.Key);

            ConnectionModel connectModel = JsonConvert.DeserializeObject<ConnectionModel>(key);
            credits.UserId = connectModel.UserId;
            credits.Username = connectModel.Username;
            credits.Password = this.Decrypt(connectModel.DBPassword);

            return credits;
        }

        #endregion "User Credentials"

        #region "Initialize and Base Initialize"

        /// <summary>
        /// Initialized Connection in Command MariaDB
        /// </summary>
        /// <param name="connection">Connection Model Details[Username and Password]</param>
        /// <returns>Returns Command MariaDB for Connection</returns>
        public dynamic Initialize(ConnectionModel connection)
        {
            ConnectionModel.Schema = ConfigurationManager.AppSettings["SchemaName"];
            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];

            if (this.Cmd.GenericConnection == null)
            {
                this.Cmd.GenericConnection = new ConnectionGeneric<MySqlConnection>(this._config.WebConfigConnectionString(username, password));
            }

            return this.Cmd;
        }

        /// <summary>
        /// Base Initialization of Command MariaDB
        /// </summary>
        /// <returns>Returns Command MariaDB for Connection</returns>
        public dynamic BaseInitialize()
        {
            ConnectionModel.Schema = ConfigurationManager.AppSettings["SchemaName"];

            if (this.Cmd.GenericConnection == null)
            {
                this.Cmd.GenericConnection = new ConnectionGeneric<MySqlConnection>(this._config.WebConfigConnectionString(0));
            }

            return this.Cmd;
        }

        /// <summary>
        /// Base initializes Command Oracle
        /// </summary>
        /// <returns>Returns Command MariaDB for Connection</returns>
        public dynamic OracleBaseInitialize()
        {
            if (this.CmdOracle.GenericConnection == null)
            {
                this.CmdOracle.GenericConnection = new ConnectionGeneric<MySqlConnection>(this._config.WebConfigConnectionString(1));
            }

            return this.CmdOracle;
        }

        #endregion "Initialize and Base Initialize"

        #region "Encrypt and Decrypt"

        /// <summary>
        /// Encrypt JSON Object String
        /// Modify WebConfig add the following: AppName Setting and Assignned Value
        /// </summary>
        /// <param name="toEncrypt">String to Encrypt</param>
        /// <returns>Returns Encrypted text based on Application Name</returns>
        public string Encrypt(string toEncrypt)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            string key = (string)settingsReader.GetValue("AppName", typeof(string));

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cryptoTransfrom = tdes.CreateEncryptor();
            byte[] resultArray = cryptoTransfrom.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Decrypt Data based on Application Configuration Name
        /// </summary>
        /// <param name="cipherString">Encrypted String Values</param>
        /// <returns>Returns Non-Encrypted String</returns>
        public string Decrypt(string cipherString)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            string key = (string)settingsReader.GetValue("AppName", typeof(string));
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider desEncryp = new TripleDESCryptoServiceProvider();
            desEncryp.Key = keyArray;
            desEncryp.Mode = CipherMode.ECB;
            desEncryp.Padding = PaddingMode.PKCS7;

            ICryptoTransform cryptoTransform = desEncryp.CreateDecryptor();
            byte[] resultArray = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            desEncryp.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        #endregion "Encrypt and Decrypt"

        #region "Class Wrapper get connection string"

        /// <summary>
        /// Get Connection String in Web Configuration
        /// </summary>
        private class GetConnectionString
        {
            /// <summary>
            /// Web Config Connection String
            /// </summary>
            /// <returns>Database Connection String</returns>
            public string WebConfigConnectionString(int sqlInd)
            {
                string connection = string.Empty;

                if (sqlInd == 0)
                {
                    connection = ConfigurationManager.ConnectionStrings["DatabaseMySql"].ConnectionString;
                }
                else
                {
                    connection = ConfigurationManager.ConnectionStrings["DatabaseOracle"].ConnectionString;
                }

                return connection;
            }

            /// <summary>
            /// Web Config Connection String
            /// </summary>
            /// <param name="username">Schema Database Username</param>
            /// <param name="password">Schema Database Password</param>
            /// <returns>Database Connection String</returns>
            public string WebConfigConnectionString(string username, string password)
            {
                string connection = ConfigurationManager.ConnectionStrings["DatabaseMySql"].ConnectionString;
                return string.Format(connection, username, password);
                //return connection.Substring(0, 131) + "User Id=" + username + ";Password=" + password + ";";
            }
        }

        #endregion "Class Wrapper get connection string"
    }
}