﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Module Class
    /// </summary>
    public class Module
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new module
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the action added</returns>
        public ResponseModel AddModule(ConnectionModel credits, ModuleModel moduleModel)
        {
            DataTable dataModule = new DataTable();
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                dataModule = moduleAccess.AddModule(moduleModel);
                this._response.Data = dataModule;

                if (Convert.ToInt32(dataModule.Rows[0]["MODULE_ID"]) > 0)
                {
                    this._response.Message = "Module has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Module was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified module
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the modified module</returns>
        public ResponseModel ModifyModule(ConnectionModel credits, ModuleModel moduleModel)
        {
            DataTable dataModule = new DataTable();
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                dataModule = moduleAccess.ModifyModule(moduleModel);
                this._response.Data = dataModule;

                if (dataModule.Rows[0]["MODULE_ID"].ToString() == moduleModel.Id.ToString())
                {
                    this._response.Message = "Module has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Module was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified module
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleId">id of the module</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveModule(ConnectionModel credits, int moduleId)
        {
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                moduleAccess.RemoveModule(moduleId);

                this._response.Message = "Module has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleId">id of the specified module</param>
        /// <returns>returns a data table containing the details of the specified module</returns>
        public ResponseModel GetModule(ConnectionModel credits, int moduleId)
        {
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                this._response.Data = moduleAccess.GetModule(moduleId);
                this._response.Message = "Specific Module detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all parent modules
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all parent modules found on the database</returns>
        public ResponseModel GetParentModules(ConnectionModel credits)
        {
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                this._response.Data = moduleAccess.GetParentModules();
                this._response.Message = "All Parent Modules was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all modules
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all modules found on the database</returns>
        public ResponseModel GetAllModules(ConnectionModel credits)
        {
            ModuleAccess moduleAccess = new ModuleAccess(credits);

            try
            {
                this._response.Data = moduleAccess.GetAllModules();
                this._response.Message = "All Modules was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}