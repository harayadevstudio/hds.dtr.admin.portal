﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Data Identifier Class
    /// </summary>
    public class DataIdentifier
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new data identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataIdentifierDesc">description for the new data identifier</param>
        /// <returns>returns the details of the data identifier added</returns>
        public ResponseModel AddDataIdentifier(ConnectionModel credits, string dataIdentifierDesc)
        {
            DataIdentifierAccess dataIdentifierAccess = new DataIdentifierAccess(credits);
            DataTable dataDataIdentifier = new DataTable();

            try
            {
                dataDataIdentifier = dataIdentifierAccess.AddDataIdentifier(dataIdentifierDesc);
                this._response.Data = dataDataIdentifier;

                if (dataDataIdentifier.Rows[0]["DESCRIPTION"].ToString() == dataIdentifierDesc)
                {
                    this._response.Message = "Data Identifier has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Identifier was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified data identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataIdentifierId">id of the selected data identifier</param>
        /// <param name="description">description of the data identifier</param>
        /// <returns>returns the details of the modified data identifier</returns>
        public ResponseModel ModifyDataIdentifier(ConnectionModel credits, int dataIdentifierId, string description)
        {
            DataIdentifierAccess dataIdentifierAccess = new DataIdentifierAccess(credits);
            DataTable dataDataIdentifier = new DataTable();

            try
            {
                dataDataIdentifier = dataIdentifierAccess.ModifyDataIdentifier(dataIdentifierId, description);
                this._response.Data = dataDataIdentifier;

                if (dataDataIdentifier.Rows[0]["DATA_IDN_ID"].ToString() == dataIdentifierId.ToString() &&
                    dataDataIdentifier.Rows[0]["DESCRIPTION"].ToString() == description)
                {
                    this._response.Message = "Data Identifier has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Identifier was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified data identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataIdentifierId">id of the data identifier</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveDataIdentifier(ConnectionModel credits, int dataIdentifierId)
        {
            DataIdentifierAccess dataIdentifierAccess = new DataIdentifierAccess(credits);

            try
            {
                dataIdentifierAccess.RemoveDataIdentifier(dataIdentifierId);

                this._response.Message = "Data Identifier has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataIdentifierId">id of the specified data identifier</param>
        /// <returns>returns a data table containing the details of the specified data identifier</returns>
        public ResponseModel GetDataIdentifier(ConnectionModel credits, int dataIdentifierId)
        {
            DataIdentifierAccess dataIdentifierAccess = new DataIdentifierAccess(credits);

            try
            {
                this._response.Data = dataIdentifierAccess.GetDataIdentifier(dataIdentifierId);
                this._response.Message = "Specific data identifier details was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all data identifier found on the database</returns>
        public ResponseModel GetAllDataIdentifier(ConnectionModel credits)
        {
            DataIdentifierAccess dataIdentifierAccess = new DataIdentifierAccess(credits);

            try
            {
                this._response.Data = dataIdentifierAccess.GetAllDataIdentifier();
                this._response.Message = "All data identifiers was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}