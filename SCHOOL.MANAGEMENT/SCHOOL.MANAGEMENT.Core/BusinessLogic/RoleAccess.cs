﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using KCC.MIS.Core.ConHelper.Generic;
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Contains all the Business Logic Layer Role Access Class
    /// </summary>
    public class RoleAccess
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new role access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleAccessModel">Model object for module access</param>
        /// <returns>returns the details of the added role access</returns>
        public ResponseModel AddModuleAccess(ConnectionModel credits, ModuleAccessModel moduleAccessModel)
        {
            DataTable dataModuleAccess = new DataTable();
            RoleRightAccess roleAccess = new RoleRightAccess(credits);

            try
            {
                dataModuleAccess = roleAccess.AddModuleAccess(moduleAccessModel);

                if (dataModuleAccess.Rows.Count > 0)
                {
                    this._response.Message = "Role access has been added successfully";
                    this._response.Status = 1;
                    this._response.Data = dataModuleAccess;
                }
                else
                {
                    this._response.Message = "Role access was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified role access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="modelModuleAccess">Model object for module access</param>
        /// <returns>returns the details of the modified role access</returns>
        public ResponseModel ModifyModuleAccess(ConnectionModel credits, ModuleAccessModel modelModuleAccess)
        {
            DataTable dataRoleAccess = new DataTable();
            RoleRightAccess roleAccess = new RoleRightAccess(credits);

            try
            {
                dataRoleAccess = roleAccess.ModifyModuleAccess(modelModuleAccess);
                this._response.Data = dataRoleAccess;

                if (dataRoleAccess.Rows.Count > 0)
                {
                    this._response.Message = "Role access has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Role access was not updated";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified role access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="roleAccessId">id of the selected role access</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveRoleAccess(ConnectionModel credits, int roleAccessId)
        {
            RoleRightAccess roleAccess = new RoleRightAccess(credits);

            try
            {
                //roleaccess.removeroleaccess(roleaccessid);

                this._response.Message = "Role access has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleAccessId">id of the specified role access</param>
        /// <returns>returns a data table containing the details of the specified role access</returns>
        public ResponseModel GetModuleAccess(ConnectionModel credits, int moduleAccessId)
        {
            RoleRightAccess roleAccess = new RoleRightAccess(credits);

            try
            {
                this._response.Data = roleAccess.GetModuleAccess(moduleAccessId);
                this._response.Message = "Specific Role Access detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes a module Access
        /// </summary>
        /// <param name="credits">Connection model Instance</param>
        /// <param name="moduleAccessId">id of the module access to be removed</param>
        /// <returns>object based on the execution result</returns>
        public ResponseModel RemoveModuleAccess(ConnectionModel credits, int moduleAccessId)
        {
            RoleRightAccess roleAccess = new RoleRightAccess(credits);
            DataTable transaction = new DataTable();

            try
            {
                transaction = roleAccess.RemoveModuleAccess(moduleAccessId);
                this._response.Data = transaction;

                if (Convert.ToInt32(transaction.Rows[0]["DELETED"]) > 0)
                {
                    this._response.Message = "Module Access has been Removed";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Unsuccessfully Removed Module Access";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all role access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all role access found on the database</returns>
        public ResponseModel GetAllModuleAccess(ConnectionModel credits)
        {
            RoleRightAccess roleAccess = new RoleRightAccess(credits);

            try
            {
                this._response.Data = roleAccess.GetAllModuleAccess();
                this._response.Message = "All Role Access was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}