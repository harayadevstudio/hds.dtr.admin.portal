﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using Newtonsoft.Json;
    using System.Data;

    /// <summary>
    /// Class Object Convertion
    /// </summary>
    public class ObjectConversion
    {
        /// <summary>
        /// Convert Object to DataTable
        /// </summary>
        /// <param name="objName">Set Object Value</param>
        /// <returns>Returns DataTable Data from Object Data</returns>
        public DataTable ConvertObject(object objName)
        {
            DataTable dataObject = new DataTable();

            dataObject = (DataTable)JsonConvert.DeserializeObject(
                JsonConvert.SerializeObject(objName).ToString(),
                typeof(DataTable));

            return dataObject;
        }
    }
}