namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Business Logic Class for Component
    /// </summary>
    public class Component
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Add component method
        /// </summary>
        /// <param name="credits">Connection Model [Key]</param>
        /// <param name="component">Component Model</param>
        /// <returns>Returns details of added component</returns>
        public ResponseModel AddComponent(ConnectionModel credits, ComponentModel component)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.AddComponent(component);
                this._response.Message = "Successfully added new component";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Modify component method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="component">Component Model</param>
        /// <returns>Returns details of modified component</returns>
        public ResponseModel ModifyComponent(ConnectionModel credits, ComponentModel component)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.ModifyComponent(component);
                this._response.Message = "Successfully modified component";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get shown component method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <returns>Returns details of fetch component</returns>
        public ResponseModel GetShownComponent(ConnectionModel credits)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetComponent(0, 1);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get all components method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <returns>Returns details of fetch component</returns>
        public ResponseModel GetAllComponent(ConnectionModel credits)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetComponent(0, 2);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get specific component method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="componentId">Component Primary Id</param>
        /// <returns>Returns details of specific component</returns>
        public ResponseModel GetComponent(ConnectionModel credits, int componentId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetComponent(componentId, 2);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Add component data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="component">Component Model</param>
        /// <returns>Returns details of added component data</returns>
        public ResponseModel AddComponentData(ConnectionModel credits, ComponentModel[] component)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                string componentDataList = string.Empty;

                int dataCounter = 0;

                foreach (ComponentModel data in component)
                {
                    var componentRow = new
                    {
                        compVal = new
                        {
                            data_id = data.DataId,
                            data_value = data.DataValue,
                            data_primary_key = data.DataKeyId,
                            metadata_id = data.MetadataId,
                            component_id = data.ComponentId,
                            unique_ind = data.UniqueInd,
                            comp_map_id = data.CompMapId
                        }
                    };

                    string dataValue = Newtonsoft.Json.JsonConvert.SerializeObject(componentRow, Newtonsoft.Json.Formatting.None).Replace("compVal", "data" + ++dataCounter);

                    dataValue = dataValue.Remove(0, 1);
                    dataValue = dataValue.Remove(dataValue.Length - 1, 1);

                    componentDataList = componentDataList + dataValue + ",";
                }

                componentDataList = "{" + componentDataList.Remove(componentDataList.Length - 1, 1) + "}";

                this._response.Data = componentAccess.AddComponentData(componentDataList);
                this._response.Message = "Successfully added new component data";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Modify component data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="component">Component Model</param>
        /// <returns>Returns details of modified component data</returns>
        public ResponseModel ModifyComponentData(ConnectionModel credits, ComponentModel[] component)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                string componentDataList = string.Empty;

                int dataCounter = 0;

                foreach (ComponentModel data in component)
                {
                    var componentRow = new
                    {
                        compVal = new
                        {
                            data_id = data.DataId,
                            data_value = data.DataValue,
                            data_primary_key = data.DataKeyId,
                            metadata_id = data.MetadataId,
                            component_id = data.ComponentId,
                            unique_ind = data.UniqueInd,
                            comp_map_id = data.CompMapId
                        }
                    };

                    string dataValue = Newtonsoft.Json.JsonConvert.SerializeObject(componentRow, Newtonsoft.Json.Formatting.None).Replace("compVal", "data" + ++dataCounter);

                    dataValue = dataValue.Remove(0, 1);
                    dataValue = dataValue.Remove(dataValue.Length - 1, 1);

                    componentDataList = componentDataList + dataValue + ",";
                }

                componentDataList = "{" + componentDataList.Remove(componentDataList.Length - 1, 1) + "}";

                this._response.Data = componentAccess.ModifyComponentData(componentDataList);
                this._response.Message = "Successfully modified component data";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get component's data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="componentId">Component's Id</param>
        /// <returns>Returns details of fetch component's data</returns>
        public ResponseModel GetComponentData(ConnectionModel credits, int componentId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetComponentDataByUser(0, componentId);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get component's data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="componentId">Component's Id</param>
        /// <returns>Returns details of fetch component's data</returns>
        public ResponseModel GetActiveComponentData(ConnectionModel credits, int componentId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                DataTable componentData = new DataTable();

                componentData = componentAccess.GetComponentDataByUser(0, componentId);

                if (componentData.Columns.Contains("STATUS"))
                {
                    componentData = componentData.Select("[STATUS] = '1'").CopyToDataTable();
                }

                this._response.Data = componentData;

                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get component's data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="dataKeyId">Data Key's Id</param>
        /// <param name="componentId">Component's Id</param>
        /// <returns>Returns details of fetch component's data</returns>
        public ResponseModel GetComponentData(ConnectionModel credits, int dataKeyId, int componentId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits.Username, credits.DBPassword);

                this._response.Data = componentAccess.GetComponentDataByUser(dataKeyId, componentId);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get component's data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <returns>Returns details of all components data</returns>
        public ResponseModel GetAllComponentData(ConnectionModel credits)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetComponentDataByUser(0, 0);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get specific component data method
        /// </summary>
        /// <param name="credits">Connection Model [key]</param>
        /// <param name="dataKeyId">Data Primary Id</param>
        /// <param name="componentId">Component Id</param>
        /// <returns>Returns details of specific component data</returns>
        public ResponseModel GetSpecificData(ConnectionModel credits, int dataKeyId, int componentId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess(credits);

                this._response.Data = componentAccess.GetSpecificData(dataKeyId, componentId);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get Foreign Local Component Data
        /// </summary>
        /// <param name="credits">Connection Model Credits</param>
        /// <param name="componentId">Component Id</param>
        /// <param name="dataPrimaryKey">Data Primary Key</param>
        /// <returns>returns details of Foreign Local Component Data</returns>
        public ResponseModel GetForeignLocalData(ConnectionModel credits, int componentId, int dataPrimaryKey)
        {
            try
            {
                ComponentAccess component = new ComponentAccess(credits.Username, credits.DBPassword);

                this._response.Data = component.GetForeignLocalData(componentId, dataPrimaryKey);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets Component data base on organization hierarchy
        /// </summary>
        /// <param name="credits">Consist of user credentials</param>
        /// <param name="componentId">Components Identifier</param>
        /// <param name="orgHrchyId">organization hierarchy id</param>
        /// <returns>Returns fetch component data</returns>
        public ResponseModel GetComponentDataByOrg(int componentId, int orgHrchyId)
        {
            try
            {
                ComponentAccess componentAccess = new ComponentAccess();

                this._response.Data = componentAccess.GetComponentDataByOrg(componentId, orgHrchyId);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}
