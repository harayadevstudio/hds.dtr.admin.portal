﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Under Action Class
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Gets the details of All Actions
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>Returns a data containing the details of all actions found on the database</returns>
        public ResponseModel GetAllActions(ConnectionModel credits)
        {
            ActionAccess actionAccess = new ActionAccess(credits);

            try
            {
                this._response.Data = actionAccess.GetAllActions();
                this._response.Message = "All actions was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets the details of the Specific Action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="actionId">ID of the Specific Action</param>
        /// <returns>Returns the details of the Specified Action</returns>
        public ResponseModel GetAction(ConnectionModel credits, int? actionId)
        {
            ActionAccess actionAccess = new ActionAccess(credits);

            try
            {
                this._response.Data = actionAccess.GetAction(actionId);
                this._response.Message = "Specific action details was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Adds New Action Information
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="actionModel">Action Model Details[Name, Description, CreatedBy]</param>
        /// <returns>Returns the Details of the New Added Action</returns>
        public ResponseModel AddAction(ConnectionModel credits, ActionModel actionModel)
        {
            DataTable dataAction = new DataTable();
            ActionAccess actionAccess = new ActionAccess(credits);

            try
            {
                dataAction = actionAccess.AddAction(actionModel);

                if (dataAction.Rows.Count > 0)
                {
                    this._response.Data = dataAction;
                    this._response.Message = "Action has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Action was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the Information of Specified Action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="actionModel">Action Model Details[Name, Description, UpdatedBy]</param>
        /// <returns>Returns the Details of the Modified Action</returns>
        public ResponseModel ModifyAction(ConnectionModel credits, ActionModel actionModel)
        {
            DataTable dataAction = new DataTable();
            ActionAccess actionAccess = new ActionAccess(credits);

            try
            {
                dataAction = actionAccess.ModifyAction(actionModel);

                if (dataAction.Rows.Count > 0)
                {
                    this._response.Data = dataAction;
                    this._response.Message = "Action has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Action was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the Specified Action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="actionId">ID of then Action</param>
        /// <returns>Returns Response Model Details[Status, Message]</returns>
        public ResponseModel RemoveAction(ConnectionModel credits, int? actionId)
        {
            ActionAccess actionAccess = new ActionAccess(credits);

            try
            {
                actionAccess.RemoveAction(actionId);

                this._response.Message = "Action has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}