﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Role Class
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new user role
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the added user role</returns>
        public ResponseModel AddUserRole(ConnectionModel credits, RoleModel roleModel)
        {
            DataTable dataRole = new DataTable();
            Core.DataAccess.RoleAccess roleAccess = new Core.DataAccess.RoleAccess(credits);

            try
            {
                dataRole = roleAccess.AddUserRole(roleModel);
                this._response.Data = dataRole;

                if (Convert.ToInt32(dataRole.Rows[0]["ROLE_ID"]) > 0)
                {
                    this._response.Message = "User Role has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User Role was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified user role
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the modified user role</returns>
        public ResponseModel ModifyUserRole(ConnectionModel credits, RoleModel roleModel)
        {
            DataTable dataRole = new DataTable();
            Core.DataAccess.RoleAccess roleAccess = new Core.DataAccess.RoleAccess(credits);

            try
            {
                dataRole = roleAccess.ModifyUserRole(roleModel);
                this._response.Data = dataRole;

                if (dataRole.Rows[0]["ROLE_ID"].ToString() == roleModel.Id.ToString())
                {
                    this._response.Message = "User Role has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User Role was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified role
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userRoleId">id of the selected role</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveUserRole(ConnectionModel credits, int userRoleId)
        {
            Core.DataAccess.RoleAccess roleAccess = new Core.DataAccess.RoleAccess(credits);

            try
            {
                roleAccess.RemoveUserRole(userRoleId);

                this._response.Message = "User Role has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userRoleId">id of the specified role</param>
        /// <returns>returns a data table containing the details of the specified role</returns>
        public ResponseModel GetUserRole(ConnectionModel credits, int userRoleId)
        {
            Core.DataAccess.RoleAccess roleAccess = new Core.DataAccess.RoleAccess(credits);

            try
            {
                this._response.Data = roleAccess.GetUserRole(userRoleId);
                this._response.Message = "Specific User Role detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all user roles
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all user roles found on the database</returns>
        public ResponseModel GetAllUserRoles(ConnectionModel credits)
        {
            Core.DataAccess.RoleAccess roleAccess = new Core.DataAccess.RoleAccess(credits);

            try
            {
                this._response.Data = roleAccess.GetAllUserRoles();
                this._response.Message = "All Roles was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}