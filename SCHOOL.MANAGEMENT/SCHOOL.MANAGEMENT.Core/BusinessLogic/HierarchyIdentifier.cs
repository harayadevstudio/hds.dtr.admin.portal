﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Hierarchy Identifier Class
    /// </summary>
    public class HierarchyIdentifier
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new hierarchy identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="hierarchyIdentifierDesc">description for the new hierarchy identifier</param>
        /// <returns>returns the details of the hierarchy identifier added</returns>
        public ResponseModel AddHierarchyIdentifier(ConnectionModel credits, string hierarchyIdentifierDesc)
        {
            HierarchyIdentifierAccess hierarchyIdentifierAccess = new HierarchyIdentifierAccess(credits);
            DataTable dataHierarchyIdentifier = new DataTable();

            try
            {
                dataHierarchyIdentifier = hierarchyIdentifierAccess.AddHierarchyIdentifier(hierarchyIdentifierDesc);
                this._response.Data = dataHierarchyIdentifier;

                if (dataHierarchyIdentifier.Rows[0]["DESCRIPTION"].ToString() == hierarchyIdentifierDesc)
                {
                    this._response.Message = "Hierarchy Identifier has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Hierarchy Identifier was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified hierarchy identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="hierarchyIdentifierId">id of the selected hierarchy identifier</param>
        /// <param name="description">description of the hierarchy identifier</param>
        /// <returns>returns the details of the modified hierarchy identifier</returns>
        public ResponseModel ModifyHierarchyIdentifier(ConnectionModel credits, int hierarchyIdentifierId, string description)
        {
            HierarchyIdentifierAccess hierarchyIdentifierAccess = new HierarchyIdentifierAccess(credits);
            DataTable dataHierarchyIdentifier = new DataTable();

            try
            {
                dataHierarchyIdentifier = hierarchyIdentifierAccess.ModifyHierarchyIdentifier(hierarchyIdentifierId, description);
                this._response.Data = dataHierarchyIdentifier;

                if (dataHierarchyIdentifier.Rows[0]["DESCRIPTION"].ToString() == description &&
                    dataHierarchyIdentifier.Rows[0]["HRCHY_IDN_ID"].ToString() == hierarchyIdentifierId.ToString())
                {
                    this._response.Message = "Hierarchy Identifier has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Hierarchy Identifier was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified hierarchy identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="hierarchyIdentifierId">id of the hierarchy identifier</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveHierarchyIdentifier(ConnectionModel credits, int hierarchyIdentifierId)
        {
            HierarchyIdentifierAccess hierarchyIdentifierAccess = new HierarchyIdentifierAccess(credits);

            try
            {
                hierarchyIdentifierAccess.RemoveHierarchyIdentifier(hierarchyIdentifierId);

                this._response.Message = "Hierarchy Identifier has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified hierarchy identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="hierarchyIdentifierId">id of the specified hierarchy identifier</param>
        /// <returns>returns a data table containing the details of the specified hierarchy identifier</returns>
        public ResponseModel GetHierarchyIdentifier(ConnectionModel credits, int hierarchyIdentifierId)
        {
            HierarchyIdentifierAccess hierarchyIdentifierAccess = new HierarchyIdentifierAccess(credits);

            try
            {
                this._response.Data = hierarchyIdentifierAccess.GetHierarchyIdentifier(hierarchyIdentifierId);
                this._response.Message = "Specific Hierarchy Identifier detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all hierarchy identifier
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all hierarchy identifier found on the database</returns>
        public ResponseModel GetAllHierarchyIdentifier(ConnectionModel credits)
        {
            HierarchyIdentifierAccess hierarchyIdentifierAccess = new HierarchyIdentifierAccess(credits);

            try
            {
                this._response.Data = hierarchyIdentifierAccess.GetAllHierarchyIdentifier();
                this._response.Message = "All Hierarchy Identifiers was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}