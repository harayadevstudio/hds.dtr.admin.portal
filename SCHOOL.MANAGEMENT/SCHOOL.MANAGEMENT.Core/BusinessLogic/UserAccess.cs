﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using Newtonsoft.Json;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer User Access Class
    /// </summary>
    public class UserAccess
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new user access
        /// </summary>
        /// <param name="credits">Connection Model Instance</param>
        /// <param name="modelUserAccess">user Access Model</param>
        /// <returns>returns the details of the added user access</returns>
        public ResponseModel AddUserAccess(ConnectionModel credits, UserAccessModel modelUserAccess)
        {
            DataTable dataUserAccess = new DataTable();
            UserRightAccess userAccessRights = new UserRightAccess(credits);

            try
            {
                dataUserAccess = userAccessRights.AddUserAccess(modelUserAccess);
                this._response.Data = dataUserAccess;

                if (dataUserAccess.Rows.Count > 0)
                {
                    this._response.Message = "User Access has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User Access was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified user access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="modelUserAccess">user Access model</param>
        /// <returns>returns the details of the added user access</returns>
        public ResponseModel ModifyUserAccess(ConnectionModel credits, UserAccessModel modelUserAccess)
        {
            DataTable dataUserAccess = new DataTable();
            UserRightAccess userAccessRights = new UserRightAccess(credits);

            try
            {
                dataUserAccess = userAccessRights.ModifyUserAccess(modelUserAccess);
                this._response.Data = dataUserAccess;

                if (dataUserAccess.Rows.Count > 0)
                {
                    this._response.Message = "User Access has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User Access was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified user access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userAccessId">id of the selected user access</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveUserAccess(ConnectionModel credits, int userAccessId)
        {
            UserRightAccess userAccessRights = new UserRightAccess(credits);

            try
            {
                userAccessRights.RemoveUserAccess(userAccessId);

                this._response.Message = "User Access has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userAccessId">id of the specified user access</param>
        /// <returns>returns a data table containing the details of the specified user access</returns>
        public ResponseModel GetUserAccess(ConnectionModel credits, int userAccessId)
        {
            UserRightAccess userAccessRights = new UserRightAccess(credits);

            try
            {
                this._response.Data = userAccessRights.GetUserAccess(userAccessId);
                this._response.Message = "Specific User Access detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets all user access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all user access found on the database</returns>
        public ResponseModel GetAllUserAccess(ConnectionModel credits)
        {
            UserRightAccess userAccessRights = new UserRightAccess(credits);

            try
            {
                this._response.Data = userAccessRights.GetAllUserAccess();
                this._response.Message = "All User Access was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets Lists of user Modules by user
        /// </summary>
        /// <param name="credits">connection model instance</param>
        /// <returns>Lists of User Modules</returns>
        public ResponseModel GetUserModules(ConnectionModel credits)
        {
            UserRightAccess userModules = new UserRightAccess(credits);

            try
            {
                this._response.Data = userModules.GetUserModules();
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets Lists of organization Access
        /// </summary>
        /// <param name="credits">Connection Model Instance</param>
        /// <returns>Lists of Organization Access</returns>
        public ResponseModel GetOrganizationAccess(ConnectionModel credits)
        {
            UserRightAccess userAccess = new UserRightAccess(credits);

            try
            {
                this._response.Data = userAccess.GetOrganizationAccess();
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets Lists of organization Access by irg
        /// </summary>
        /// <param name="credits">Connection model Instance</param>
        /// <param name="orgId">org id specified</param>
        /// <returns>New Encrypted Cookie</returns>
        public ResponseModel GetOrganizationAccessByOrg(ConnectionModel credits, int orgId)
        {
            UserRightAccess userAccess = new UserRightAccess(credits);
            CredentialAccess credentialAccess = new CredentialAccess();
            DatabaseConnection oracle = new DatabaseConnection();
            ConnectionModel connect = new ConnectionModel();

            try
            {
                DataTable dataCurrentDate = new DataTable();
                dataCurrentDate = credentialAccess.GetDatabaseDate();
                //// string currentDate = Convert.ToDateTime(dataCurrentDate.Rows[0]["SYSTEM_DATE"].ToString()).ToLongDateString();
                string currentDate = DateTime.Now.ToString();

                credits.OrganizationId = orgId;

                this._response.Status = 1;

                object key = new
                {
                    UserId = credits.UserId,
                    Username = credits.Username,
                    DBPassword = oracle.Encrypt(credits.Password),
                    OrganizationId = credits.OrganizationId,
                    TimeStamp = currentDate
                };

                this._response.Data = new
                {
                    key = oracle.Encrypt(JsonConvert.SerializeObject(key, Formatting.None))
                };
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets Lists of User Module Actions
        /// </summary>
        /// <param name="credits">connection model instance</param>
        /// <param name="modulePage">module page</param>
        /// <returns>Lists of User Module Actions</returns>
        public ResponseModel GetUserModuleActions(ConnectionModel credits, string modulePage)
        {
            UserRightAccess userModules = new UserRightAccess(credits);

            try
            {
                this._response.Data = userModules.GetUserModuleActions(modulePage);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Get the allowed actions per module of the users
        /// </summary>
        /// <param name="credits">Connection Model Details [key]</param>
        /// <param name="userModel"> User Model</param>
        /// <returns>returns a data table containing the details of all allowed actions in every module of the users</returns>
        public ResponseModel GetUsersModuleAccess(ConnectionModel credits, UserModel userModel)
        {
            UserRightAccess userModules = new UserRightAccess(credits.Username, credits.Password);

            try
            {
                this._response.Data = userModules.GetUsersModuleAccess(userModel);
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}