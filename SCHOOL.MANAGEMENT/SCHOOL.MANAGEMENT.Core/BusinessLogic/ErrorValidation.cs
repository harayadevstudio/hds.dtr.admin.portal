﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using Microsoft.VisualBasic;

    /// <summary>
    /// Error Validation Handler
    /// </summary>
    public class ErrorValidation
    {
        /// <summary>
        /// Error Message From Oracle
        /// </summary>
        /// <param name="errornum">Error Number</param>
        /// <returns>Returns a Value containing the Error</returns>
        public static string ErrorMsg(string errornum)
        {
            string retval = string.Empty;
            try
            {
                if (errornum.Contains("ORA-02292"))
                {
                    retval = "Unable to delete record(s), Record is already used by another transaction!";
                }
                else if (errornum.Contains("ORA-00001"))
                {
                    retval = "Record already existed.";
                }
                else if (errornum.Contains("ORA-00942"))
                {
                    retval = "The User is not yet activated or is not found, Please contact the Administrator.";
                }
                else if (errornum.Contains("ORA-06550"))
                {
                    retval = "Validate Return Value can be out or invalid data type";
                }
                else if (errornum.Contains("Object reference not set to an instance of an object."))
                {
                    retval = "Please contact the Administrator, Thank you.";
                }
                else
                {
                    retval = errornum;
                }
            }
            catch
            {
                throw;
            }

            return retval;
        }

        /// <summary>
        /// Replace value for null object with specified return value
        /// </summary>
        /// <param name="fieldname">Parameter fieldname</param>
        /// <param name="returnValue">Parameter returnValue</param>
        /// <returns>Returns Object</returns>
        public static object Isnull(object fieldname, object returnValue)
        {
            object str = null;
            try
            {
                if (Information.IsDBNull(fieldname) == true)
                {
                    str = returnValue;
                }
                else if (fieldname == null)
                {
                    str = returnValue;
                }
                else
                {
                    str = fieldname;
                }

                return str;
            }
            catch
            {
                return null;
            }
        }
    }
}