﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Data Hierarchy Class
    /// </summary>
    public class DataHierarchy
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new data hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the data hierarchy added</returns>
        public ResponseModel AddDataHierarchy(ConnectionModel credits, DataHierarchyModel dataHierarchyModel)
        {
            DataHierarchyAccess dataHierarchyAccess = new DataHierarchyAccess(credits);
            DataTable dataHierarchy = new DataTable();

            try
            {
                dataHierarchy = dataHierarchyAccess.AddDataHierarchy(dataHierarchyModel);
                this._response.Data = dataHierarchy;

                if (dataHierarchy.Rows[0]["REFERENCE_ID"].ToString() == dataHierarchyModel.ReferenceId.ToString() ||
                    dataHierarchy.Rows[0]["PARENT_ID"].ToString() == dataHierarchyModel.ParentId.ToString() ||
                    dataHierarchy.Rows[0]["DATA_IDN_ID"].ToString() == dataHierarchyModel.DataIdentifierId.ToString())
                {
                    this._response.Message = "Data Hierarchy has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Hierarchy was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified data hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the modified data hierarchy</returns>
        public ResponseModel ModifyDataHierarchy(ConnectionModel credits, DataHierarchyModel dataHierarchyModel)
        {
            DataHierarchyAccess dataHierarchyAccess = new DataHierarchyAccess(credits);
            DataTable dataHierarchy = new DataTable();

            try
            {
                dataHierarchy = dataHierarchyAccess.ModifyDataHierarchy(dataHierarchyModel);
                this._response.Data = dataHierarchy;

                if (dataHierarchy.Rows[0]["DATA_HRCHY_ID"].ToString() == dataHierarchyModel.Id.ToString() &&
                    dataHierarchy.Rows[0]["REFERENCE_ID"].ToString() == dataHierarchyModel.ReferenceId.ToString() &&
                    dataHierarchy.Rows[0]["PARENT_ID"].ToString() == dataHierarchyModel.ParentId.ToString() &&
                    dataHierarchy.Rows[0]["DATA_IDN_ID"].ToString() == dataHierarchyModel.DataIdentifierId.ToString())
                {
                    this._response.Message = "Data Hierarchy has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Hierarchy was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified data hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataHierarchyId">id of the data hierarchy</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveDataHierarchy(ConnectionModel credits, int dataHierarchyId)
        {
            DataHierarchyAccess dataHierarchyAccess = new DataHierarchyAccess(credits);

            try
            {
                dataHierarchyAccess.RemoveDataHierarchy(dataHierarchyId);

                this._response.Message = "Data Hierarchy has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataHierarchyId">id of the specified data hierarchy</param>
        /// <returns>returns a data table containing the details of the specified data hierarchy</returns>
        public ResponseModel GetDataHierarchy(ConnectionModel credits, int? dataHierarchyId)
        {
            DataHierarchyAccess dataHierarchyAccess = new DataHierarchyAccess(credits);

            try
            {
                this._response.Data = dataHierarchyAccess.GetDataHierarchy(dataHierarchyId);
                this._response.Message = "Specific data hierarchy details was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all data hierarchy found on the database</returns>
        public ResponseModel GetAllDataHierarchy(ConnectionModel credits)
        {
            DataHierarchyAccess dataHierarchyAccess = new DataHierarchyAccess(credits);

            try
            {
                this._response.Data = dataHierarchyAccess.GetAllDataHierarchy();
                this._response.Message = "All data hierarchy was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}