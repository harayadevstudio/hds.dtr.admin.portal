﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Data Access Class
    /// </summary>
    public class DataAccess
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new data access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataAccessModel">Model object for data access</param>
        /// <returns>returns the details of the data access added</returns>
        public ResponseModel AddDataAccess(ConnectionModel credits, DataAccessModel dataAccessModel)
        {
            Core.DataAccess.DataAccess dataAccess = new Core.DataAccess.DataAccess(credits);
            DataTable dataDataAccess = new DataTable();

            try
            {
                dataDataAccess = dataAccess.AddDataAccess(dataAccessModel);
                this._response.Data = dataDataAccess;

                if (dataDataAccess.Rows.Count > 0)
                {
                    this._response.Message = "Data Access has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Access was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified data access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataAccessModel">Model object for data access</param>
        /// <returns>returns the details of the modified data access</returns>
        public ResponseModel ModifyDataAccess(ConnectionModel credits, DataAccessModel dataAccessModel)
        {
            Core.DataAccess.DataAccess dataAccess = new Core.DataAccess.DataAccess(credits);
            DataTable dataDataAccess = new DataTable();

            try
            {
                dataDataAccess = dataAccess.ModifyDataAccess(dataAccessModel);
                this._response.Data = dataDataAccess;

                if (dataDataAccess.Rows.Count > 0)
                {
                    this._response.Message = "Data Access has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Data Access was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified data access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataAccessId">id of the data access</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveDataAccess(ConnectionModel credits, int? dataAccessId)
        {
            Core.DataAccess.DataAccess dataAccess = new Core.DataAccess.DataAccess(credits);

            try
            {
                dataAccess.RemoveDataAccess(dataAccessId);

                this._response.Message = "Data Access has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="dataAccessId">id of the specified data access</param>
        /// <returns>returns a data table containing the details of the specified data access</returns>
        public ResponseModel GetDataAccess(ConnectionModel credits, int? dataAccessId)
        {
            Core.DataAccess.DataAccess dataAccess = new Core.DataAccess.DataAccess(credits);

            try
            {
                this._response.Data = dataAccess.GetDataAccess(dataAccessId);
                this._response.Message = "Specific data access details was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data access
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all data access found on the database</returns>
        public ResponseModel GetAllDataAccess(ConnectionModel credits)
        {
            Core.DataAccess.DataAccess dataAccess = new Core.DataAccess.DataAccess(credits);

            try
            {
                this._response.Data = dataAccess.GetAllDataAccess();
                this._response.Message = "All data access was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}