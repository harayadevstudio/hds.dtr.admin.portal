﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Organization Hierarchy Class
    /// </summary>
    public class OrgHierarchy
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new organization hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the organization hierarchy added</returns>
        public ResponseModel AddOrgHierarchy(ConnectionModel credits, OrgHierarchyModel orgHierarchyModel)
        {
            OrgHierarchyAccess orgHierarchyAccess = new OrgHierarchyAccess(credits);
            DataTable dataOrgHierarchy = new DataTable();

            try
            {
                dataOrgHierarchy = orgHierarchyAccess.AddOrgHierarchy(orgHierarchyModel);
                this._response.Data = dataOrgHierarchy;

                if (Convert.ToInt32(dataOrgHierarchy.Rows[0]["ORG_HRCHY_ID"]) > 0)
                {
                    this._response.Message = "Organization Hierarchy has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Organization Hierarchy was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified organization hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the modified organization hierarchy</returns>
        public ResponseModel ModifyOrgHierarchy(ConnectionModel credits, OrgHierarchyModel orgHierarchyModel)
        {
            DataTable dataOrgHierarchy = new DataTable();
            OrgHierarchyAccess orgHierarchyAccess = new OrgHierarchyAccess(credits);

            try
            {
                dataOrgHierarchy = orgHierarchyAccess.ModifyOrgHierarchy(orgHierarchyModel);
                this._response.Data = dataOrgHierarchy;

                if (dataOrgHierarchy.Rows[0]["ORG_HRCHY_ID"].ToString() == orgHierarchyModel.Id.ToString())
                {
                    this._response.Message = "Organization Hierarchy has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Organization Hierarchy was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified organization hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="orgHierarchyId">id of the organization hierarchy</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveOrgHierarchy(ConnectionModel credits, int orgHierarchyId)
        {
            OrgHierarchyAccess orgHierarchyAccess = new OrgHierarchyAccess(credits);

            try
            {
                orgHierarchyAccess.RemoveOrgHierarchy(orgHierarchyId);

                this._response.Message = "Organization Hierarchy has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified organization hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="orgHierarchyId">id of the specified organization hierarchy</param>
        /// <returns>returns a data table containing the details of the specified organization hierarchy</returns>
        public ResponseModel GetOrgHierarchy(ConnectionModel credits, int orgHierarchyId)
        {
            OrgHierarchyAccess orgHierarchyAccess = new OrgHierarchyAccess(credits);

            try
            {
                this._response.Data = orgHierarchyAccess.GetOrgHierarchy(orgHierarchyId);
                this._response.Message = "Specific Organization Hierarchy detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all organization hierarchy
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all organization hierarchy found on the database</returns>
        public ResponseModel GetAllOrgHierarchy(ConnectionModel credits)
        {
            OrgHierarchyAccess orgHierarchyAccess = new OrgHierarchyAccess(credits);

            try
            {
                this._response.Data = orgHierarchyAccess.GetAllOrgHierarchy();
                this._response.Message = "All Organization Hierarchy was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}