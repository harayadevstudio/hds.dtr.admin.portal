﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;

    /// <summary>
    /// Contains all the Business Logic Layer Module Class
    /// </summary>
    public class ModuleAction
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Adds new module action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleId">Id of the selected module</param>
        /// <param name="actionId">Id of the selected action</param>
        /// <param name="userId">Id of the creator</param>
        /// <returns>returns the details of the added module action</returns>
        public ResponseModel AddModuleAction(ConnectionModel credits, int moduleId, int actionId, int userId)
        {
            DataTable dataModuleAction = new DataTable();
            ModuleActionAccess moduleActionAccess = new ModuleActionAccess(credits);

            try
            {
                dataModuleAction = moduleActionAccess.AddModuleAction(moduleId, actionId, userId);
                this._response.Data = dataModuleAction;

                if (dataModuleAction.Rows[0]["MODULE_ID"].ToString() == moduleId.ToString() ||
                    dataModuleAction.Rows[0]["ACTION_ID"].ToString() == actionId.ToString() ||
                    dataModuleAction.Rows[0]["CREATED_BY"].ToString() == userId.ToString())
                {
                    this._response.Message = "Module Action has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Module Action was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified module action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleActionId">id of the module action</param>
        /// <param name="moduleId">id of the selected module</param>
        /// <param name="actionId">id of the selected action</param>
        /// <returns>returns the details of the modified module action</returns>
        public ResponseModel ModifyModuleAction(ConnectionModel credits, int moduleActionId, int moduleId, int actionId)
        {
            DataTable dataModuleAction = new DataTable();
            ModuleActionAccess moduleActionAccess = new ModuleActionAccess(credits);

            try
            {
                dataModuleAction = moduleActionAccess.ModifyModuleAction(moduleActionId, moduleId, actionId);
                this._response.Data = dataModuleAction;

                if (dataModuleAction.Rows[0]["MODULE_ID"].ToString() == moduleId.ToString() ||
                    dataModuleAction.Rows[0]["ACTION_ID"].ToString() == actionId.ToString() ||
                    dataModuleAction.Rows[0]["MODULE_ACTION_ID"].ToString() == moduleActionId.ToString())
                {
                    this._response.Message = "Module Action has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "Module Action was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Removes the specified module action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleActionId">id of the module action</param>
        /// <returns>status 1 if successful and null if not</returns>
        public ResponseModel RemoveModuleAction(ConnectionModel credits, int moduleActionId)
        {
            ModuleActionAccess moduleActionAccess = new ModuleActionAccess(credits);

            try
            {
                moduleActionAccess.RemoveModuleAction(moduleActionId);

                this._response.Message = "Module Action has been removed successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module action
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="moduleActionId">id of the specified module action</param>
        /// <returns>returns a data table containing the details of the specified module action</returns>
        public ResponseModel GetModuleAction(ConnectionModel credits, int moduleActionId)
        {
            ModuleActionAccess moduleActionAccess = new ModuleActionAccess(credits);

            try
            {
                this._response.Data = moduleActionAccess.GetModuleAction(moduleActionId);
                this._response.Message = "Specific Module Action detail was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all module actions
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns a data table containing the details of all module actions found on the database</returns>
        public ResponseModel GetAllModuleActions(ConnectionModel credits)
        {
            ModuleActionAccess moduleActionAccess = new ModuleActionAccess(credits);

            try
            {
                this._response.Data = moduleActionAccess.GetAllModuleActions();
                this._response.Message = "All Module Actions was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}