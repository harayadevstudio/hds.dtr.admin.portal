﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using System;
    using System.Data;
    using SCHOOL.MANAGEMENT.Core.Model;
    using Newtonsoft.Json;

    /// <summary>
    /// Contains all the Business Logic Layer Under User Class
    /// </summary>
    public class User
    {
        /// <summary>
        /// Private Response Model
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Dynamic Data Access that gets the specified user
        /// </summary>
        /// <param name="username">username of the user</param>
        /// <param name="password">password of the user</param>
        /// <param name="currentDate">current date</param>
        /// <returns>returns a data table containing the details of the specified user</returns>
        public ResponseModel GetUserByCredentials(string username, string password, string currentDate)
        {
            DataTable dataUser = new DataTable();
            DataTable dataUserModules = new DataTable();
            DatabaseConnection general = new DatabaseConnection();
            Core.DataAccess.CredentialAccess credentialAccess = new Core.DataAccess.CredentialAccess();

            try
            {
                dataUser = credentialAccess.GetUserByCredentials(username, password);

                if (dataUser.Rows.Count > 0)
                {
                    if (dataUser.Rows[0]["IS_ACTIVE"].ToString() == "1")
                    {
                        object key = new
                        {
                            UserId = dataUser.Rows[0]["USER_ID"].ToString(),
                            Username = dataUser.Rows[0]["USERNAME"].ToString(),
                            DBPassword = general.Encrypt(dataUser.Rows[0]["DB_PASSWORD"].ToString()),
                            OrganizationId = dataUser.Rows[0]["ORG_HRCHY_ID"].ToString(),
                            TimeStamp = currentDate
                        };

                        this._response.Data = new
                        {
                            userId = dataUser.Rows[0]["USER_ID"].ToString(),
                            userName = dataUser.Rows[0]["USERNAME"].ToString(),
                            firstName = dataUser.Rows[0]["FIRST_NAME"].ToString(),
                            middleName = dataUser.Rows[0]["MIDDLE_NAME"].ToString(),
                            lastName = dataUser.Rows[0]["LAST_NAME"].ToString(),
                            modulePage = dataUser.Rows[0]["MODULE_PAGE"].ToString(),
                            key = general.Encrypt(JsonConvert.SerializeObject(key, Formatting.None))
                        };

                        this._response.Message = "User Account Successfully Login";
                        this._response.Status = 1;
                    }
                    else
                    {
                        this._response.Message = "User Account is inactive";
                        this._response.Status = 0;
                    }
                }
                else
                {
                    this._response.Message = "Invalid username and password";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Gets users with specified role
        /// </summary>
        /// <param name="credits">Connection model Instance</param>
        /// <param name="role">role name</param>
        /// <returns>List of users under specified role</returns>
        public ResponseModel GetUsersByRole(ConnectionModel credits, string role)
        {
            DataTable dataUser = new DataTable();
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                dataUser = userAccess.GetUsersByRole(role);

                this._response.Data = dataUser;
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
                throw;
            }

            return this._response;
        }

        /// <summary>
        /// Adds new user
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userModel">Model object for user</param>
        /// <returns>returns the details of the added user</returns>
        public ResponseModel AddUser(ConnectionModel credits, UserModel userModel)
        {
            DataTable dataUser = new DataTable();
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                dataUser = userAccess.AddUser(userModel);
                this._response.Data = dataUser;

                if (Convert.ToInt32(dataUser.Rows[0]["USER_ID"]) > 0)
                {
                    this._response.Message = "User has been added successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User was not added successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified user
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userModel">Model object for user</param>
        /// <returns>returns the details of the added user</returns>
        public ResponseModel ModifyUser(ConnectionModel credits, UserModel userModel)
        {
            DataTable dataUser = new DataTable();
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                dataUser = userAccess.ModifyUser(userModel);
                this._response.Data = dataUser;

                if (dataUser.Rows[0]["USER_ID"].ToString() == userModel.Id.ToString())
                {
                    this._response.Message = "User has been updated successfully";
                    this._response.Status = 1;
                }
                else
                {
                    this._response.Message = "User was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Updates the specified user's password
        /// </summary>
        /// <param name="credits">user credentials</param>
        /// <param name="password">new password of the user</param>
        /// <param name="oldPassword">old password of the user</param>
        /// <returns>returns the details of the updated user</returns>
        public ResponseModel ModifyUserPassword(ConnectionModel credits, string password, string oldPassword)
        {
            DataTable dataUser = new DataTable();
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                dataUser = userAccess.ModifyUserPassword(password, oldPassword);
                this._response.Data = dataUser;

                if (dataUser.Rows[0]["PASSWORD"].ToString() == password)
                {
                    this._response.Message = "User password has been updated";
                    this._response.Status = 1;
                }
                else if (dataUser.Rows[0]["PASSWORD"].ToString() != oldPassword)
                {
                    this._response.Message = "Old password is incorrect";
                    this._response.Status = 0;
                }
                else
                {
                    this._response.Message = "User password was not updated successfully";
                    this._response.Status = 0;
                }
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userId">id of the specified user</param>
        /// <returns>returns a data table containing the details of the specified user</returns>
        public ResponseModel GetUserById(ConnectionModel credits, int userId)
        {
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                this._response.Data = userAccess.GetUser(userId);
                this._response.Message = "Specific user details was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// gets the user details of all users
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <returns>returns JSON string composed of status and other needed objects</returns>
        public ResponseModel GetAllUser(ConnectionModel credits)
        {
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                this._response.Data = userAccess.GetAllUser();
                this._response.Message = "All users was loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }

        /// <summary>
        /// Dynamic Data Access that gets all user allowed modules
        /// </summary>
        /// <param name="credits">Connection Model Details[Key]</param>
        /// <param name="userId">ID of the user</param>
        /// <returns>returns a data table containing the details of all allowed modules of the user</returns>
        public ResponseModel GetUserModules(ConnectionModel credits, int userId)
        {
            Core.DataAccess.UserAccess userAccess = new Core.DataAccess.UserAccess(credits);

            try
            {
                this._response.Data = userAccess.GetUserModules(userId);
                this._response.Message = "Modules are loaded successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}
