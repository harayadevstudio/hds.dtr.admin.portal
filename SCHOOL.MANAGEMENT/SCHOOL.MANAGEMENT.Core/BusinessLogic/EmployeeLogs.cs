﻿namespace SCHOOL.MANAGEMENT.Core.BusinessLogic
{
    using SCHOOL.MANAGEMENT.Core.DataAccess;
    using SCHOOL.MANAGEMENT.Core.Model;
    using System;
    using System.Data;
    public class EmployeeLogs
    {
        private ResponseModel _response = new ResponseModel();

        public ResponseModel GetEmployeeCount(ConnectionModel credits)
        {
            EmployeeLogsAccess employeeLogsAccess = new EmployeeLogsAccess(credits);

            try
            {
                this._response.Data = employeeLogsAccess.GetEmployeeCount();
                this._response.Message = "Employee Counted Successfully";
                this._response.Status = 1;
            }
            catch (Exception ex)
            {
                this._response.Status = 2;
                this._response.Message = ErrorValidation.ErrorMsg(ex.Message);
            }

            return this._response;
        }
    }
}
