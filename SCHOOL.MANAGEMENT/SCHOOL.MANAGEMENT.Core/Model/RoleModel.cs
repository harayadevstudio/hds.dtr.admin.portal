﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Role
    /// </summary>
    public class RoleModel
    {
        /// <summary>
        /// Gets or sets ID of the Role
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the Role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Description of the Role
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Creates the Information
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Modifies the Information
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets Status of the Role
        /// </summary>
        public int IsActive { get; set; }
    }
}