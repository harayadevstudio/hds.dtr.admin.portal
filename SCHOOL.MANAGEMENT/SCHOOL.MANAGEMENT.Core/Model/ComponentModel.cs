namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Component Model
    /// </summary>
    public class ComponentModel
    {
        /// <summary>
        /// Gets or sets Component Id
        /// </summary>
        public int ComponentId { get; set; }

        /// <summary>
        /// Gets or sets Component Name
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// Gets or sets Component Icon
        /// </summary>
        public string ComponentDesc { get; set; }

        /// <summary>
        /// Gets or sets ComponentIcon
        /// </summary>
        public string ComponentIcon { get; set; }

        /// <summary>
        /// Gets or sets IsShown
        /// </summary>
        public int IsShown { get; set; }

        /// <summary>
        /// Gets or sets Data Id
        /// </summary>
        public int DataId { get; set; }

        /// <summary>
        /// Gets or sets Data Value
        /// </summary>
        public string DataValue { get; set; }

        /// <summary>
        /// Gets or sets Data Key Id
        /// </summary>
        public int DataKeyId { get; set; }

        /// <summary>
        /// Gets or sets Metadata ID
        /// </summary>
        public int MetadataId { get; set; }

        /// <summary>
        /// Gets or sets Unique Indentifier
        /// </summary>
        public int UniqueInd { get; set; }

        /// <summary>
        /// Gets or sets Component Map Id
        /// </summary>
        public int CompMapId { get; set; }
    }
}
