﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Action Information
    /// </summary>
    public class ActionModel
    {
        /// <summary>
        /// Gets or sets ID of the Action
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the Action
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Description of the Action
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Creates the Information
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Modifies the Information
        /// </summary>
        public int UpdatedBy { get; set; }
    }
}