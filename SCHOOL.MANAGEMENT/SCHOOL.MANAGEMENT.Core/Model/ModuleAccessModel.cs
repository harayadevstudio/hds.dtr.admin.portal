﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Role Access
    /// </summary>
    public class ModuleAccessModel
    {
        /// <summary>
        /// Gets or sets ID of the Role Access
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets ID of the Module Action
        /// </summary>
        public int ModuleActionId { get; set; }

        /// <summary>
        /// Gets or sets ID of the User Role Access
        /// </summary>
        public int UserRoleAccessId { get; set; }
    }
}