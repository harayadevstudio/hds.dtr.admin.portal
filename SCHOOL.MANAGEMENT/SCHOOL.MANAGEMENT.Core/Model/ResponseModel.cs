﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Response
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Gets or sets response id returns
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets data
        /// </summary>
        public object Data { get; set; }
    }
}