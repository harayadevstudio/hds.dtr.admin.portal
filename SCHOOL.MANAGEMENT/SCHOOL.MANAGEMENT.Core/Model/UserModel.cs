﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for User
    /// </summary>
    public class UserModel
    {
        /// <summary>
        /// Gets or sets ID of the User
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Username of the User
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets Password of the User
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets First Name of the User
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets Middle Name of the User
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets Last Name of the User
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Status of the User
        /// </summary>
        public int IsActive { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Creates the Information
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Modifies the Information
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets Employee ID of the User
        /// </summary>
        public int EmployeeId { get; set; }
    }
}