namespace SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel
{
    using Oracle.DataAccess.Client;
    using Oracle.DataAccess.Types;
    using System;







    /// <summary>
    /// ComponentData Object Class
    /// </summary>
    public class ComponentDataObject : IOracleCustomType
    {






        /// <summary>
        /// Gets or sets Oracle Object Mapping DataId
        /// </summary>
        [OracleObjectMapping("DATA_ID")]
        public int DataId { get; set; }







        /// <summary>
        /// Gets or sets Oracle Object Mapping DataValue
        /// </summary>
        [OracleObjectMapping("DATA_VALUE")]
        public string DataValue { get; set; }







        /// <summary>
        /// Gets or sets Oracle Object Mapping DataPrimaryKey
        /// </summary>
        [OracleObjectMapping("DATA_PRIMARY_KEY")]
        public int DataPrimaryKey { get; set; }







        /// <summary>
        /// Gets or sets Oracle Object Mapping MetadataId
        /// </summary>
        [OracleObjectMapping("METADATA_ID")]
        public int MetadataId { get; set; }







        /// <summary>
        /// Gets or sets Oracle Object Mapping ComponentId
        /// </summary>
        [OracleObjectMapping("COMPONENT_ID")]
        public int ComponentId { get; set; }







        /// <summary>
        /// Gets or sets Oracle Object Mapping CreatedBy
        /// </summary>
        [OracleObjectMapping("CREATED_BY")]
        public int CreatedBy { get; set; }











        /// <summary>
        /// From Custom ComponentData Object
        /// </summary>
        /// <param name="con">Oracle Connection</param>
        /// <param name="parameter">Integer Parameter</param>
        public void FromCustomObject(OracleConnection con, IntPtr parameter)
        {
            OracleUdt.SetValue(con, parameter, "DATA_ID", this.DataId);

            OracleUdt.SetValue(con, parameter, "DATA_VALUE", this.DataValue);

            OracleUdt.SetValue(con, parameter, "DATA_PRIMARY_KEY", this.DataPrimaryKey);

            OracleUdt.SetValue(con, parameter, "METADATA_ID", this.MetadataId);

            OracleUdt.SetValue(con, parameter, "COMPONENT_ID", this.ComponentId);

            OracleUdt.SetValue(con, parameter, "CREATED_BY", this.CreatedBy);

        }










        /// <summary>
        /// To Custom ComponentData Object
        /// </summary>
        /// <param name="con">Oracle Connection</param>
        /// <param name="parameter">Integer Parameter</param>
        public void ToCustomObject(OracleConnection con, IntPtr parameter)
        {
            this.DataId = Convert.ToInt32(OracleUdt.GetValue(con, parameter, "DATA_ID"));

            this.DataValue = Convert.ToString(OracleUdt.GetValue(con, parameter, "DATA_VALUE"));

            this.DataPrimaryKey = Convert.ToInt32(OracleUdt.GetValue(con, parameter, "DATA_PRIMARY_KEY"));

            this.MetadataId = Convert.ToInt32(OracleUdt.GetValue(con, parameter, "METADATA_ID"));

            this.ComponentId = Convert.ToInt32(OracleUdt.GetValue(con, parameter, "COMPONENT_ID"));

            this.CreatedBy = Convert.ToInt32(OracleUdt.GetValue(con, parameter, "CREATED_BY"));

        }
    }
}