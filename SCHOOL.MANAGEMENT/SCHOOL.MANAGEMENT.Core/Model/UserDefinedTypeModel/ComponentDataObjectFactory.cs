namespace SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel
{
    using Oracle.DataAccess.Types;







    /// <summary>
    /// ComponentData Object Factory Mapping for Oracle Type
    /// </summary>
    [OracleCustomTypeMapping(".COMPONENT_DATA_OBJ")]
    public class ComponentDataObjectFactory : IOracleCustomTypeFactory
    {








        /// <summary>
        /// Create ComponentData Object 
        /// </summary>
        /// <returns>Return ComponentData</returns>
        public IOracleCustomType CreateObject()
        {
            ComponentDataObject obj = new ComponentDataObject();
            return obj;
        }
    }
}