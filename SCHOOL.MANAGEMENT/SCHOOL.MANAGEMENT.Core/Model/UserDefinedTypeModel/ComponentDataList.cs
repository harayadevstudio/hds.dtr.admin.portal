namespace SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel
{
    using Oracle.DataAccess.Types;
    using System;







    /// <summary>
    /// ComponentData List Class Model
    /// </summary>
    public class ComponentDataList : IOracleCustomType
    {






        /// <summary>
        ///  Gets or sets ComponentData Object List Mapping 
        /// </summary>
        [OracleArrayMapping]
        public ComponentDataObject[] ComponentDataObjectList { get; set; }











        /// <summary>
        /// From Custom  ComponentData List Object
        /// </summary>
        /// <param name="con">Oracle Connection</param>
        /// <param name="parameter">Integer Parameter</param>
        public void FromCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr parameter)
        {
            OracleUdt.SetValue(con, parameter, 0, this.ComponentDataObjectList);
        }











        /// <summary>
        /// To Custom  ComponentData List Object
        /// </summary>
        /// <param name="con">Oracle Connection</param>
        /// <param name="parameter">Integer Parameter</param>
        public void ToCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr parameter)
        {
            this.ComponentDataObjectList = ((ComponentDataObject[])(OracleUdt.GetValue(con, parameter, 0)));
        }
    }
}