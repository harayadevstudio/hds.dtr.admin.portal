namespace SCHOOL.MANAGEMENT.Core.UserDefinedTypeModel
{
    using Oracle.DataAccess.Types;
    using System;







    /// <summary>
    /// ComponentData List Factory Mapping for Oracle Type
    /// </summary>
    [OracleCustomTypeMapping(".COMPONENT_DATA_LIST")]
    public class ComponentDataListFactory : IOracleCustomTypeFactory, IOracleArrayTypeFactory
    {










        /// <summary>
        /// Create ComponentData Array 
        /// </summary>
        /// <param name="numElems">Number of the Elements</param>
        /// <returns>Return ComponentDataObject array</returns>
        public Array CreateArray(int numElems)
        {
            return new ComponentDataObject[numElems];
        }









        /// <summary>
        /// Create ComponentData Object 
        /// </summary>
        /// <returns>Return ComponentDataList Object</returns>
        public IOracleCustomType CreateObject()
        {
            return new ComponentDataList();
        }











        /// <summary>
        /// Create Status Array
        /// </summary>
        /// <param name="numElems">Number of the Elements</param>
        /// <returns>Return null</returns>
        public Array CreateStatusArray(int numElems)
        {
            return null;
        }
    }
}