﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Module
    /// </summary>
    public class ModuleModel
    {
        /// <summary>
        /// Gets or sets ID of the Module
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the Module
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Description of the Module
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Creates the Information
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID of the User who Modifies the Information
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID of the Parent Module
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or sets URL Page of the Module
        /// </summary>
        public string ModulePage { get; set; }

        /// <summary>
        /// Gets or sets Status of the Module
        /// </summary>
        public int IsActive { get; set; }

        /// <summary>
        /// Gets or sets Shown Status of the Module
        /// </summary>
        public int IsShown { get; set; }

        /// <summary>
        /// Gets or sets Sort Number of the Module
        /// </summary>
        public int SortNo { get; set; }

        /// <summary>
        /// Gets or sets Font Awesome Icon Class of the Module
        /// </summary>
        public string Icon { get; set; }
    }
}