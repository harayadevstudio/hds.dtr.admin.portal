﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Data Access
    /// </summary>
    public class DataAccessModel
    {
        /// <summary>
        /// Gets or sets ID of the Data Access
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets ID of the User
        /// </summary>
        public int UserAccessId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Data Hierarchy
        /// </summary>
        public int DataHierarchyId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Module
        /// </summary>
        public int? ModuleId { get; set; }
    }
}