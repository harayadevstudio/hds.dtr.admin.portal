﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Data Hierarchy
    /// </summary>
    public class DataHierarchyModel
    {
        /// <summary>
        /// Gets or sets ID of the Data Hierarchy
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets ID of the Reference
        /// </summary>
        public int? ReferenceId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Parent Data Hierarchy
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Data Identifier
        /// </summary>
        public int DataIdentifierId { get; set; }
    }
}