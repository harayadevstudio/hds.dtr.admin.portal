﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Contains all parameters needed for User Access
    /// </summary>
    public class UserAccessModel
    {
        /// <summary>
        /// Gets or sets user Id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets Role Id
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// Gets or sets Org Id
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// Gets or sets UserAccess Id
        /// </summary>
        public int UserAccessId { get; set; }
    }
}