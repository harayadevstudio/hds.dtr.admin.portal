﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Model for Organization Hierarchy
    /// </summary>
    public class OrgHierarchyModel
    {
        /// <summary>
        /// Gets or sets ID of the Organization Hierarchy
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets ID of the Parent Organization Hierarchy
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Reference Data
        /// </summary>
        public int ReferenceId { get; set; }

        /// <summary>
        /// Gets or sets ID of the Hierarchy Identifier
        /// </summary>
        public int HierarchyIdentifierId { get; set; }
    }
}