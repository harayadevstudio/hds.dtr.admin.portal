﻿namespace SCHOOL.MANAGEMENT.Core.Model
{
    /// <summary>
    /// Connection Model for Credential Access
    /// </summary>
    public class ConnectionModel
    {
        /// <summary>
        /// Gets or sets Host / IP Address for Database Connection
        /// </summary>
        public static string Host { get; set; }

        /// <summary>
        /// Gets or sets Port for Database Connection
        /// </summary>
        public static string Port { get; set; }

        /// <summary>
        /// Gets or sets Catalog for Database Connection
        /// </summary>
        public static string Catalog { get; set; }

        /// <summary>
        /// Gets or sets Schema Name for Database Connection
        /// </summary>
        public static string Schema { get; set; }

        /// <summary>
        /// Gets or sets RMS Schema Name for Database Connection
        /// </summary>
        public static string RMSSchema { get; set; }

        /// <summary>
        /// Gets or sets Username Credential for Database Connection
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets Password for Table Base Credential
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets Password Credential for Database Connection
        /// </summary>
        public string DBPassword { get; set; }

        /// <summary>
        /// Gets or sets ID of the User Details
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets Encrypted Key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets Cookie Timestamp
        /// </summary>
        public string TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets organization Id
        /// </summary>
        public int OrganizationId { get; set; }
    }
}