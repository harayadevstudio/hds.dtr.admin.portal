﻿namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using System;
    using Enyim.Caching;
    using System.Configuration;
    using Core.Model;
    using Newtonsoft.Json;
    using Core;

    public class CookieValidator
    {
        /// <summary>
        /// Validate Cookie Status
        /// </summary>
        /// <param name="requestCookie">Value of Request Cookie</param>
        /// <returns>Returns Cookie Status</returns>
        public CookieStatus ValidateCookie(string requestCookie)
        {
            if (!string.IsNullOrEmpty(requestCookie))
            {
                // Decrypt Request Cookie
                // Validate If Cookie has valid Format
                bool isCookieValid = true;
                if (isCookieValid)
                {
                    using (MemcachedClient memCached = new MemcachedClient())
                    {
                        DatabaseConnection encryption = new DatabaseConnection();
                        ConnectionModel userCreds = JsonConvert.DeserializeObject<ConnectionModel>(encryption.Decrypt(requestCookie));

                        string cacheTimeStamp = memCached.Get<string>(GeneralModel.CookieName + "_" + userCreds.UserId);

                        DateTime reqTimeStamp = Convert.ToDateTime(userCreds.TimeStamp);

                        if (!string.IsNullOrEmpty(cacheTimeStamp))
                        {
                            if (DateTime.Compare(reqTimeStamp, Convert.ToDateTime(cacheTimeStamp)) == 0)
                            {
                                return CookieStatus.IsValid;
                            }
                            else
                            {
                                return CookieStatus.IsExpired;
                            }
                        }
                        else
                        {
                            return CookieStatus.IsNull;
                        }
                    }
                }
                else
                {
                    return CookieStatus.IsInvalid;
                }
            }
            else
            {
                return CookieStatus.IsNull;
            }
        }
    }
}
