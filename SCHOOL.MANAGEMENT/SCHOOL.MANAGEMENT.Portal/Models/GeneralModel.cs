﻿namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Dynamic;
    using System.Linq;
    using System.Web;

    public class GeneralModel
    {
        public static string CookieName
        {
            get
            {
                return ConfigurationManager.AppSettings["AppName"] + "_Token";
            }
        }

        /// <summary>
        /// Create expando from object
        /// </summary>
        /// <param name="source">source object to be converted</param>
        /// <returns>generated expando object</returns>
        public static ExpandoObject CreateExpandoFromObject(object source)
        {
            var result = new ExpandoObject();
            IDictionary<string, object> dictionary = result;
            foreach (var property in source
                .GetType()
                .GetProperties()
                .Where(p => p.CanRead && p.GetMethod.IsPublic))
            {
                dictionary[property.Name] = property.GetValue(source, null);
            }

            return result;
        }
    }
}