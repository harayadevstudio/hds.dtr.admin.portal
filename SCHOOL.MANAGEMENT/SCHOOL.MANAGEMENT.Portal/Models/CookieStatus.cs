﻿namespace SCHOOL.MANAGEMENT.Portal.Models
{
    /// <summary>
    /// Enumeration of Cookie Status
    /// </summary>
    public enum CookieStatus
    {
        /// <summary>
        /// Enumerataon of Is Valid
        /// </summary>
        IsValid,

        /// <summary>
        /// Enumerataon of Is Expired
        /// </summary>
        IsExpired,

        /// <summary>
        /// Enumerataon of Is Invalid
        /// </summary>
        IsInvalid,

        /// <summary>
        /// Enumerataon of Is Null
        /// </summary>
        IsNull
    }
}