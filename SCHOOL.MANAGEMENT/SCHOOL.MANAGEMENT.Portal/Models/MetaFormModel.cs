namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Meta Form Model
    /// </summary>
    public class MetaFormModel
    {
        /// <summary>
        /// Gets or sets Meta Class Name
        /// </summary>
        internal string ClassName { get; set; }

        /// <summary>
        /// Gets or sets Element Id
        /// </summary>
        internal string ElementId { get; set; }

        /// <summary>
        /// Gets or sets First Data Name
        /// </summary>
        internal string FirstDataName { get; set; }

        /// <summary>
        /// Gets or sets First Data Values
        /// </summary>
        internal string FirstDataValue { get; set; }

        /// <summary>
        /// Gets or sets Second Data Name
        /// </summary>
        internal string SecondDataName { get; set; }

        /// <summary>
        /// Gets or sets Second Data Value
        /// </summary>
        internal string SecondDataValue { get; set; }
    }
}