namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.Optimization;

    public static class BundleFilter
    {
        public static Bundle IncludeDirectoryWithExclusion(this StyleBundle bundle, string directoryVirtualPath, string searchPattern, params string[] fileToExclude)
        {
            var folderPath = HttpContext.Current.Server.MapPath(directoryVirtualPath);
            var virtualPath = string.Empty;
            var fileName = string.Empty;

            foreach (var file in Directory.GetFiles(folderPath, searchPattern, SearchOption.AllDirectories))
            {
                fileName = Path.GetFileName(file);

                if (!String.IsNullOrEmpty(Array.Find(fileToExclude, s => s.ToLower() == fileName.ToLower())))
                {
                    continue;
                }

                virtualPath = Path.GetDirectoryName(file).Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");

                bundle.Include(virtualPath + "/" + fileName);
            }

            return bundle;
        }
    }
}