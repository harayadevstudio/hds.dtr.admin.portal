﻿namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using Core.BusinessLogic;
    using Core.Model;
    using System.Configuration;
    using System.Data;
    using System.Web.Mvc;

    /// <summary>
    /// View Module Filter
    /// </summary>
    public class ViewModuleFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Declaration of Connection Model Credential
        /// </summary>
        private ConnectionModel _credits = new ConnectionModel();

        /// <summary>
        /// Information Details Model for Private Response
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// On Result Executing
        /// </summary>
        /// <param name="filterContext">Result Executing Context</param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            ObjectConversion convert = new ObjectConversion();
            UserAccess userAccess = new UserAccess();
            DataTable data = new DataTable();
            Core.DatabaseConnection oracle = new Core.DatabaseConnection();

            if (filterContext.HttpContext.Request.Cookies[GeneralModel.CookieName] != null)
            {
                string requestKey = filterContext.HttpContext.Request.Cookies[GeneralModel.CookieName].Value.ToString();

                this._credits.Key = requestKey;
                this._response = userAccess.GetUserModules(this._credits);

                filterContext.Controller.ViewBag.Username = this._credits.Username;
                filterContext.Controller.ViewBag.ModuleList = convert.ConvertObject(this._response.Data);

                DataTable moduleDT = convert.ConvertObject(this._response.Data);

                //string requestPage1 = moduleDT.Rows[0]["MODULE_PAGE"].ToString();

                string requestPage = "/" + filterContext.RouteData.Values["controller"].ToString() + "/" + filterContext.RouteData.Values["action"].ToString();

                DataTable modules = new DataTable();
                modules = filterContext.Controller.ViewBag.ModuleList;

                DataRow[] module = modules.Select("MODULE_PAGE = '" + requestPage + "'");
                if (module.Length > 0)
                {
                    filterContext.Controller.ViewBag.Title = module[0]["MODULE_NAME"].ToString();
                }
            }
        }
    }
}