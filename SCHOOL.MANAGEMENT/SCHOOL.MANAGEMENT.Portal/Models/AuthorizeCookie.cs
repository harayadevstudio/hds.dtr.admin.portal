﻿namespace SCHOOL.MANAGEMENT.Portal.Models
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Portal.Models;

    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeCookie : AuthorizeAttribute
    {
        CookieStatus _cookieStatus = CookieStatus.IsInvalid;

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            string cookieName = GeneralModel.CookieName;
            bool isAjaxRequest = filterContext.HttpContext.Request.IsAjaxRequest();

            if (_cookieStatus == CookieStatus.IsExpired)
                if (!isAjaxRequest)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", Action = "SessionExpired" }));
                }
                else
                {
                    filterContext.Result = new ContentResult();
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.HttpContext.Response.StatusDescription = "Your session is expired. Please login again.";
                }
            else // when cookie is null or invalid
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", Action = "Index" }));
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string cookieName = GeneralModel.CookieName;
            if (httpContext.Request.Cookies[cookieName] != null)
            {
                string requestKey = httpContext.Request.Cookies[cookieName].Value.ToString();

                CookieValidator cookieValidator = new CookieValidator();
                this._cookieStatus = cookieValidator.ValidateCookie(requestKey);

                if (this._cookieStatus == CookieStatus.IsValid)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}