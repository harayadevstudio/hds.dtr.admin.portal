﻿namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using Core;
    using Core.BusinessLogic;
    using Core.Model;
    using Portal.Models;
    using Newtonsoft.Json;
    using System.Web.Mvc;

    /// <summary>
    /// User Manager Controller
    /// </summary>
    [AuthorizeCookie]
    public class UserManagerController : Controller
    {
        /// <summary>
        /// Information Details Model for Private Response
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Declaration of Encryption
        /// </summary>
        private DatabaseConnection _encrypt = new DatabaseConnection();

        /// <summary>
        /// Declaration of Connection Model Credential
        /// </summary>
        private ConnectionModel _credits = new ConnectionModel();

        /// <summary>
        /// User Manger Page View
        /// </summary>
        /// <returns>Returns HTML Elements</returns>
        [ViewModuleFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "User Manager";
            ViewBag.ModuleName = "User Manager";

            return this.View();
        }

        #region "Get User Data Module Access"

        /// <summary>
        /// Gets All User Data Access Allowed Modules
        /// </summary>
        /// <returns>Returns List of Data containing the details of all allowed Modules of the User</returns>
        //// [CheckPageAccess, HttpPost]
        //// [HttpPost]
        public object GetUserModules()
        {
            UserAccess user = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.GetUserModules(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "Get User Data Module Access"

        #region "User Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Gets All User Information
        /// </summary>
        /// <returns>Returns list of data containing the details of all users found on the database</returns>
        [HttpPost]
        public object GetAllUsers()
        {
            User user = new User();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.GetAllUser(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Gets the Specific User Information
        /// </summary>
        /// <param name="userId">ID of the Specified User</param>
        /// <returns>Returns a data containing the details of the specified user</returns>
        [HttpPost]
        public object GetUser(int userId)
        {
            User user = new User();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.GetUserById(this._credits, userId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds New User Information
        /// </summary>
        /// <param name="userModel">User Model Details Object</param>
        /// <returns>Returns the details of the added user</returns>
        [HttpPost]
        public object AddUser(UserModel userModel)
        {
            User user = new User();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.AddUser(this._credits, userModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the Information of Specific User
        /// </summary>
        /// <param name="userModel">User Model Details Object</param>
        /// <returns>Returns the details of the modified user</returns>
        [HttpPost]
        public object ModifyUser(UserModel userModel)
        {
            User user = new User();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.ModifyUser(this._credits, userModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the Password of Specific User
        /// </summary>
        /// <param name="userId">ID of the Specific User</param>
        /// <param name="password">New Password of the Specific User</param>
        /// <param name="oldPassword">Old Password of the Specific User</param>
        /// <returns>Returns the details of the updated user information</returns>
        [HttpPost]
        public object ModifyUserPassword(string password, string oldPassword)
        {
            User user = new User();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = user.ModifyUserPassword(this._credits, password, oldPassword);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "User Information [GET|ADD|UPDATE]"

        #region "Module Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Gets All Module Information
        /// </summary>
        /// <returns>Returns list of data containing the information of all modules found on the database</returns>
        [HttpPost]
        public object GetAllModules()
        {
            Module module = new Module();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = module.GetAllModules(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module
        /// </summary>
        /// <param name="moduleId">id of the specified module</param>
        /// <returns>returns a data table containing the details of the specified module</returns>
        [HttpPost]
        public object GetModule(int moduleId)
        {
            Module module = new Module();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = module.GetModule(this._credits, moduleId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all parent modules
        /// </summary>
        /// <returns>returns a data table containing the details of all parent modules found on the database</returns>
        [HttpPost]
        public object GetParentModules()
        {
            Module module = new Module();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = module.GetParentModules(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new module
        /// </summary>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the action added</returns>
        [HttpPost]
        public object AddModule(ModuleModel moduleModel)
        {
            Module module = new Module();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = module.AddModule(this._credits, moduleModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified module
        /// </summary>
        /// <param name="moduleModel">Model object for module</param>
        /// <returns>returns the details of the modified module</returns>
        [HttpPost]
        public object ModifyModule(ModuleModel moduleModel)
        {
            Module module = new Module();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = module.ModifyModule(this._credits, moduleModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "Module Information [GET|ADD|UPDATE]"

        #region "Action Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Dynamic Data Access that gets the details of all actions
        /// </summary>
        /// <returns>returns a data table containing the details of all actions found on the database</returns>
        [HttpPost]
        public object GetAllActions()
        {
            Core.BusinessLogic.Action action = new Core.BusinessLogic.Action();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = action.GetAllActions(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of the selected action
        /// </summary>
        /// <param name="actionId">id of the selected action</param>
        /// <returns>returns the details of the specified action</returns>
        [HttpPost]
        public object GetAction(int actionId)
        {
            Core.BusinessLogic.Action action = new Core.BusinessLogic.Action();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = action.GetAction(this._credits, actionId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new action
        /// </summary>
        /// <param name="actionModel">Model object for action</param>
        /// <returns>returns the details of the action added</returns>
        [HttpPost]
        public object AddAction(ActionModel actionModel)
        {
            Core.BusinessLogic.Action action = new Core.BusinessLogic.Action();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = action.AddAction(this._credits, actionModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified action
        /// </summary>
        /// <param name="actionModel">Model object for action</param>
        /// <returns>returns the details of the modified action</returns>
        [HttpPost]
        public object ModifyAction(ActionModel actionModel)
        {
            Core.BusinessLogic.Action action = new Core.BusinessLogic.Action();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = action.ModifyAction(this._credits, actionModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "Action Information [GET|ADD|UPDATE]"

        #region "Module Action Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Dynamic Data Access that gets the details of all module actions
        /// </summary>
        /// <returns>returns a data table containing the details of all module actions found on the database</returns>
        //// [HttpPost]
        public object GetAllModuleActions()
        {
            ModuleAction moduleAction = new ModuleAction();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = moduleAction.GetAllModuleActions(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified module action
        /// </summary>
        /// <param name="moduleActionId">id of the specified module action</param>
        /// <returns>returns a data table containing the details of the specified module action</returns>
        //// [HttpPost]
        public object GetModuleAction(int moduleActionId)
        {
            ModuleAction moduleAction = new ModuleAction();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = moduleAction.GetModuleAction(this._credits, moduleActionId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new module action
        /// </summary>
        /// <param name="moduleId">Id of the selected module</param>
        /// <param name="actionId">Id of the selected action</param>
        /// <param name="userId">Id of the creator</param>
        /// <returns>returns the details of the added module action</returns>
        //// [HttpPost]
        public object AddModuleAction(int moduleId, int actionId, int userId)
        {
            ModuleAction moduleAction = new ModuleAction();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = moduleAction.AddModuleAction(this._credits, moduleId, actionId, userId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified module action
        /// </summary>
        /// <param name="moduleActionId">id of the module action</param>
        /// <param name="moduleId">id of the selected module</param>
        /// <param name="actionId">id of the selected action</param>
        /// <returns>returns the details of the modified module action</returns>
        //// [HttpPost]
        public object ModifyModuleAction(int moduleActionId, int moduleId, int actionId)
        {
            ModuleAction moduleAction = new ModuleAction();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = moduleAction.ModifyModuleAction(this._credits, moduleActionId, moduleId, actionId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "Module Action Information [GET|ADD|UPDATE]"

        #region "User Role Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Dynamic Data Access that gets the details of all user roles
        /// </summary>
        /// <returns>returns a data table containing the details of all user roles found on the database</returns>
        //// [HttpPost]
        public object GetAllUserRoles()
        {
            Role role = new Role();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = role.GetAllUserRoles(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role
        /// </summary>
        /// <param name="roleId">id of the specified role</param>
        /// <returns>returns a data table containing the details of the specified role</returns>
        //// [HttpPost]
        public object GetUserRole(int roleId)
        {
            Role role = new Role();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = role.GetUserRole(this._credits, roleId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new user role
        /// </summary>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the added user role</returns>
        //// [HttpPost]
        public object AddUserRole(RoleModel roleModel)
        {
            Role role = new Role();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = role.AddUserRole(this._credits, roleModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified user role
        /// </summary>
        /// <param name="roleModel">Model object for role</param>
        /// <returns>returns the details of the modified user role</returns>
        //// [HttpPost]
        public object ModifyUserRole(RoleModel roleModel)
        {
            Role role = new Role();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = role.ModifyUserRole(this._credits, roleModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        #endregion "User Role Information [GET|ADD|UPDATE]"

        /// <summary>
        /// Dynamic Data Access that gets all user access
        /// </summary>
        /// <returns>returns a data table containing the details of all user access found on the database</returns>
        //// [HttpPost]
        public object GetAllUserAccess()
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.GetAllUserAccess(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified user access
        /// </summary>
        /// <param name="userAccessId">id of the specified user access</param>
        /// <returns>returns a data table containing the details of the specified user access</returns>
        //// [HttpPost]
        public object GetUserAccess(int userAccessId)
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.GetUserAccess(this._credits, userAccessId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new user access
        /// </summary>
        /// <param name="modelUserAccess">Module User Access Details</param>
        /// <returns>returns the details of the added user access</returns>
        //// [HttpPost]
        public object AddUserAccess(UserAccessModel modelUserAccess)
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.AddUserAccess(this._credits, modelUserAccess);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified user access
        /// </summary>
        /// <param name="modelUserAccess">Module User Access Details</param>
        /// <returns>returns the details of the added user access</returns>
        //// [HttpPost]
        public object ModifyUserAccess(UserAccessModel modelUserAccess)
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.ModifyUserAccess(this._credits, modelUserAccess);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierDesc">description for the new hierarchy identifier</param>
        /// <returns>returns the details of the hierarchy identifier added</returns>
        //// [HttpPost]
        public object AddHierarchyIdentifier(string hierarchyIdentifierDesc)
        {
            HierarchyIdentifier hierarchyIdentifier = new HierarchyIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = hierarchyIdentifier.AddHierarchyIdentifier(this._credits, hierarchyIdentifierDesc);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new data identifier
        /// </summary>
        /// <param name="dataIdentifierDesc">description for the new data identifier</param>
        /// <returns>returns the details of the data identifier added</returns>
        //// [HttpPost]
        public object AddDataIdentifier(string dataIdentifierDesc)
        {
            DataIdentifier dataIdentifier = new DataIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataIdentifier.AddDataIdentifier(this._credits, dataIdentifierDesc);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the organization hierarchy added</returns>
        //// [HttpPost]
        public object AddOrgHierarchy(OrgHierarchyModel orgHierarchyModel)
        {
            OrgHierarchy orgHierarchy = new OrgHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = orgHierarchy.AddOrgHierarchy(this._credits, orgHierarchyModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new role access
        /// </summary>
        /// <param name="moduleAccessModel">Model object for module access</param>
        /// <returns>returns the details of the added role access</returns>
        //// [HttpPost]
        public object AddModuleAccess(ModuleAccessModel moduleAccessModel)
        {
            RoleAccess roleAccess = new RoleAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = roleAccess.AddModuleAccess(this._credits, moduleAccessModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new data hierarchy
        /// </summary>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the data hierarchy added</returns>
        //// [HttpPost]
        public object AddDataHierarchy(DataHierarchyModel dataHierarchyModel)
        {
            DataHierarchy dataHierarchy = new DataHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataHierarchy.AddDataHierarchy(this._credits, dataHierarchyModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Adds new data access
        /// </summary>
        /// <param name="dataAccessModel">Model object for data access</param>
        /// <returns>returns the details of the data access added</returns>
        //// [HttpPost]
        public object AddDataAccess(DataAccessModel dataAccessModel)
        {
            DataAccess dataAccess = new DataAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataAccess.AddDataAccess(this._credits, dataAccessModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId">id of the selected hierarchy identifier</param>
        /// <param name="description">description of the hierarchy identifier</param>
        /// <returns>returns the details of the modified hierarchy identifier</returns>
        //// [HttpPost]
        public object ModifyHierarchyIdentifier(int hierarchyIdentifierId, string description)
        {
            HierarchyIdentifier hierarchyIdentifier = new HierarchyIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = hierarchyIdentifier.ModifyHierarchyIdentifier(this._credits, hierarchyIdentifierId, description);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified data identifier
        /// </summary>
        /// <param name="dataIdentifierId">id of the selected data identifier</param>
        /// <param name="description">description of the data identifier</param>
        /// <returns>returns the details of the modified data identifier</returns>
        //// [HttpPost]
        public object ModifyDataIdentifier(int dataIdentifierId, string description)
        {
            DataIdentifier dataIdentifier = new DataIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataIdentifier.ModifyDataIdentifier(this._credits, dataIdentifierId, description);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyModel">Model object for organization hierarchy</param>
        /// <returns>returns the details of the modified organization hierarchy</returns>
        //// [HttpPost]
        public object ModifyOrgHierarchy(OrgHierarchyModel orgHierarchyModel)
        {
            OrgHierarchy orgHierarchy = new OrgHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = orgHierarchy.ModifyOrgHierarchy(this._credits, orgHierarchyModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified role access
        /// </summary>
        /// <param name="modelModuleAccess">Model object for module access</param>
        /// <returns>returns the details of the modified role access</returns>
        //// [HttpPost]
        public object ModifyModuleAccess(ModuleAccessModel modelModuleAccess)
        {
            RoleAccess roleAccess = new RoleAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = roleAccess.ModifyModuleAccess(this._credits, modelModuleAccess);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified data hierarchy
        /// </summary>
        /// <param name="dataHierarchyModel">Model object for data hierarchy</param>
        /// <returns>returns the details of the modified data hierarchy</returns>
        //// [HttpPost]
        public object ModifyDataHierarchy(DataHierarchyModel dataHierarchyModel)
        {
            DataHierarchy dataHierarchy = new DataHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataHierarchy.ModifyDataHierarchy(this._credits, dataHierarchyModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Updates the specified data access
        /// </summary>
        /// <param name="dataAccessModel">Model object for data access</param>
        /// <returns>returns the details of the modified data access</returns>
        //// [HttpPost]
        public object ModifyDataAccess(DataAccessModel dataAccessModel)
        {
            DataAccess dataAccess = new DataAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataAccess.ModifyDataAccess(this._credits, dataAccessModel);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified hierarchy identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId">id of the specified hierarchy identifier</param>
        /// <returns>returns a data table containing the details of the specified hierarchy identifier</returns>
        //// [HttpPost]
        public object GetHierarchyIdentifier(int hierarchyIdentifierId)
        {
            HierarchyIdentifier hierarchyIdentifier = new HierarchyIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = hierarchyIdentifier.GetHierarchyIdentifier(this._credits, hierarchyIdentifierId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data identifier
        /// </summary>
        /// <param name="dataIdentifierId">id of the specified data identifier</param>
        /// <returns>returns a data table containing the details of the specified data identifier</returns>
        //// [HttpPost]
        public object GetDataIdentifier(int dataIdentifierId)
        {
            DataIdentifier dataIdentifier = new DataIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataIdentifier.GetDataIdentifier(this._credits, dataIdentifierId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified organization hierarchy
        /// </summary>
        /// <param name="orgHierarchyId">id of the specified organization hierarchy</param>
        /// <returns>returns a data table containing the details of the specified organization hierarchy</returns>
        //// [HttpPost]
        public object GetOrgHierarchy(int orgHierarchyId)
        {
            OrgHierarchy orgHierarchy = new OrgHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = orgHierarchy.GetOrgHierarchy(this._credits, orgHierarchyId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified role access
        /// </summary>
        /// <param name="moduleAccessId">id of the specified module access</param>
        /// <returns>returns a data table containing the details of the specified role access</returns>
        //// [HttpPost]
        public object GetModuleAccess(int moduleAccessId)
        {
            RoleAccess roleAccess = new RoleAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = roleAccess.GetModuleAccess(this._credits, moduleAccessId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Remove Module Access
        /// </summary>
        /// <param name="moduleAccessId">ID of the Module Access ID</param>
        /// <returns>object based on the execution result</returns>
        public object RemoveModuleAccess(int moduleAccessId)
        {
            RoleAccess roleAccess = new RoleAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = roleAccess.RemoveModuleAccess(this._credits, moduleAccessId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data hierarchy
        /// </summary>
        /// <param name="dataHierarchyId">id of the specified data hierarchy</param>
        /// <returns>returns a data table containing the details of the specified data hierarchy</returns>
        //// [HttpPost]
        public object GetDataHierarchy(int dataHierarchyId)
        {
            DataHierarchy dataHierarchy = new DataHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataHierarchy.GetDataHierarchy(this._credits, dataHierarchyId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the specified data access
        /// </summary>
        /// <param name="dataAccessId">id of the specified data access</param>
        /// <returns>returns a data table containing the details of the specified data access</returns>
        //// [HttpPost]
        public object GetDataAccess(int dataAccessId)
        {
            DataAccess dataAccess = new DataAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataAccess.GetDataAccess(this._credits, dataAccessId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all hierarchy identifier
        /// </summary>
        /// <returns>returns a data table containing the details of all hierarchy identifier found on the database</returns>
        //// [HttpPost]
        public object GetAllHierarchyIdentifier()
        {
            HierarchyIdentifier hierarchyIdentifier = new HierarchyIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = hierarchyIdentifier.GetAllHierarchyIdentifier(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data identifier
        /// </summary>
        /// <returns>returns a data table containing the details of all data identifier found on the database</returns>
        //// [HttpPost]
        public object GetAllDataIdentifier()
        {
            DataIdentifier dataIdentifier = new DataIdentifier();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataIdentifier.GetAllDataIdentifier(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all organization hierarchy
        /// </summary>
        /// <returns>returns a data table containing the details of all organization hierarchy found on the database</returns>
        //// [HttpPost]
        public object GetAllOrgHierarchy()
        {
            OrgHierarchy orgHierarchy = new OrgHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = orgHierarchy.GetAllOrgHierarchy(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all role access
        /// </summary>
        /// <returns>returns a data table containing the details of all role access found on the database</returns>
        //// [HttpPost]
        public object GetAllModuleAccess()
        {
            RoleAccess roleAccess = new RoleAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = roleAccess.GetAllModuleAccess(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data hierarchy
        /// </summary>
        /// <returns>returns a data table containing the details of all data hierarchy found on the database</returns>
        //// [HttpPost]
        public object GetAllDataHierarchy()
        {
            DataHierarchy dataHierarchy = new DataHierarchy();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataHierarchy.GetAllDataHierarchy(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Dynamic Data Access that gets the details of all data access
        /// </summary>
        /// <returns>returns a data table containing the details of all data access found on the database</returns>
        //// [HttpPost]
        public object GetAllDataAccess()
        {
            DataAccess dataAccess = new DataAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = dataAccess.GetAllDataAccess(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Get Organization Access that gets the details of all data access
        /// </summary>
        /// <returns>returns a data table containing the details of all organization access found on the database</returns>
        public object GetOrganizationAccess()
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.GetOrganizationAccess(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }

        /// <summary>
        /// Get Organization Access based on Organization ID
        /// </summary>
        /// <param name="orgId">ID of the Organization</param>
        /// <returns>returns a data table containing the details of specific organization access found on the database</returns>
        public object GetOrganizationAccessByOrg(int orgId)
        {
            UserAccess userAccess = new UserAccess();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = userAccess.GetOrganizationAccessByOrg(this._credits, orgId);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }
    }
}