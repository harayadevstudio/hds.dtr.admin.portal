﻿namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using System.Web.Mvc;
    using Portal.Models;

    [ViewModuleFilter]
    public class ErrorController : Controller
    {
        public ActionResult ServerError()
        {
            return View();
        }

        public ActionResult PageNotFound()
        {
            return View();
        }

        public ActionResult Default()
        {
            return View();
        }

        public ActionResult SessionExpired()
        {
            return View();
        }
    }
}