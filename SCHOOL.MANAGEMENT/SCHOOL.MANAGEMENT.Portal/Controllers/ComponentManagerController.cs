namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using System.Web.Mvc;
    using Core;
    using Core.BusinessLogic;
    using Core.Model;
    using Newtonsoft.Json;
    using Portal.Models;

    /// <summary>
    /// Component Manager Controller
    /// </summary>
    [AuthorizeCookie]
    public class ComponentManagerController : Controller
    {
        /// <summary>
        /// Component Manger Page View
        /// </summary>
        /// <returns>Returns HTML Elements</returns>
        [ViewModuleFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "Component Manager";
            ViewBag.ModuleName = "Component Manager";

            return this.View();
        }
        
    }
}