﻿namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using Core;
    using Core.BusinessLogic;
    using Core.Model;
    using Portal.Models;
    using Newtonsoft.Json;
    using System.Web.Mvc;

    [AuthorizeCookie]
    public class EmployeeLogsController : Controller
    {
        /// <summary>
        /// Information Details Model for Private Response
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        /// <summary>
        /// Declaration of Encryption
        /// </summary>
        private DatabaseConnection _encrypt = new DatabaseConnection();

        /// <summary>
        /// Declaration of Connection Model Credential
        /// </summary>
        private ConnectionModel _credits = new ConnectionModel();

        [ViewModuleFilter]
        public ActionResult Index()
        {
            return View();
        }
        public object GetEmployeeCount()
        {
            EmployeeLogs employeeLogs = new EmployeeLogs();
            this._credits.Key = this.Request.Cookies[GeneralModel.CookieName].Value;

            this._response = employeeLogs.GetEmployeeCount(this._credits);
            return JsonConvert.SerializeObject(this._response, Formatting.None);
        }
    }
}