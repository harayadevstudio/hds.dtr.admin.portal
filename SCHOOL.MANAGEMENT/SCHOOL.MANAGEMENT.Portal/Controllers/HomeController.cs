﻿namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using Enyim.Caching;
    using Enyim.Caching.Memcached;
    using Portal.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;

    [ViewModuleFilter]
    public class HomeController : Controller
    {
        [AuthorizeCookie]
        public ActionResult Index()
        {

            return View();
        }

        [AuthorizeCookie, HttpPost]
        public ActionResult AjaxRequest()
        {
            return Json(new { status = 1 });
        }
    }
}