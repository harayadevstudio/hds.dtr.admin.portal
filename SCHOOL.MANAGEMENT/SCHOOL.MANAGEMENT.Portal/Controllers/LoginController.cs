﻿namespace SCHOOL.MANAGEMENT.Portal.Controllers
{
    using Enyim.Caching;
    using Enyim.Caching.Memcached;
    using Core;
    using Core.BusinessLogic;
    using Core.Model;
    using Portal.Models;
    using Newtonsoft.Json;
    using System;
    using System.Configuration;
    using System.Reflection;
    using System.Web.Mvc;

    public class LoginController : Controller
    {
        /// <summary>
        /// Information Details Model for Private Response
        /// </summary>
        private ResponseModel _response = new ResponseModel();

        // GET: Login
        public ActionResult Index()
        {
            if (this.Request.Cookies[GeneralModel.CookieName] != null)
            {
                CookieValidator cookieValidator = new CookieValidator();
                string reqCookie = this.Request.Cookies[GeneralModel.CookieName].Value;
                if (cookieValidator.ValidateCookie(reqCookie) == CookieStatus.IsValid)
                    return RedirectToRoute(new { controller = "Home", action = "Index" });
                else
                    return View();
            }

            return View();
        }

        public ActionResult Logout()
        {
            if (this.Request.Cookies[GeneralModel.CookieName] != null)
            {
                string reqCookie = this.Request.Cookies[GeneralModel.CookieName].Value;
                using (MemcachedClient memCached = new MemcachedClient())
                {
                    DatabaseConnection encryption = new DatabaseConnection();
                    ConnectionModel userCreds = new ConnectionModel();
                    userCreds = JsonConvert.DeserializeObject<ConnectionModel>(encryption.Decrypt(reqCookie));

                    CookieValidator cookieValidator = new CookieValidator();

                    if (cookieValidator.ValidateCookie(reqCookie) == CookieStatus.IsValid)
                        memCached.Remove(GeneralModel.CookieName + "_" + userCreds.UserId);
                }
            }

            return RedirectToRoute(new { controller = "Login", action = "Index" });
        }

        [HttpPost]
        public object CheckUserCredentials(string username, string password)
        {
            using (MemcachedClient client = new MemcachedClient())
            {
                User user = new User();

                string currentDate = DateTime.Now.ToString();
                this._response = user.GetUserByCredentials(username, password, currentDate);

                dynamic userCreds = GeneralModel.CreateExpandoFromObject(this._response);

                if (this._response.Status == 1)
                {
                    PropertyInfo responseProperty = this._response.Data.GetType().GetProperty("userId");
                    int userId = Convert.ToInt32(responseProperty.GetValue(this._response.Data, null));
                    userCreds.CookieName = GeneralModel.CookieName;

                    client.Store(StoreMode.Set, GeneralModel.CookieName + "_" + userId, currentDate);
                }

                return JsonConvert.SerializeObject(userCreds, Formatting.None);
            }
        }
    }
}