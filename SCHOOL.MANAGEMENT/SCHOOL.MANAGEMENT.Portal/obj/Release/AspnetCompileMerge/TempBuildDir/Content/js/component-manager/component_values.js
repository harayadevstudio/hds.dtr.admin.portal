﻿$(document).ready(function () {
    var compFnc = new ComponentData();
    $.when(compFnc.InitCompDataObjs()).done(function () {
        var listGroup = $('.list-group')[0].children[0];
        $(listGroup).click();
    });
});

function ComponentData() {
    var componentData = this;    
    var componentRow = null;
    var tblSets = {};

    this.InitCompDataObjs = function () {
        /// <summary>
        /// Initializatio of Objects   
        ///         

        $("[type='checkbox']").bootstrapSwitch();

        $('.selectpicker').selectpicker();   

        var listGroup = $('.list-group')[0].children;

        $.each(listGroup, function (index) {
            var componentObj = listGroup[index];
            var componentId = $(componentObj).data('component-id');
            var componentName = $(componentObj).data('component-name');

            componentData.GetComponentData(componentName, componentId);

            $('#btnSubmit' + componentName).off('click').on('click', function () {
                /// Get Component Name
                var compName = $(this).data('compname');
                var process = $(this).attr('btn-process');
                var dataKeyId = $(this).data('datakey-id');

                var componentModel = [];
                var modalInputs = $('#modal' + compName.replace(' ', '') + ' .modal-dialog .modal-body').children();

                $.each(modalInputs, function (index) {

                    var htmlFor = modalInputs[index].children[0].htmlFor;
                    var selector = modalInputs[index].children[0].dataset.valueSelector;

                    var dataValue = eval('$("#' + htmlFor + '").' + selector);
                    var componentId = eval('$("#' + htmlFor + '").data("component-id")');
                    var metadataId = eval('$("#' + htmlFor + '").data("metadata-id")');
                    var dataId = eval('$("#' + htmlFor + '").data("data-id")') === undefined ? 0 : eval('$("#' + htmlFor + '").data("data-id")');

                    var details = {
                        DataId: dataId,
                        DataKeyId: dataKeyId,
                        DataValue: dataValue,
                        ComponentId: componentId,
                        MetadataId: metadataId
                    };

                    componentModel.push(details);

                    console.log(componentModel);
                });

                if (hasNull(componentModel) !== true) {
                    if (process === 'add') {
                        MaterialConfirm({
                            message: 'Add new ' + compName + '?'
                        }).done(function () {
                            componentData.AddComponentData(componentModel, compName.replace(' ', ''));
                        });
                    } else {
                        MaterialConfirm({
                            message: 'Update ' + compName + ' details?'
                        }).done(function () {                            
                            componentData.ModifyComponentData(componentModel, compName.replace(' ', ''));
                        });
                    }
                } else {
                    MaterialNotify("Please fill in all fields", 'warning');
                }
            });

            $('#tbl' + componentName).off('click').on('click', '.nav-container .edit-data', function (e) {
                var componentId = $(this).data('component-id');
                var dataKey = $(this).data('datakey-id');
                
                componentData.GetSpecificData(componentName, componentId, dataKey);

                // set rowMetadata value
                var rowDtl = $(this);                
                componentData.componentRow = tblSets[componentName].row(rowDtl[0].offsetParent.parentElement);

                e.preventDefault();
            });

            $('#modal' + componentName).on('hidden.bs.modal', function () {
                componentData.ClearFields(componentName);
            });
        });     
        
    };

    this.AddComponentData = function (componentModel, componentName) {
        /// <summary>
        /// Function for adding new component data
        /// </summary>
        $.post("../ComponentValues/AddComponentData", { componentModel: componentModel }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modal' + componentName).modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Add data to datatable
                tblSets[componentName].row.add(responseObj.Data[0]).draw();
                componentData.ClearFields(componentName);
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyComponentData = function (componentModel, componentName) {
        /// <summary>
        /// Function for modifying component data
        /// </summary>
        $.post("../ComponentValues/ModifyComponentData", { componentModel: componentModel }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modal' + componentName).modal('hide');
                componentData.ClearFields(componentName);
                MaterialNotify(responseObj.Message, 'success');
                //// Update row data from datatable
                tblSets[componentName].row(componentData.componentRow).data(responseObj.Data[0]).draw();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetComponentData = function (componentName, componentId) {
        /// <summary>
        /// Function for fetching data rows
        /// </summary>
        $.post("../ComponentValues/GetComponentData", { componentId: componentId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                                                
                var th = $('#tbl' + componentName + ' thead tr th');
                var tblColumns = [];

                $.each(th, function (index) {                       
                    if (!index) {
                        tblColumns.push({
                            data: th[index].innerText,
                            render: function (data, type, full, meta) {
                                return '<span>' + data + '</span><br/><div class="nav-container"><a class="edit-data active" data-datakey-id="' + full.DATA_PRIMARY_KEY + '" data-component-id="' + full.COMPONENT_ID + '" href="#" data-toggle="modal" data-target="#modal' + componentName + '"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</a></div>';
                            }
                        });
                    } else {    
                        if (th[index].dataset.metaType === 'boolean') {
                            tblColumns.push({
                                data: th[index].innerText,
                                render: function (data, type, full, meta) {
                                    if (+data === 1) {
                                        return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                    } else {
                                        return '<div class="user-status badge badge-pill badge-danger">InActive</div>';
                                    }
                                }
                            });                            
                        } else {
                            tblColumns.push({ "data": th[index].innerText });
                        }
                    }                    
                });   

                var dataTbl = $('#tbl' + componentName).DataTable({
                    info: false,
                    sort: false,
                    paging: true,
                    lengthChange: false,
                    pagingType: "simple_numbers",
                    data: responseObj.Data,
                    columns: tblColumns          
                });

                tblSets[componentName] = dataTbl;
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };   

    this.GetSpecificData = function (componentName, componentId, dataKeyId) {
        /// <summary>
        /// Function for fetching data rows
        /// </summary>
        $.post("../ComponentValues/GetSpecificData", { dataKeyId: dataKeyId, componentId: componentId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                var compName = $('#btnSubmit' + componentName).data('compname');
                var modalInputs = $('#modal' + compName.replace(' ', '') + ' .modal-dialog .modal-body').children();

                $.each(modalInputs, function (index) {

                    var htmlFor = modalInputs[index].children[0].htmlFor;
                    var selector = modalInputs[index].children[0].dataset.valueSelector;

                    if (eval('$("#' + htmlFor + '").is(":checkbox")')) {
                        if (+responseObj.Data[index].DATA_VALUE === 1 && eval('$("#' + htmlFor + '").prop("checked")') === false) {
                            eval('$("#' + htmlFor + '").click()');
                        } else if (+responseObj.Data[index].DATA_VALUE !== 1 && eval('$("#' + htmlFor + '").prop("checked")') === true) {
                            eval('$("#' + htmlFor + '").click()');
                        }
                    } else {
                        eval('$("#' + htmlFor + '").prop("value", "' + responseObj.Data[index].DATA_VALUE + '")');
                    }

                    eval('$("#' + htmlFor + '").data("data-id", ' + responseObj.Data[index].DATA_ID + ')');                    
                });          

                $('#btnSubmit' + componentName).html('<i aria-hidden="true" class="fa fa-pencil"></i> Update data');
                $('#btnSubmit' + componentName).attr('btn-process', 'update');
                $('#btnSubmit' + componentName).data('datakey-id', responseObj.Data[0].DATA_PRIMARY_KEY);                
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };  

    this.ClearFields = function (componentName) {
        /// <summary>
        /// Clear Modal Fields
        /// </summary>
        /// <param name="componentName">Component Modal to be Cleared</param>

        //// loop through all modal-header of componentName to clear UI
        $('#btnSubmit' + componentName);
        $('#btnSubmit' + componentName.replace(' ', '')).attr('btn-process', 'add');

        var compName = $('#btnSubmit' + componentName).data('compname');
        var modalInputs = $('#modal' + compName.replace(' ', '') + ' .modal-dialog .modal-body').children();

        $.each(modalInputs, function (index) {

            var htmlFor = modalInputs[index].children[0].htmlFor;
                      
            if (eval('$("#' + htmlFor + '").is(":checkbox")')) {
                if (!eval('$("#' + htmlFor + '").prop("checked")')) {
                    eval('$("#' + htmlFor + '").click()');
                }
            } else {
                eval('$("#' + htmlFor + '").prop("value", "")');
            }

            eval('$("#' + htmlFor + '").data("data-id", 0)');
        });
    };
}