﻿$(document).ready(function () {
    /// <summary>
    /// Initial Page Load
    /// </summary>

    var login = new Login();

    login.ClearFields();
    login.InitializeComponents();

});

function Login() {
    /// <summary>
    /// Login Class Function
    /// </summary>

    this.InitializeComponents = function () {
        /// <summary>
        /// Initialize All JavaScript Events in Login Page
        /// </summary>

        $('#txtUsername').focus();

        $('#txtUsername').onEnter(function () {
            $('#txtPassword').focus();
        });

        $('#txtPassword').onEnter(function () {
            $('#btnLogin').click();
        });

        $('#txtPassword').keypress(function (e) {
            var s = String.fromCharCode(e.which);
            if ((s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) || (s.toUpperCase() !== s && s.toLowerCase() === s && e.shiftKey)) {
                $('#txtPassword').closest('.form-group').addClass('caps-on');
            } else if ((s.toLowerCase() === s && s.toUpperCase() !== s && !e.shiftKey) || (s.toLowerCase() !== s && s.toUpperCase() === s && e.shiftKey)) {
                $('#txtPassword').closest('.form-group').removeClass('caps-on');
            }
        });

        $('#txtPassword').blur(function () {
            $('#txtPassword').closest('.form-group').removeClass('caps-on');
        });

        $('#btnLogin').unbind().click(function () {
            var login = new Login();
            var username = $('#txtUsername').val();
            var password = $('#txtPassword').val();

            login.GetUserCredentials(username, password);
        });
    }

    this.GetUserCredentials = function (username, password) {
        /// <summary>
        /// Checking of User Credential Access
        /// </summary>
        /// <param name="username" type="string">Username of the User Account</param>
        /// <param name="password" type="string">Password of the User Account</param>
        /// <returns type="Json Object">Returns User Information and Encrypted Key for Access</returns>

        if (username == '' && password == '') {
            MaterialNotify('Please fill in fields', 'warning');
        } else if (username == '') {
            MaterialNotify('Please input username', 'warning');
        } else if (password == '') {
            MaterialNotify('Please input password', 'warning');
        } else {
            $.post("Login/CheckUserCredentials",
                { username: username, password: password },
                function (result) {
                    var response = JSON.parse(result);

                    if (response.Status == 1) {
                        ToggleLoading();

                        var userData = response.Data;
                        SetCookie(response.CookieName, userData.key, 0.583333); // 14 Hours

                        setTimeout(function () {
                            window.location = '.' + userData.modulePage;
                        }, 1500);
                    } else {
                        MaterialNotify(response.Message, 'warning');
                    }
                }).fail(function (ex) {
                    MaterialNotify(ex.statusText, 'danger', true);
                });
        }
    }

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Login Page
        /// </summary>

        $('#txtUsername').val('');
        $('#txtPassword').val('');
    }
}