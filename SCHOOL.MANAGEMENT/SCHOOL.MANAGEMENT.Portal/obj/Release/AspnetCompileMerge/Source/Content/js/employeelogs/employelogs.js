﻿$(document).ready(function () {
    var employeeLogs = new EmployeeLogs();
    employeeLogs.IntializeEvents();
});

function EmployeeLogs() {

    this.IntializeEvents = function () {

        var loadFnc = this;

        var minDate = '';
        new Pikaday({
            field: $('#dateFrom')[0],
            format: 'MMMM DD, YYYY',
            onSelect: function () {
                mindate = this.getMoment().toDate();
            }
        });

        new Pikaday({
            field: $('#dateTo')[0],
            format: 'MMMM DD, YYYY',
            minDate: minDate
        });

        loadFnc.GetEmployeeCount();
    };

    this.GetEmployeeCount = function () {
        $.post("../EmployeeLogs/GetEmployeeCount", function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#employeeCount').append(responseObj.Data[0].COUNT);
            } else {
                Swal.fire({
                    title:'We\'ve Encountered a Problem!',
                    text: 'GetEmployeeCount Function returned Error',
                    icon: 'error',
                    confirmButtonText: 'Close'
                })
            }
        });
    };


};