﻿var _userManagerGlobal = new UserManagerGlobals();

$(document).ready(function () {
    /// <summary>
    /// Initialized Page Load
    /// </summary>

    var userManager = new UserManager().InitUserManagerComponents();

    var users = new Users();
    users.GetAllUsers();
    users.InitUserComponents();

    var module = new Module();
    module.GetAllModules();
    module.InitModuleComponents();

    var action = new Action();
    action.GetAllActions();
    action.InitActionComponents();

    var moduleAction = new ModuleAction();
    moduleAction.GetAllModuleActions();
    moduleAction.InitModuleActionComponents();

    var role = new Role();
    role.GetAllUserRoles();
    role.InitRoleComponents();

    var hierarchyIdentifier = new HierarchyIdentifier();
    hierarchyIdentifier.GetAllHierarchyIdentifiers();
    hierarchyIdentifier.InitHrchyIdentifierComponents();

    var dataIdentifier = new DataIdentifier();
    dataIdentifier.GetAllDataIdentifiers();
    dataIdentifier.InitDataIdentifierComponents();

    var orgHierarchy = new OrganizationHierarchy();
    orgHierarchy.GetAllOrgHierarchy();
    orgHierarchy.InitOrgHrchyComponents();

    var dataHierarchy = new DataHierarchy();
    dataHierarchy.GetAllDataHierarchy();
    dataHierarchy.InitDataHrchyComponents();

    var dataAccess = new DataAccess();
    dataAccess.GetAllDataAccess();
    dataAccess.InitComponents();

    var moduleAccess = new ModuleAccess();
    moduleAccess.GetAllRoleAccess();
    moduleAccess.InitComponents();

    var userAccess = new UserAccess();
    userAccess.GetAllUserAccess();
    userAccess.GetAllUsers();
    userAccess.InitComponents();
});

function UserManagerGlobals() {
    /// <summary>
    /// Global Declaration of Variables
    /// </summary>

    this.userId = '';
    this.moduleId = '';
    this.actionId = '';
    this.moduleActionId = '';
    this.userRoleId = '';
    this.hierarchyIdentifierId = '';
    this.dataIdentifierId = '';
    this.userAccessId = '';
    this.orgHierarchyId = '';
    this.moduleAccessId = '';
    this.dataHierarchyId = '';
    this.dataAccessId = '';
}

function UserManager() {
    /// <summary>
    /// User Manager Class Function
    /// </summary>

    this.InitUserManagerComponents = function () {
        /// <summary>
        /// Initialize Components: Bootstrap Switch, Select Picker and Next Page Toggle
        /// </summary>

        $("[type='checkbox']").bootstrapSwitch();
        $('.selectpicker').selectpicker();

        $('.next-page-toggle').unbind().click(function () {
            $(this).closest('.header').siblings('.inner-forms').find('.wrapper').toggleClass('next-page');
            $(this).toggleClass('clicked');
            var buttonId = $(this).data('button-id');

            if ($(this).hasClass('clicked')) {
                $(buttonId).removeClass('btn-primary').addClass('btn-success');
                $(buttonId).html('<i class="fa fa-list" aria-hidden="true"></i> ' + $(this).data('off'));
            } else {
                $(buttonId).removeClass('btn-success').addClass('btn-primary');
                $(buttonId).html('<i class="fa fa-plus" aria-hidden="true"></i> ' + $(this).data('on'));
            }
        });
    };
}

function Users() {
    /// <summary>
    /// User Class Function
    /// </summary>

    var userFunc = this;
    var userTbl = null;
    var userRow = null;

    this.InitUserComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        $('#tblUsers').on('click', '.nav-container .edit-user', function (e) {
            
            _userManagerGlobal.userId = $(this).data('user-id');
            userFunc.GetUser($(this).data('user-id'));

            var tblObj = $(this);
            userRow = userTbl.row(tblObj[0].offsetParent.parentElement);
            
            e.preventDefault();
        });

        $('#tblUsers').on('click', '.nav-container .grant-user-access', function (e) {
            var userId = $(this).data('user-id');

            MaterialConfirm({
                message: 'Grant user access?'
            }).done(function () {
                var userAccess = new UserAccess().AddUserAccess(userId, userObj.userId);
            });

            e.preventDefault();
        });

        $('#btnClearUserForm').unbind().click(function () {
            $('#btnSubmitUser').html('<i aria-hidden="true" class="fa fa-plus"></i> Add User');
            $('#btnSubmitUser').attr('btn-process', 'add');
            $('#txtPassword').prop('disabled', false);
        });

        $('.for-username').off('keyup').on('keyup', function () {
            var firstName = $('#txtFirstName').val();
            var middleName = $('#txtMiddleName').val();
            var lastName = $('#txtLastName').val();
            var employeeId = $('#txtEmployeeId').val();

            $('#txtUserName').val(firstName.charAt(0) + middleName.charAt(0) + lastName + employeeId);
        });

        $('#btnSubmitUser').unbind().click(function () {
            var process = $(this).attr('btn-process');
            var isActive = null;

            if ($('#chkUserStatus').prop('checked') === true) {
                isActive = '1';
            } else {
                isActive = '0';
            }

            var userDetails = {
                UserName: $('#txtUserName').val().toUpperCase(),
                Password: $('#txtPassword').val(),
                FirstName: $('#txtFirstName').val(),
                MiddleName: $('#txtMiddleName').val(),
                LastName: $('#txtLastName').val(),
                IsActive: isActive,
                EmployeeId: $('#txtEmployeeId').val()
            };

            if (hasNull(userDetails) !== true) {

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new user?'
                    }).done(function () {
                        userFunc.AddUser(userDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update user details?'
                    }).done(function () {
                        userDetails.Id = _userManagerGlobal.userId;
                        userFunc.ModifyUser(userDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllUsers = function () {
        /// <summary>
        /// Gets All User Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of User Information</returns>

        $.post("../UserManager/GetAllUsers", function (response) {
            var usersObj = JSON.parse(response);

            if (usersObj.Status === 1) {
                userTbl = $('#tblUsers').DataTable({
                    info: false,
                    data: usersObj.Data,
                    columns: [
                        {
                            data: 'EMPLOYEE_ID',
                            render: function (data, type, full, meta) {
                                return '<span class="username">' + data + '</span><br/><div class="nav-container"><a class="edit-user active" data-user-id="' + full.USER_ID + '" href="#"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a class="grant-user-access active" data-user-id="' + full.USER_ID + '" href="#"><i aria-hidden="true" class="fa fa-check-square"></i> Grant User Access</a></div>';
                            }
                        },
                        {
                            data: 'USERNAME'
                        },
                        {
                            data: {
                                first_name: 'FIRST_NAME',
                                middle_name: 'MIDDLE_NAME',
                                last_name: 'LAST_NAME',
                            },
                            render: function (data, type, full, meta) {
                                return data.LAST_NAME + ', ' + data.FIRST_NAME + ' ' + data.MIDDLE_NAME;
                            }
                        },
                        {
                            data: 'IS_ACTIVE',
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Inactive</div>';
                                }
                            }
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });

            } else {
                MaterialNotify(usersObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetUser = function (userId) {
        /// <summary>
        /// Gets Specific User Information based on User ID
        /// </summary>
        /// <param name="userId" type="int">ID of the Specific User</param>
        /// <returns type="Json Object">Returns the Information of Specific User</returns>

        $.post("../UserManager/GetUser", { userId: userId }, function (response) {
            var usersObj = JSON.parse(response);

            if (usersObj.Status === 1) {
                userFunc.PopulateUserForm(usersObj.Data[0]);
            } else {
                MaterialNotify(usersObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateUserForm = function (userData) {
        /// <summary>
        /// Populate User Information
        /// </summary>
        /// <param name="userData" type="object">User Information</param>
        /// <returns type="Json Object">Returns the Information of Specific User</returns>

        $('#txtUserName').val(userData.USERNAME);
        $('#txtPassword').prop('disabled', true);
        $('#txtPassword').val(userData.PASSWORD);
        $('#txtFirstName').val(userData.FIRST_NAME);
        $('#txtLastName').val(userData.LAST_NAME);
        $('#txtMiddleName').val(userData.MIDDLE_NAME);
        $('#txtEmployeeId').val(userData.EMPLOYEE_ID);

        if (+userData.IS_ACTIVE === 1 && $('#chkUserStatus').prop('checked') === false) {
            $('#chkUserStatus').click();
        } else if (+userData.IS_ACTIVE !== 1 && $('#chkUserStatus').prop('checked') === true) {
            $('#chkUserStatus').click();
        }

        $('#btnSubmitUser').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update User');
        $('#btnSubmitUser').attr('btn-process', 'update');
        $('#btnUserPageToggle').click();
    };

    this.AddUser = function (userDetails) {
        /// <summary>
        /// Adds New User Information
        /// </summary>
        /// <param name="userDetails" type="object">User Details[UserName, Password,
        /// FirstName, MiddleName, LastName, IsActive, CreatedBy, EmployeeId]</param>
        /// <returns type="Json Object">Returns Inserted User Information in the Database</returns>
        
        $.post("../UserManager/AddUser", { userModel: userDetails }, function (response) {
            var usersObj = JSON.parse(response);

            if (usersObj.Status === 1) {
                MaterialNotify(usersObj.Message, 'success');
                userTbl.row.add(usersObj.Data[0]).draw();
                $('#btnClearUserForm').click();
            } else {
                MaterialNotify(usersObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyUser = function (userDetails) {
        /// <summary>
        /// Modifies the Information of User
        /// </summary>
        /// <param name="userDetails" type="object">User Details[Id, UserName, Password,
        /// FirstName, MiddleName, LastName, IsActive, UpdatedBy, EmployeeId]</param>
        /// <returns type="Json Object">Returns Modified Information of User in the Database</returns>

        $.post("../UserManager/ModifyUser", { userModel: userDetails }, function (response) {
            var usersObj = JSON.parse(response);

            if (usersObj.Status === 1) {
                MaterialNotify(usersObj.Message, 'success');
                userTbl.row(userRow).data(usersObj.Data[0]).draw();
                $('#btnClearUserForm').click();
            } else {
                MaterialNotify(usersObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };
}

function Module() {
    /// <summary>
    /// Module Class Function
    /// </summary>

    var moduleFnc = this;
    var moduleTbl = null;
    var moduleRow = null;

    this.InitModuleComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        moduleFnc.GetParentModules();

        $('#tblModules').on('click', '.nav-container .edit-module', function (e) {
            _userManagerGlobal.moduleId = $(this).data('module-id');
            moduleFnc.GetModule($(this).data('module-id'));

            var tblObj = $(this);
            moduleRow = moduleTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnClearModuleForm').unbind().click(function () {
            $('#cbxModuleParent').selectpicker('val', null);
            $('#btnSubmitModule').html('<i aria-hidden="true" class="fa fa-plus"></i> Add Module');
            $('#btnSubmitModule').attr('btn-process', 'add');
        });

        $('#btnSubmitModule').unbind().click(function () {
            var process = $(this).attr('btn-process');
            var isActive = null;
            var isShown = null;

            if ($('#chkModuleStatus').prop('checked') === true) {
                isActive = '1';
            } else {
                isActive = '0';
            }

            if ($('#chkModuleIsShown').prop('checked') === true) {
                isShown = '1';
            } else {
                isShown = '0';
            }

            var moduleDetails = {
                Name: $('#txtModuleName').val(),
                Description: $('#txtModuleDescription').val(),
                ModulePage: $('#txtModulePage').val(),
                Icon: $('#txtModuleIcon').val(),
                SortNo: $('#txtModuleSortNo').val(),
                IsShown: isShown,
                IsActive: isActive
            };

            if (hasNull(moduleDetails) !== true) {

                moduleDetails.ParentId = $("#cbxModuleParent").val() || null;

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new module?'
                    }).done(function () {
                        moduleFnc.AddModule(moduleDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update module details?'
                    }).done(function () {
                        moduleDetails.Id = _userManagerGlobal.moduleId;
                        moduleFnc.ModifyModule(moduleDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllModules = function () {
        /// <summary>
        /// Gets All Module Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Module Information</returns>

        $.post("../UserManager/GetAllModules", function (response) {
            var modulesObj = JSON.parse(response);

            if (modulesObj.Status === 1) {
                moduleTbl = $('#tblModules').DataTable({
                    info: false,
                    data: modulesObj.Data,
                    columns: [
                        {
                            data: 'MODULE_NAME',
                            render: function (data, type, full, meta) {
                                return '<span class="module-name">' + data + '</span><br/><div class="nav-container"><a class="edit-module active" data-module-id="' + full.MODULE_ID + '" href="#"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        },
                        {
                            data: 'MODULE_PAGE'
                        },
                        {
                            data: 'PARENT_ID'
                        },
                        {
                            data: 'SORT_NO'
                        },
                        {
                            data: 'IS_ACTIVE',
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Inactive</div>';
                                }
                            }
                        },
                        {
                            data: 'IS_SHOWN',
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Shown</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Hidden</div>';
                                }
                            }
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(modulesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetModule = function (moduleId) {
        /// <summary>
        /// Gets Specific Module Information based on Module ID
        /// </summary>
        /// <param name="moduleId" type="int">ID of the Specific Module</param>
        /// <returns type="Json Object">Returns the Information of Specific Module</returns>

        $.post("../UserManager/GetModule", { moduleId: moduleId }, function (response) {
            var modulesObj = JSON.parse(response);

            if (modulesObj.Status === 1) {
                moduleFnc.PopulateModuleForm(modulesObj.Data[0]);
            } else {
                MaterialNotify(modulesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetParentModules = function () {
        /// <summary>
        /// Gets All Parent Module Information
        /// </summary>
        /// <returns type="Json Object">Returns the Information of Parent Module</returns>

        $.post("../UserManager/GetParentModules", function (response) {
            var modulesObj = JSON.parse(response);

            if (modulesObj.Status === 1) {
                moduleFnc.PopulateParentModules(modulesObj.Data);
            } else {
                MaterialNotify(modulesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateModuleForm = function (moduleData) {
        /// <summary>
        /// Populate Module Information
        /// </summary>
        /// <param name="moduleData" type="object">Module Information</param>
        /// <returns type="Json Object">Returns the Information of Specific Module</returns>

        $('#txtModuleName').val(moduleData.MODULE_NAME);
        $('#txtModulePage').val(moduleData.MODULE_PAGE);
        $('#txtModuleDescription').val(moduleData.DESCRIPTION);
        $('#txtModuleIcon').val(moduleData.ICON);
        $('#txtModuleSortNo').val(moduleData.SORT_NO);

        $('#cbxModuleParent').selectpicker('val', moduleData.PARENT_ID);

        if (+moduleData.IS_ACTIVE === 1 && $('#chkModuleStatus').prop('checked') === false) {
            $('#chkModuleStatus').click();
        } else if (+moduleData.IS_ACTIVE !== 1 && $('#chkModuleStatus').prop('checked') === true) {
            $('#chkModuleStatus').click();
        }

        if (+moduleData.IS_SHOWN === 1 && $('#chkModuleIsShown').prop('checked') === false) {
            $('#chkModuleIsShown').click();
        } else if (+moduleData.IS_SHOWN !== 1 && $('#chkModuleIsShown').prop('checked') === true) {
            $('#chkModuleIsShown').click();
        }

        $('#btnSubmitModule').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update Module');
        $('#btnSubmitModule').attr('btn-process', 'update');
        $('#btnModulePageToggle').click();
    };

    this.PopulateParentModules = function (moduleData) {
        /// <summary>
        /// Populate Parent Module Information
        /// </summary>
        /// <param name="moduleData" type="object">Parent Module Information</param>
        /// <returns type="Json Object">Returns the Information of Specific Parent Module</returns>

        var htmlString = '';

        $.each(moduleData, function (index) {
            htmlString += '<option value="' + moduleData[index].MODULE_ID + '">' + moduleData[index].MODULE_NAME + '</option>';
        });

        $('#cbxModuleParent').append(htmlString);
        $('#cbxModuleParent').selectpicker('refresh');
    };

    this.AddModule = function (moduleDetails) {
        /// <summary>
        /// Adds New Module Information
        /// </summary>
        /// <param name="moduleDetails" type="object">Module Details[Name, Description,
        /// CreatedBy, ParentId, ModulePage, IsActive, IsShown, SortNo, Icon]</param>
        /// <returns type="Json Object">Returns Inserted Module Information in the Database</returns>

        $.post("../UserManager/AddModule", { moduleModel: moduleDetails }, function (response) {
            var moduleObj = JSON.parse(response);

            if (moduleObj.Status === 1) {
                MaterialNotify(moduleObj.Message, 'success');
                $('#btnClearModuleForm').click();
                moduleTbl.row.add(moduleObj.Data[0]).draw();
            } else {
                MaterialNotify(moduleObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyModule = function (moduleDetails) {
        /// <summary>
        /// Modifies the Information of Module
        /// </summary>
        /// <param name="moduleDetails" type="object">Module Details[Id, Name, Description,
        /// UpdatedBy, ParentId, ModulePage, IsActive, IsShown, SortNo, Icon]</param>
        /// <returns type="Json Object">Returns Modified Information of Module in the Database</returns>

        $.post("../UserManager/ModifyModule", { moduleModel: moduleDetails }, function (response) {
            var moduleObj = JSON.parse(response);

            if (moduleObj.Status === 1) {
                MaterialNotify(moduleObj.Message, 'success');
                moduleTbl.row(moduleRow).data(moduleObj.Data[0]).draw();
                $('#btnClearModuleForm').click();
            } else {
                MaterialNotify(moduleObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };
}

function Action() {
    /// <summary>
    /// Action Class Function
    /// </summary>

    var actionFnc = this;
    var actionTbl = null;
    var actionRow = null;

    this.InitActionComponents = function () {
        /// <summary>
        /// Initialize Page Events in Action Page
        /// </summary>

        $('#tblActions').on('click', '.nav-container .edit-action', function (e) {
            _userManagerGlobal.actionId = $(this).data('action-id');
            actionFnc.GetAction($(this).data('action-id'));

            var tblObj = $(this);
            actionRow = actionTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnActionPageToggle').unbind().click(function () {
            if ($('#btnSubmitAction').attr('btn-process') === 'update') {
                actionFnc.ClearFields();
            }
        });

        $('#btnSubmitAction').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var actionDetails = {
                Name: $('#txtActionName').val(),
                Description: $('#txtActionDescription').val()
            };

            if (hasNull(actionDetails) !== true) {

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new action?'
                    }).done(function () {
                        actionFnc.AddAction(actionDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update action details?'
                    }).done(function () {
                        actionDetails.Id = _userManagerGlobal.actionId;
                        actionFnc.ModifyAction(actionDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllActions = function () {
        /// <summary>
        /// Gets All Action Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Action Information</returns>

        $.post("../UserManager/GetAllActions", function (response) {
            var actionsObj = JSON.parse(response);

            if (actionsObj.Status === 1) {
                actionTbl = $('#tblActions').DataTable({
                    info: false,
                    data: actionsObj.Data,
                    columns: [
                        {
                            data: 'ACTION_NAME',
                            render: function (data, type, full, meta) {
                                return '<span class="action-name">' + data + '</span><br/><div class="nav-container"><a class="edit-action active" data-action-id="' + full.ACTION_ID + '" href="#" data-toggle="modal" data-target="#modalAddAction"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(actionsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAction = function (actionId) {
        /// <summary>
        /// Gets Specific Action Information based on Action ID
        /// </summary>
        /// <param name="actionId" type="int">ID of the Specific Action</param>
        /// <returns type="Json Object">Returns the Information of Specific Action</returns>

        $.post("../UserManager/GetAction", { actionId: actionId }, function (response) {
            var actionObj = JSON.parse(response);

            if (actionObj.Status === 1) {
                $('#txtActionName').val(actionObj.Data[0].ACTION_NAME);
                $('#txtActionDescription').val(actionObj.Data[0].DESCRIPTION);

                $('#modalAddAction').addClass('update');
                $('#btnSubmitAction').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitAction').attr('btn-process', 'update');
            } else {
                MaterialNotify(actionObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddAction = function (actionDetails) {
        /// <summary>
        /// Adds New Action Information
        /// </summary>
        /// <param name="actionDetails" type="object">Action Details[Name, Description, CreatedBy]</param>
        /// <returns type="Json Object">Returns Inserted Action Information in the Database</returns>

        $.post("../UserManager/AddAction", { actionModel: actionDetails }, function (response) {
            var actionObj = JSON.parse(response);

            if (actionObj.Status === 1) {
                $('#modalAddAction').modal('hide');
                actionFnc.ClearFields();
                actionTbl.row.add(actionObj.Data[0]).draw();
                MaterialNotify(actionObj.Message, 'success');
            } else {
                MaterialNotify(actionObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyAction = function (actionDetails) {
        /// <summary>
        /// Modifies the Information of Action
        /// </summary>
        /// <param name="actionDetails" type="object">Action Details[Id, Name, Description, UpdatedBy]</param>
        /// <returns type="Json Object">Returns Modified Information of Action in the Database</returns>

        $.post("../UserManager/ModifyAction", { actionModel: actionDetails }, function (response) {
            var actionObj = JSON.parse(response);

            if (actionObj.Status === 1) {
                $('#modalAddAction').modal('hide');
                actionFnc.ClearFields();
                actionTbl.row(actionRow).data(actionObj.Data[0]).draw();
                MaterialNotify(actionObj.Message, 'success');
            } else {
                MaterialNotify(actionObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Action Page
        /// </summary>

        $('#txtActionName').val('');
        $('#txtActionDescription').val('');
        $('#modalAddAction').removeClass('update');
        $('#btnSubmitAction').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitAction').attr('btn-process', 'add');
    };
}

function ModuleAction() {
    /// <summary>
    /// Module Action Class Function
    /// </summary>

    var moduleActFnc = this;
    var moduleActTbl = null;
    var moduleActRow = null;

    this.InitModuleActionComponents = function () {
        /// <summary>
        /// Initialize Page Events in Module Action Page
        /// </summary>

        moduleActFnc.GetAllModules();
        moduleActFnc.GetAllActions();

        $('#tblModuleActions').on('click', '.nav-container .edit-module-action', function (e) {
            _userManagerGlobal.moduleActionId = $(this).data('module-action-id');
            moduleActFnc.GetModuleAction($(this).data('module-action-id'));

            var tblObj = $(this);
            moduleActRow = moduleActTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnClearModuleForm').unbind().click(function () {
            $('#btnSubmitModule').html('<i aria-hidden="true" class="fa fa-plus"></i> Add Module');
            $('#btnSubmitModule').attr('btn-process', 'add');
        });

        $('#btnModuleActionPageToggle').unbind().click(function () {
            if ($('#btnSubmitModuleAction').attr('btn-process') === 'update') {
                moduleActFnc.ClearFields();
            }
        });

        $('#btnSubmitModuleAction').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var moduleActionDetails = {
                moduleId: $('#cbxModuleActionMod').val(),
                actionId: $('#cbxModuleActionAc').val(),
                userId: '1'
            };

            if (hasNull(moduleActionDetails) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new module action?'
                    }).done(function () {
                        moduleActFnc.AddModuleAction(moduleActionDetails.moduleId, moduleActionDetails.actionId, moduleActionDetails.userId);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update module action details?'
                    }).done(function () {
                        moduleActionDetails.moduleActionId = _userManagerGlobal.moduleActionId;
                        moduleActFnc.ModifyModuleAction(moduleActionDetails.moduleActionId, moduleActionDetails.moduleId, moduleActionDetails.actionId);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllModules = function () {
        /// <summary>
        /// Gets All Module Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Module Information</returns>

        $.post("../UserManager/GetAllModules", function (response) {
            var modulesObj = JSON.parse(response);

            if (modulesObj.Status === 1) {
                var htmlString = '';
                var moduleData = modulesObj.Data;

                $.each(moduleData, function (index) {
                    htmlString += '<option value="' + moduleData[index].MODULE_ID + '">' + moduleData[index].MODULE_NAME + '</option>';
                });

                $('#cbxModuleActionMod').append(htmlString);
                $('#cbxModuleActionMod').selectpicker('refresh');
            } else {
                MaterialNotify(modulesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllActions = function () {
        /// <summary>
        /// Gets All Action Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Action Information</returns>

        $.post("../UserManager/GetAllActions", function (response) {
            var actionsObj = JSON.parse(response);

            if (actionsObj.Status === 1) {
                var htmlString = '';
                var actionsData = actionsObj.Data;

                $.each(actionsData, function (index) {
                    htmlString += '<option value="' + actionsData[index].ACTION_ID + '">' + actionsData[index].ACTION_NAME + '</option>';
                });

                $('#cbxModuleActionAc').append(htmlString);
                $('#cbxModuleActionAc').selectpicker('refresh');
            } else {
                MaterialNotify(actionsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllModuleActions = function () {
        /// <summary>
        /// Gets All Module Action Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Module Action Information</returns>

        $.post("../UserManager/GetAllModuleActions", function (response) {
            var moduleActionsObj = JSON.parse(response);

            if (moduleActionsObj.Status === 1) {
                moduleActTbl = $('#tblModuleActions').DataTable({
                    info: false,
                    data: moduleActionsObj.Data,
                    columns: [
                        {
                            data: 'MODULE_NAME',
                            render: function (data, type, full, meta) {
                                return '<span class="module-name">' + data + '</span><br/><div class="nav-container"><a class="edit-module-action active" data-module-action-id="' + full.MODULE_ACTION_ID + '" href="#" data-toggle="modal" data-target="#modalAddModuleAction"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'MODULE_DESC'
                        },
                        {
                            data: 'ACTION_NAME'
                        },
                        {
                            data: 'ACTION_DESC'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(moduleActionsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetModuleAction = function (moduleActionId) {
        /// <summary>
        /// Gets Specific Module Action Information based on Module Action ID
        /// </summary>
        /// <param name="moduleActionId" type="int">ID of the Specific Module Action</param>
        /// <returns type="Json Object">Returns the Information of Specific Module Action</returns>

        $.post("../UserManager/GetModuleAction", { moduleActionId: moduleActionId }, function (response) {
            var moduleActionObj = JSON.parse(response);

            if (moduleActionObj.Status === 1) {
                $('#cbxModuleActionMod').selectpicker('val', moduleActionObj.Data[0].MODULE_ID);
                $('#cbxModuleActionAc').selectpicker('val', moduleActionObj.Data[0].ACTION_ID);

                $('#modalAddModuleAction').addClass('update');
                $('#btnSubmitModuleAction').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitModuleAction').attr('btn-process', 'update');
            } else {
                MaterialNotify(modulesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddModuleAction = function (moduleId, actionId, userId) {
        /// <summary>
        /// Adds New Module Action Information
        /// </summary>
        /// <param name="moduleId" type="int">ID of the Module</param>
        /// <param name="actionId" type="int">ID of the Action</param>
        /// <param name="userId" type="int">ID of the User</param>
        /// <returns type="Json Object">Returns Inserted Module Action Information in the Database</returns>

        $.post("../UserManager/AddModuleAction", { moduleId: moduleId, actionId: actionId, userId: userId }, function (response) {
            var moduleActionObj = JSON.parse(response);

            if (moduleActionObj.Status === 1) {
                $('#modalAddModuleAction').modal('hide');
                moduleActFnc.ClearFields();
                moduleActTbl.row.add(moduleActionObj.Data[0]).draw();
                MaterialNotify(moduleActionObj.Message, 'success');
            } else {
                MaterialNotify(moduleActionObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyModuleAction = function (moduleActionId, moduleId, actionId) {
        /// <summary>
        /// Modifies the Information of Module Action
        /// </summary>
        /// <param name="moduleActionId" type="int">ID of the Module Action</param>
        /// <param name="moduleId" type="int">ID of the Module</param>
        /// <param name="actionId" type="int">ID of the Action</param>
        /// <returns type="Json Object">Returns Modified Information of Module Action in the Database</returns>

        $.post("../UserManager/ModifyModuleAction", { moduleActionId: moduleActionId, moduleId: moduleId, actionId: actionId }, function (response) {
            var moduleActionObj = JSON.parse(response);

            if (moduleActionObj.Status === 1) {
                $('#modalAddModuleAction').modal('hide');
                moduleActFnc.ClearFields();
                moduleActTbl.row(moduleActRow).data(moduleActionObj.Data[0]).draw();
                MaterialNotify(moduleActionObj.Message, 'success');
            } else {
                MaterialNotify(moduleActionObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Module Action Page
        /// </summary>

        $('#cbxModuleActionMod').selectpicker('val', null);
        $('#cbxModuleActionAc').selectpicker('val', null);

        $('#modalAddModuleAction').removeClass('update');
        $('#btnSubmitModuleAction').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitModuleAction').attr('btn-process', 'add');
    };
}

function Role() {
    /// <summary>
    /// User Role Class Function
    /// </summary>

    var roleFnc = this;
    var roleTbl = null;
    var roleRow = null;

    this.InitRoleComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        $('#tblUserRoles').on('click', '.nav-container .edit-user-role', function (e) {
            _userManagerGlobal.userRoleId = $(this).data('role-id');
            roleFnc.GetUserRole($(this).data('role-id'));

            var tblObj = $(this);
            roleRow = roleTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnRolePageToggle').unbind().click(function () {
            if ($('#btnSubmitRole').attr('btn-process') === 'update') {
                if ($('#chkRoleStatus').prop('checked') === false) {
                    $('#chkRoleStatus').click();
                }

                $('#txtRoleName').val('');
                $('#txtRoleDescription').val('');
                $('#modalAddUserRole').removeClass('update');
                $('#btnSubmitRole').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
                $('#btnSubmitRole').attr('btn-process', 'add');
            }
        });

        $('#btnSubmitRole').unbind().click(function () {
            var process = $(this).attr('btn-process');
            var isActive = null;

            if ($('#chkRoleStatus').prop('checked') === true) {
                isActive = '1';
            } else {
                isActive = '0';
            }

            var roleDetails = {
                Name: $('#txtRoleName').val(),
                Description: $('#txtRoleDescription').val(),
                IsActive: isActive
            };

            if (hasNull(roleDetails) !== true) {

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new user role?'
                    }).done(function () {
                        roleFnc.AddUserRole(roleDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update user role details?'
                    }).done(function () {
                        roleDetails.Id = _userManagerGlobal.userRoleId;
                        roleFnc.ModifyUserRole(roleDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllUserRoles = function () {
        /// <summary>
        /// Gets All User Role Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of User Role Information</returns>

        $.post("../UserManager/GetAllUserRoles", function (response) {
            var userRolesObj = JSON.parse(response);

            if (userRolesObj.Status === 1) {
                roleTbl = $('#tblUserRoles').DataTable({
                    info: false,
                    data: userRolesObj.Data,
                    columns: [
                        {
                            data: 'ROLE_NAME',
                            render: function (data, type, full, meta) {
                                return '<span class="role-name">' + data + '</span><br/><div class="nav-container"><a class="edit-user-role active" data-role-id="' + full.ROLE_ID + '" href="#" data-toggle="modal" data-target="#modalAddUserRole"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        },
                        {
                            data: 'IS_ACTIVE',
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Inactive</div>';
                                }
                            }
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(userRolesObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetUserRole = function (roleId) {
        /// <summary>
        /// Gets Specific User Role Information based on User ID
        /// </summary>
        /// <param name="roleId" type="int">ID of the Specific User Role</param>
        /// <returns type="Json Object">Returns the Information of Specific User Role</returns>

        $.post("../UserManager/GetUserRole", { roleId: roleId }, function (response) {
            var roleObj = JSON.parse(response);

            if (roleObj.Status === 1) {
                $('#txtRoleName').val(roleObj.Data[0].ROLE_NAME);
                $('#txtRoleDescription').val(roleObj.Data[0].DESCRIPTION);

                if (roleObj.Data[0].IS_ACTIVE === 1 && $('#chkRoleStatus').prop('checked') === false) {
                    $('#chkRoleStatus').click();
                } else if (roleObj.Data[0].IS_ACTIVE !== 1 && $('#chkRoleStatus').prop('checked') === true) {
                    $('#chkRoleStatus').click();
                }

                $('#modalAddUserRole').addClass('update');
                $('#btnSubmitRole').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitRole').attr('btn-process', 'update');
            } else {
                MaterialNotify(roleObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddUserRole = function (roleDetails) {
        /// <summary>
        /// Adds New User Role Information
        /// </summary>
        /// <param name="roleDetails" type="object">User Role Details[Name, Description, IsActive, CreatedBy]</param>
        /// <returns type="Json Object">Returns Inserted User Role Information in the Database</returns>

        $.post("../UserManager/AddUserRole", { roleModel: roleDetails }, function (response) {
            var userRoleObj = JSON.parse(response);

            if (userRoleObj.Status === 1) {
                MaterialNotify(userRoleObj.Message, 'success');
                roleFnc.ClearFields();
                roleTbl.row.add(userRoleObj.Data[0]).draw();
                $('#modalAddUserRole').modal('hide');
            } else {
                MaterialNotify(userRoleObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyUserRole = function (roleDetails) {
        /// <summary>
        /// Modifies the Information of User Role
        /// </summary>
        /// <param name="roleDetails" type="object">User Role Details[Id, Name, Description, IsActive, UpdatedBy]</param>
        /// <returns type="Json Object">Returns Modified Information of User Role in the Database</returns>

        $.post("../UserManager/ModifyUserRole", { roleModel: roleDetails }, function (response) {
            var userRoleObj = JSON.parse(response);

            if (userRoleObj.Status === 1) {
                MaterialNotify(userRoleObj.Message, 'success');
                roleFnc.ClearFields();
                roleTbl.row(roleRow).data(userRoleObj.Data[0]).draw();
                $('#modalAddUserRole').modal('hide');
            } else {
                MaterialNotify(userRoleObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in User Role Page
        /// </summary>

        if ($('#chkRoleStatus').prop('checked') === false) {
            $('#chkRoleStatus').click();
        }

        $('#txtRoleName').val('');
        $('#txtRoleDescription').val('');
        $('#modalAddUserRole').removeClass('update');
        $('#btnSubmitRole').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitRole').attr('btn-process', 'add');
    };
}

function HierarchyIdentifier() {
    /// <summary>
    /// Hierarchy Identifier Class Function
    /// </summary>
    var hierFnc = this;
    var hierTbl = null;
    var hierRow = null;
     
    this.InitHrchyIdentifierComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        $('#tblHierarchyIdentifiers').on('click', '.nav-container .edit-hier-idn', function (e) {
            _userManagerGlobal.hierarchyIdentifierId = $(this).data('hier-idn-id');
            hierFnc.GetHierarchyIdentifier($(this).data('hier-idn-id'));

            var tblObj = $(this);
            hierRow = hierTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnHierIdnPageToggle').unbind().click(function () {
            if ($('#btnSubmitHierIdn').attr('btn-process') === 'update') {
                hierFnc.ClearFields();
            }
        });

        $('#btnSubmitHierIdn').unbind().click(function () {
            var process = $(this).attr('btn-process');
            var hierarchyIdentifierDesc = $('#txtHierIdnDesc').val();

            if (hierarchyIdentifierDesc !== '') {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new hierarchy identifier?'
                    }).done(function () {
                        hierFnc.AddHierarchyIdentifier(hierarchyIdentifierDesc);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update hierarchy identifier details?'
                    }).done(function () {
                        hierFnc.ModifyHierarchyIdentifier(_userManagerGlobal.hierarchyIdentifierId, hierarchyIdentifierDesc);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllHierarchyIdentifiers = function () {
        /// <summary>
        /// Gets All Hierarchy Identifier Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Hierarchy Identifier Information</returns>

        $.post("../UserManager/GetAllHierarchyIdentifier", function (response) {
            var hierIdnsObj = JSON.parse(response);

            if (hierIdnsObj.Status === 1) {
                hierTbl = $('#tblHierarchyIdentifiers').DataTable({
                    info: false,
                    data: hierIdnsObj.Data,
                    columns: [
                        {
                            data: 'HRCHY_IDN_ID',
                            render: function (data, type, full, meta) {
                                return '<span class="role-name">' + data + '</span><br/><div class="nav-container"><a class="edit-hier-idn active" data-hier-idn-id="' + full.HRCHY_IDN_ID + '" href="#" data-toggle="modal" data-target="#modalAddHierIdn"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(hierIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetHierarchyIdentifier = function (hierarchyIdentifierId) {
        /// <summary>
        /// Gets Specific Hierarchy Identifier Information based on Hierarchy Identifier ID
        /// </summary>
        /// <param name="hierarchyIdentifierId" type="int">ID of the Specific Hierarchy Identifier</param>
        /// <returns type="Json Object">Returns the Information of Specific Hierarchy Identifier</returns>

        $.post("../UserManager/GetHierarchyIdentifier", { hierarchyIdentifierId: hierarchyIdentifierId }, function (response) {
            var hierIdnsObj = JSON.parse(response);

            if (hierIdnsObj.Status === 1) {
                $('#txtHierIdnDesc').val(hierIdnsObj.Data[0].DESCRIPTION);

                $('#modalAddHierIdn').addClass('update');
                $('#btnSubmitHierIdn').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitHierIdn').attr('btn-process', 'update');
            } else {
                MaterialNotify(hierIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddHierarchyIdentifier = function (hierarchyIdentifierDesc) {
        /// <summary>
        /// Adds New Hierarchy Identifier Information
        /// </summary>
        /// <param name="hierarchyIdentifierDesc" type="string">Description of the Hierarchy Identifier</param>
        /// <returns type="Json Object">Returns Inserted Hierarchy Identifier Information in the Database</returns>

        $.post("../UserManager/AddHierarchyIdentifier", { hierarchyIdentifierDesc: hierarchyIdentifierDesc }, function (response) {
            var hierIdnsObj = JSON.parse(response);

            if (hierIdnsObj.Status === 1) {
                MaterialNotify(hierIdnsObj.Message, 'success');
                hierFnc.ClearFields();
                hierTbl.row.add(hierIdnsObj.Data[0]).draw();
                $('#modalAddHierIdn').modal('hide');
            } else {
                MaterialNotify(hierIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyHierarchyIdentifier = function (hierarchyIdentifierId, description) {
        /// <summary>
        /// Modifies the Information of Hierarchy Identifier
        /// </summary>
        /// <param name="hierarchyIdentifierId" type="int">ID of the Hierarchy Identifier</param>
        /// <param name="description" type="string">Description of the Hierarchy Identifier</param>
        /// <returns type="Json Object">Returns Modified Information of Hierarchy Identifier in the Database</returns>

        $.post("../UserManager/ModifyHierarchyIdentifier", { hierarchyIdentifierId: hierarchyIdentifierId, description: description }, function (response) {
            var hierIdnsObj = JSON.parse(response);

            if (hierIdnsObj.Status === 1) {
                MaterialNotify(hierIdnsObj.Message, 'success');
                hierFnc.ClearFields();
                hierTbl.row(hierRow).data(hierIdnsObj.Data[0]).draw();
                $('#modalAddHierIdn').modal('hide');
            } else {
                MaterialNotify(hierIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Hierarchy Identifier Page
        /// </summary>

        $('#txtHierIdnDesc').val('');

        $('#modalAddHierIdn').removeClass('update');
        $('#btnSubmitHierIdn').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitHierIdn').attr('btn-process', 'add');
    };
}

function DataIdentifier() {
    /// <summary>
    /// Data Identifier Class Function
    /// </summary>

    var dataFnc = this;
    var dataTbl = null;
    var dataRow = null;

    this.InitDataIdentifierComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        $('#tblDataIdentifiers').on('click', '.nav-container .edit-data-idn', function (e) {
            _userManagerGlobal.dataIdentifierId = $(this).data('data-idn-id');
            dataFnc.GetDataIdentifier($(this).data('data-idn-id'));

            var tblObj = $(this);
            dataRow = dataTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnDataIdnPageToggle').unbind().click(function () {
            if ($('#btnSubmitDataIdn').attr('btn-process') === 'update') {
                dataFnc.ClearFields();
            }
        });

        $('#btnSubmitDataIdn').unbind().click(function () {
            var process = $(this).attr('btn-process');
            var dataIdentifierDesc = $('#txtDataIdnDesc').val();

            if (dataIdentifierDesc !== '') {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new data identifier?'
                    }).done(function () {
                        dataFnc.AddDataIdentifier(dataIdentifierDesc);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update data identifier details?'
                    }).done(function () {
                        dataFnc.ModifyDataIdentifier(_userManagerGlobal.dataIdentifierId, dataIdentifierDesc);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllDataIdentifiers = function () {
        /// <summary>
        /// Gets All Data Identifier Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Data Identifier Information</returns>

        $.post("../UserManager/GetAllDataIdentifier", function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                dataTbl = $('#tblDataIdentifiers').DataTable({
                    info: false,
                    data: dataIdnsObj.Data,
                    columns: [
                        {
                            data: 'DATA_IDN_ID',
                            render: function (data, type, full, meta) {
                                return '<span class="role-name">' + data + '</span><br/><div class="nav-container"><a class="edit-data-idn active" data-data-idn-id="' + full.DATA_IDN_ID + '" href="#" data-toggle="modal" data-target="#modalAddDataIdn"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetDataIdentifier = function (dataIdentifierId) {
        /// <summary>
        /// Gets Specific Data Identifier Information based on Data Identifier ID
        /// </summary>
        /// <param name="dataIdentifierId" type="int">ID of the Specific Data Identifier</param>
        /// <returns type="Json Object">Returns the Information of Specific Data Identifier</returns>

        $.post("../UserManager/GetDataIdentifier", { dataIdentifierId: dataIdentifierId }, function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                $('#txtDataIdnDesc').val(dataIdnsObj.Data[0].DESCRIPTION);

                $('#modalAddDataIdn').addClass('update');
                $('#btnSubmitDataIdn').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitDataIdn').attr('btn-process', 'update');
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddDataIdentifier = function (dataIdentifierDesc) {
        /// <summary>
        /// Adds New Data Identifier Information
        /// </summary>
        /// <param name="dataIdentifierDesc" type="string">Description of the Data Identifier</param>
        /// <returns type="Json Object">Returns Inserted Data Identifier Information in the Database</returns>

        $.post("../UserManager/AddDataIdentifier", { dataIdentifierDesc: dataIdentifierDesc }, function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                MaterialNotify(dataIdnsObj.Message, 'success');
                dataFnc.ClearFields();
                dataTbl.row.add(dataIdnsObj.Data[0]).draw();
                $('#modalAddDataIdn').modal('hide');
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyDataIdentifier = function (dataIdentifierId, description) {
        /// <summary>
        /// Modifies the Information of DataIdentifier
        /// </summary>
        /// <param name="dataIdentifierId" type="int">ID of the Data Identifier</param>
        /// <param name="description" type="string">Description of the Data Identifier</param>
        /// <returns type="Json Object">Returns Modified Information of Data Identifier in the Database</returns>

        $.post("../UserManager/ModifyDataIdentifier", { dataIdentifierId: dataIdentifierId, description: description }, function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                MaterialNotify(dataIdnsObj.Message, 'success');
                dataFnc.ClearFields();
                dataTbl.row(dataRow).data(dataIdnsObj.Data[0]).draw();
                $('#modalAddDataIdn').modal('hide');
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Data Identifier Page
        /// </summary>

        $('#txtDataIdnDesc').val('');

        $('#modalAddDataIdn').removeClass('update');
        $('#btnSubmitDataIdn').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitDataIdn').attr('btn-process', 'add');
    };
}

function OrganizationHierarchy() {
    /// <summary>
    /// Org Hierarchy Class Function
    /// </summary>

    var orgFnc = this;
    var orgTbl = null;
    var orgRow = null;

    this.InitOrgHrchyComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>
        
        orgFnc.PopulateParentPicker();
        orgFnc.PopulateHierarchyIdentifierPicker();

        $('#tblOrgHierarchy').on('click', '.nav-container .edit-org-hier', function (e) {
            _userManagerGlobal.orgHierarchyId = $(this).data('org-hier-id');
            orgFnc.GetOrgHierarchy($(this).data('org-hier-id'));

            var tblObj = $(this);
            orgRow = orgTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnOrgHierPageToggle').unbind().click(function () {
            if ($('#btnSubmitOrgHierarchy').attr('btn-process') === 'update') {
                orgFnc.ClearFields();
            }
        });

        $('#btnSubmitOrgHierarchy').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var orgHierarchyDetails = {
                ReferenceId: $('#txtOrgHierarchyRefId').val(),
                HierarchyIdentifierId: $('#cbxOrgHierarchyIdentifiers').val()
            };

            if (hasNull(orgHierarchyDetails) !== true) {
                orgHierarchyDetails.ParentId = $("#cbxOrgHierarchyParents").val() || null;

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new organization hierarchy?'
                    }).done(function () {
                        orgFnc.AddOrgHierarchy(orgHierarchyDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update organization hierarchy details?'
                    }).done(function () {
                        orgHierarchyDetails.Id = _userManagerGlobal.orgHierarchyId;
                        orgFnc.ModifyOrgHierarchy(orgHierarchyDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllOrgHierarchy = function () {
        /// <summary>
        /// Gets All Org Hierarchy Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Org Hierarchy Information</returns>

        $.post("../UserManager/GetAllOrgHierarchy", function (response) {
            var orgHierarchyObj = JSON.parse(response);

            if (orgHierarchyObj.Status === 1) {
                orgTbl = $('#tblOrgHierarchy').DataTable({
                    info: false,
                    data: orgHierarchyObj.Data,
                    columns: [
                        {
                            data: 'ORG_HRCHY_ID',
                            render: function (data, type, full, meta) {
                                return '<span>' + data + '</span><br/><div class="nav-container"><a class="edit-org-hier active" data-org-hier-id="' + full.ORG_HRCHY_ID + '" href="#" data-toggle="modal" data-target="#modalAddOrgHier"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'PARENT_ID',
                            render: function (data, type, full, meta) {
                                if (data === null) {
                                    return '';
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'HRCHY_IDN_DESC'
                        },
                        {
                            data: 'HRCHY_IDN_DESC'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(orgHierarchyObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetOrgHierarchy = function (orgHierarchyId) {
        /// <summary>
        /// Gets Specific Org Hierarchy Information based on Org Hierarchy ID
        /// </summary>
        /// <param name="orgHierarchyId" type="int">ID of the Specific Org Hierarchy</param>
        /// <returns type="Json Object">Returns the Information of Specific Org Hierarchy</returns>

        $.post("../UserManager/GetOrgHierarchy", { orgHierarchyId: orgHierarchyId }, function (response) {
            var orgHierarchyObj = JSON.parse(response);

            if (orgHierarchyObj.Status === 1) {
                $('#txtOrgHierarchyRefId').val(orgHierarchyObj.Data[0].HRCHY_IDN_DESC);
                $('#cbxOrgHierarchyParents').selectpicker('val', orgHierarchyObj.Data[0].PARENT_ID);
                $('#cbxOrgHierarchyIdentifiers').selectpicker('val', orgHierarchyObj.Data[0].HRCHY_IDN_ID);

                $('#modalAddOrgHier').addClass('update');
                $('#btnSubmitOrgHierarchy').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitOrgHierarchy').attr('btn-process', 'update');
            } else {
                MaterialNotify(orgHierarchyObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateParentPicker = function () {
        /// <summary>
        /// Get All Org Hierarchy Information for Parent
        /// </summary>
        /// <returns type="Json Object">Returns List of Org Hierarchy Information</returns>

        $.post("../UserManager/GetAllOrgHierarchy", function (response) {
            var orgHierarchyObj = JSON.parse(response);

            if (orgHierarchyObj.Status === 1) {
                var htmlString = '';

                if (orgHierarchyObj.Status === 1) {
                    var orgHierarchy = orgHierarchyObj.Data;

                    $.each(orgHierarchy, function (index) {
                        htmlString += '<option value="' + orgHierarchy[index].ORG_HRCHY_ID + '">' + orgHierarchy[index].ORG_HRCHY_ID + '</option>';
                    });

                    $('#cbxOrgHierarchyParents').append(htmlString);
                    $('#cbxOrgHierarchyParents').selectpicker('refresh');
                } else {
                    MaterialNotify(orgHierarchyObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateHierarchyIdentifierPicker = function () {
        /// <summary>
        /// Get All Hierarchy Identifier Information
        /// </summary>
        /// <returns type="Json Object">Returns List of Hierarchy Identifier Information</returns>

        $.post("../UserManager/GetAllHierarchyIdentifier", function (response) {
            var hierarchyIdnObj = JSON.parse(response);

            if (hierarchyIdnObj.Status === 1) {
                var htmlString = '';

                if (hierarchyIdnObj.Status === 1) {
                    var hierarchyIdn = hierarchyIdnObj.Data;

                    $.each(hierarchyIdn, function (index) {
                        htmlString += '<option value="' + hierarchyIdn[index].HRCHY_IDN_ID + '">' + hierarchyIdn[index].DESCRIPTION + '</option>';
                    });

                    $('#cbxOrgHierarchyIdentifiers').append(htmlString);
                    $('#cbxOrgHierarchyIdentifiers').selectpicker('refresh');
                } else {
                    MaterialNotify(hierarchyIdnObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddOrgHierarchy = function (orgHierarchyDetails) {
        /// <summary>
        /// Adds New Org Hierarchy Information
        /// </summary>
        /// <param name="orgHierarchyDetails" type="object">Org Hierarchy Details[ParentId, ReferenceId, HierarchyIdentifierId]</param>
        /// <returns type="Json Object">Returns Inserted Org Hierarchy Information in the Database</returns>

        $.post("../UserManager/AddOrgHierarchy", { orgHierarchyModel: orgHierarchyDetails }, function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                MaterialNotify(dataIdnsObj.Message, 'success');
                orgFnc.ClearFields();
                orgTbl.row.add(dataIdnsObj.Data[0]).draw();
                $('#modalAddOrgHier').modal('hide');
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyOrgHierarchy = function (orgHierarchyDetails) {
        /// <summary>
        /// Modifies the Information of Org Hierarchy
        /// </summary>
        /// <param name="orgHierarchyDetails" type="object">Org Hierarchy Details[Id, ParentId, ReferenceId, HierarchyIdentifierId]</param>
        /// <returns type="Json Object">Returns Modified Information of Org Hierarchy in the Database</returns>

        $.post("../UserManager/ModifyOrgHierarchy", { orgHierarchyModel: orgHierarchyDetails }, function (response) {
            var dataIdnsObj = JSON.parse(response);

            if (dataIdnsObj.Status === 1) {
                MaterialNotify(dataIdnsObj.Message, 'success');
                orgFnc.ClearFields();
                orgTbl.row(orgRow).data(dataIdnsObj.Data[0]).draw();
                $('#modalAddOrgHier').modal('hide');
            } else {
                MaterialNotify(dataIdnsObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Org Hierarchy Page
        /// </summary>

        $('#txtOrgHierarchyRefId').val('');
        $('#cbxOrgHierarchyParents').selectpicker('val', null);
        $('#cbxOrgHierarchyIdentifiers').selectpicker('val', null);

        $('#modalAddOrgHier').removeClass('update');
        $('#btnSubmitOrgHierarchy').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitOrgHierarchy').attr('btn-process', 'add');
    };
}

function DataHierarchy() {
    /// <summary>
    /// Data Hierarchy Class Function
    /// </summary>

    var dataHierFnc = this;
    var dataHierTbl = null;
    var dataHierRow = null;

    this.InitDataHrchyComponents = function () {
        /// <summary>
        /// Initialize Page Events in User Page
        /// </summary>

        dataHierFnc.PopulateParentPicker();
        dataHierFnc.PopulateDataIdentifierPicker();

        $('#tblDataHierarchy').on('click', '.nav-container .edit-data-hier', function (e) {
            _userManagerGlobal.dataHierarchyId = $(this).data('data-hier-id');
            dataHierFnc.GetDataHierarchy($(this).data('data-hier-id'));

            var tblObj = $(this);
            dataHierRow = dataHierTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnDataHierPageToggle').unbind().click(function () {
            if ($('#btnSubmitDataHierarchy').attr('btn-process') === 'update') {
                dataHierFnc.ClearFields();
            }
        });

        $('#btnSubmitDataHierarchy').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var dataHierarchyDetails = {
                ReferenceId: $('#txtDataHierarchyRefId').val(),
                DataIdentifierId: $('#cbxDataHierarchyIdentifiers').val()
            };

            if (hasNull(dataHierarchyDetails) !== true) {
                dataHierarchyDetails.ParentId = $("#cbxDataHierarchyParents").val() || null;

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new data hierarchy?'
                    }).done(function () {
                        dataHierFnc.AddDataHierarchy(dataHierarchyDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update data hierarchy details?'
                    }).done(function () {
                        dataHierarchyDetails.Id = _userManagerGlobal.dataHierarchyId;
                        dataHierFnc.ModifyDataHierarchy(dataHierarchyDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllDataHierarchy = function () {
        /// <summary>
        /// Gets All Data Hierarchy Information that has been inserted in the Database
        /// </summary>
        /// <returns type="Json Object">Returns List of Data Hierarchy Information</returns>

        $.post("../UserManager/GetAllDataHierarchy", function (response) {
            var dataHierarchyObj = JSON.parse(response);

            if (dataHierarchyObj.Status === 1) {
                dataHierTbl = $('#tblDataHierarchy').DataTable({
                    info: false,
                    data: dataHierarchyObj.Data,
                    columns: [
                        {
                            data: 'DATA_HRCHY_ID',
                            render: function (data, type, full, meta) {
                                return '<span>' + data + '</span><br/><div class="nav-container"><a class="edit-data-hier active" data-data-hier-id="' + full.DATA_HRCHY_ID + '" href="#" data-toggle="modal" data-target="#modalAddDataHier"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: 'PARENT_ID',
                            render: function (data, type, full, meta) {
                                if (data === null) {
                                    return '';
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'DESCRIPTION'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(dataHierarchyObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetDataHierarchy = function (dataHierarchyId) {
        /// <summary>
        /// Gets Specific Data Hierarchy Information based on User ID
        /// </summary>
        /// <param name="dataHierarchyId" type="int">ID of the Specific Data Hierarchy</param>
        /// <returns type="Json Object">Returns the Information of Specific Data Hierarchy</returns>

        $.post("../UserManager/GetDataHierarchy", { dataHierarchyId: dataHierarchyId }, function (response) {
            var dataHierarchyObj = JSON.parse(response);

            if (dataHierarchyObj.Status === 1) {
                $('#txtDataHierarchyRefId').val(dataHierarchyObj.Data[0].REFERENCE_ID);
                $('#cbxDataHierarchyParents').selectpicker('val', dataHierarchyObj.Data[0].PARENT_ID);
                $('#cbxDataHierarchyIdentifiers').selectpicker('val', dataHierarchyObj.Data[0].DATA_IDN_ID);

                $('#modalAddDataHier').addClass('update');
                $('#btnSubmitDataHierarchy').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitDataHierarchy').attr('btn-process', 'update');
            } else {
                MaterialNotify(dataHierarchyObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateParentPicker = function () {
        /// <summary>
        /// Get All Data Hierarchy Information for Parent
        /// </summary>
        /// <returns type="Json Object">Returns List of Data Hierarchy Information</returns>

        $.post("../UserManager/GetAllDataHierarchy", function (response) {
            var dataHierarchyObj = JSON.parse(response);

            if (dataHierarchyObj.Status === 1) {
                var htmlString = '';

                if (dataHierarchyObj.Status === 1) {
                    var dataHierarchy = dataHierarchyObj.Data;

                    $.each(dataHierarchy, function (index) {
                        htmlString += '<option value="' + dataHierarchy[index].DATA_HRCHY_ID + '">' + dataHierarchy[index].DATA_HRCHY_ID + '</option>';
                    });

                    $('#cbxDataHierarchyParents').append(htmlString);
                    $('#cbxDataHierarchyParents').selectpicker('refresh');
                } else {
                    MaterialNotify(dataHierarchyObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateDataIdentifierPicker = function () {
        /// <summary>
        /// Get All Data Identifier Information
        /// </summary>
        /// <returns type="Json Object">Returns List of Data Identifier Information</returns>

        $.post("../UserManager/GetAllDataIdentifier", function (response) {
            var dataIdnObj = JSON.parse(response);

            if (dataIdnObj.Status === 1) {
                var htmlString = '';

                if (dataIdnObj.Status === 1) {
                    var dataIdn = dataIdnObj.Data;

                    $.each(dataIdn, function (index) {
                        htmlString += '<option value="' + dataIdn[index].DATA_IDN_ID + '">' + dataIdn[index].DESCRIPTION + '</option>';
                    });

                    $('#cbxDataHierarchyIdentifiers').append(htmlString);
                    $('#cbxDataHierarchyIdentifiers').selectpicker('refresh');
                } else {
                    MaterialNotify(dataIdnObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddDataHierarchy = function (dataHierarchyDetails) {
        /// <summary>
        /// Adds New Data Hierarchy Information
        /// </summary>
        /// <param name="dataHierarchyDetails" type="object">Data Hierarchy Details[ParentId, ReferenceId, DataIdentifierId]</param>
        /// <returns type="Json Object">Returns Inserted Data Hierarchy Information in the Database</returns>

        $.post("../UserManager/AddDataHierarchy", { dataHierarchyModel: dataHierarchyDetails }, function (response) {
            var dataHierObj = JSON.parse(response);

            if (dataHierObj.Status === 1) {
                MaterialNotify(dataHierObj.Message, 'success');
                dataHierFnc.ClearFields();
                dataHierTbl.row.add(dataHierObj.Data[0]).draw();
                $('#modalAddDataHier').modal('hide');
            } else {
                MaterialNotify(dataHierObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyDataHierarchy = function (dataHierarchyDetails) {
        /// <summary>
        /// Modifies the Information of Data Hierarchy
        /// </summary>
        /// <param name="dataHierarchyDetails" type="object">Data Hierarchy Details[Id, ParentId, ReferenceId, DataIdentifierId]</param>
        /// <returns type="Json Object">Returns Modified Information of Data Hierarchy in the Database</returns>

        $.post("../UserManager/ModifyDataHierarchy", { dataHierarchyModel: dataHierarchyDetails }, function (response) {
            var dataHierObj = JSON.parse(response);

            if (dataHierObj.Status === 1) {
                MaterialNotify(dataHierObj.Message, 'success');
                dataHierFnc.ClearFields();
                dataHierTbl.row(dataHierRow).data(dataHierObj.Data[0]).draw();
                $('#modalAddDataHier').modal('hide');
            } else {
                MaterialNotify(dataHierObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        /// <summary>
        /// Clear All Text Fields in Data Hierarchy Page
        /// </summary>

        $('#txtDataHierarchyRefId').val('');
        $('#cbxDataHierarchyParents').selectpicker('val', null);
        $('#cbxDataHierarchyIdentifiers').selectpicker('val', null);

        $('#modalAddDataHier').removeClass('update');
        $('#btnSubmitDataHierarchy').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitDataHierarchy').attr('btn-process', 'add');
    };
}

function DataAccess() {

    var accessFnc = this;
    var accessTbl = null;
    var accessRow = null;

    this.InitComponents = function () {
        accessFnc.PopulateUserAccess();
        accessFnc.PopulateDataHierarchyPicker();
        accessFnc.PopulateModulePicker();

        $('#tblDataAccess').on('click', '.nav-container .edit-data-access', function (e) {
            _userManagerGlobal.dataAccessId = $(this).data('data-access-id');
            accessFnc.GetDataAccess($(this).data('data-access-id'));

            var tblObj = $(this);
            accessRow = accessTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnDataAccessPageToggle').unbind().click(function () {
            if ($('#btnSubmitDataAccess').attr('btn-process') === 'update') {
                accessFnc.ClearFields();
            }
        });

        $('#btnSubmitDataAccess').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var dataAccessDetails = {
                UserAccessId: $('#cbxDataAccessUserAccess').val(),
                DataHierarchyId: $('#cbxDataAccessHierarchy').val(),
                ModuleId: $('#cbxDataAccessModule').val()
            };

            if (hasNull(dataAccessDetails) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new role access?'
                    }).done(function () {
                        accessFnc.AddDataAccess(dataAccessDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update role access details?'
                    }).done(function () {
                        dataAccessDetails.Id = _userManagerGlobal.dataAccessId;
                        accessFnc.ModifyDataAccess(dataAccessDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllDataAccess = function () {
        $.post("../UserManager/GetAllDataAccess", function (response) {
            var dataAccessObj = JSON.parse(response);

            if (dataAccessObj.Status === 1) {
                accessTbl = $('#tblDataAccess').DataTable({
                    info: false,
                    data: dataAccessObj.Data,
                    columns: [
                        {
                            data: 'DATA_ACCESS_ID',
                            render: function (data, type, full, meta) {
                                return '<span>' + data + '</span><br/><div class="nav-container"><a class="edit-data-access active" data-data-access-id="' + full.DATA_ACCESS_ID + '" href="#" data-toggle="modal" data-target="#modalAddDataAccess"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: {
                                role_name: 'ROLE_NAME',
                                module_name: 'MODULE_NAME'
                            },
                            render: function (data, type, full, meta) {
                                return data.ROLE_NAME + ' - ' + data.MODULE_NAME;
                            }
                        },
                        {
                            data: {
                                first_name: 'FIRST_NAME',
                                middle_name: 'MIDDLE_NAME',
                                last_name: 'LAST_NAME',
                            },
                            render: function (data, type, full, meta) {
                                return data.LAST_NAME + ', ' + data.FIRST_NAME + ' ' + data.MIDDLE_NAME;
                            }
                        },
                        {
                            data: 'DATA_HRCHY_ID'
                        },
                        {
                            data: 'MODULE_NAME'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(dataAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetDataAccess = function (dataAccessId) {
        $.post("../UserManager/GetDataAccess", { dataAccessId: dataAccessId }, function (response) {
            var dataAccessObj = JSON.parse(response);

            if (dataAccessObj.Status === 1) {
                $('#cbxDataAccessUserAccess').selectpicker('val', dataAccessObj.Data[0].USER_ACCESS_ID);
                $('#cbxDataAccessHierarchy').selectpicker('val', dataAccessObj.Data[0].DATA_HRCHY_ID);
                $('#cbxDataAccessModule').selectpicker('val', dataAccessObj.Data[0].MODULE_ID);

                $('#modalAddDataAccess').addClass('update');
                $('#btnSubmitDataAccess').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitDataAccess').attr('btn-process', 'update');
            } else {
                MaterialNotify(dataAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateUserAccess = function () {
        $.post("../UserManager/GetAllUserAccess", function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                var htmlString = '';

                if (userAccessObj.Status === 1) {
                    var accessObj = userAccessObj.Data;

                    $.each(accessObj, function (index) {
                        htmlString += '<option value="' + accessObj[index].USER_ACCESS_ID + '">' + accessObj[index].USERNAME + ' - ' + accessObj[index].ROLE_NAME + ' - ' + accessObj[index].DESCRIPTION + '</option>';
                    });

                    $('#cbxDataAccessUserAccess').append(htmlString);
                    $('#cbxDataAccessUserAccess').selectpicker('refresh');
                } else {
                    MaterialNotify(userAccessObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateDataHierarchyPicker = function () {
        $.post("../UserManager/GetAllDataHierarchy", function (response) {
            var dataHierarchyObj = JSON.parse(response);

            if (dataHierarchyObj.Status === 1) {
                var htmlString = '';

                if (dataHierarchyObj.Status === 1) {
                    var dataHierarchy = dataHierarchyObj.Data;

                    $.each(dataHierarchy, function (index) {
                        htmlString += '<option value="' + dataHierarchy[index].DATA_HRCHY_ID + '">' + dataHierarchy[index].DATA_HRCHY_ID + ' - ' + dataHierarchy[index].DESCRIPTION + '</option>';
                    });

                    $('#cbxDataAccessHierarchy').append(htmlString);
                    $('#cbxDataAccessHierarchy').selectpicker('refresh');
                } else {
                    MaterialNotify(dataHierarchyObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateModulePicker = function () {
        $.post("../UserManager/GetAllModules", function (response) {
            var modulesObj = JSON.parse(response);

            if (modulesObj.Status === 1) {
                var htmlString = '';

                if (modulesObj.Status === 1) {
                    var modules = modulesObj.Data;

                    $.each(modules, function (index) {
                        htmlString += '<option value="' + modules[index].MODULE_ID + '">' + modules[index].MODULE_NAME + '</option>';
                    });

                    $('#cbxDataAccessModule').append(htmlString);
                    $('#cbxDataAccessModule').selectpicker('refresh');
                } else {
                    MaterialNotify(modulesObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddDataAccess = function (dataAccessDetails) {
        $.post("../UserManager/AddDataAccess", { dataAccessModel: dataAccessDetails }, function (response) {
            var dataAccessObj = JSON.parse(response);

            if (dataAccessObj.Status === 1) {
                MaterialNotify(dataAccessObj.Message, 'success');
                accessFnc.ClearFields();
                accessTbl.row.add(dataAccessObj.Data[0]).draw();
                $('#modalAddDataAccess').modal('hide');
            } else {
                MaterialNotify(dataAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyDataAccess = function (dataAccessDetails) {
        $.post("../UserManager/ModifyDataAccess", { dataAccessModel: dataAccessDetails }, function (response) {
            var dataAccessObj = JSON.parse(response);

            if (dataAccessObj.Status === 1) {
                MaterialNotify(dataAccessObj.Message, 'success');
                accessFnc.ClearFields();
                accessTbl.row(accessRow).data(dataAccessObj.Data[0]).draw();
                $('#modalAddDataAccess').modal('hide');
            } else {
                MaterialNotify(dataAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#cbxDataAccessUserAccess').selectpicker('val', null);
        $('#cbxDataAccessHierarchy').selectpicker('val', null);
        $('#cbxDataAccessModule').selectpicker('val', null);

        $('#modalAddDataAccess').removeClass('update');
        $('#btnSubmitDataAccess').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitDataAccess').attr('btn-process', 'add');
    };
}

function ModuleAccess() {

    var modAccessFnc = this;
    var modAccessTbl = this;
    var modAccessRow = this;

    this.InitComponents = function () {        
        modAccessFnc.PopulateModuleAction();
        modAccessFnc.PopulateUserAccess();

        $('#tblModuleAccess').on('click', '.nav-container .edit-module-access', function (e) {
            _userManagerGlobal.moduleAccessId = $(this).data('module-access-id');
            modAccessFnc.GetModuleAccess($(this).data('module-access-id'));

            var tblObj = $(this);
            modAccessRow = modAccessTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#tblModuleAccess').on('click', '.nav-container .remove-module-access', function (e) {
            var moduleAccessId = $(this).data('access-id');

            MaterialConfirm({
                message: 'This action will delete the specified Module Access. Proceed?'
            }).done(function () {
                modAccessFnc.RemoveModuleAccess(moduleAccessId);
            });

            e.preventDefault();
        });

        $('#btnRoleAccessPageToggle').unbind().click(function () {
            if ($('#btnSubmitRoleAccess').attr('btn-process') === 'update') {
                modAccessFnc.ClearFields();
            }
        });

        $('#btnSubmitRoleAccess').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var moduleAccessDetails = {
                ModuleActionId: $('#cbxModuleAccess').val(),
                UserRoleAccessId: $('#cbxUserRoleAccess').val()
            };

            if (hasNull(moduleAccessDetails) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new role access?'
                    }).done(function () {
                        modAccessFnc.AddModuleAccess(moduleAccessDetails);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update role access details?'
                    }).done(function () {
                        moduleAccessDetails.Id = _userManagerGlobal.moduleAccessId;
                        modAccessFnc.ModifyModuleAccess(moduleAccessDetails);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllRoleAccess = function () {
        $.post("../UserManager/GetAllModuleAccess", function (response) {
            var roleAccessObj = JSON.parse(response);

            if (roleAccessObj.Status === 1) {
                modAccessTbl = $('#tblModuleAccess').DataTable({
                    info: false,
                    data: roleAccessObj.Data,
                    columns: [
                        {
                            data: {
                                module: 'MODULE_NAME',
                                action: 'ACTION_NAME'
                            },
                            render: function (data, type, full, meta) {
                                return data.MODULE_NAME + ' - ' + data.ACTION_NAME + '<br/><div class="nav-container"><a class="edit-module-access active" data-module-access-id="' + full.MODULE_ACCESS_ID + '" href="#" data-toggle="modal" data-target="#modalAddRoleAccess"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a> | <a class="remove-module-access active" data-access-id="' + full.MODULE_ACCESS_ID + '" href="#" ><i aria-hidden="true" class="fa fa-pencil"></i> Remove</a></div>';
                            }
                        },
                        {
                            data: {
                                username: 'USERNAME',
                                rolename: 'ROLE_NAME',
                                organization: 'DESCRIPTION'
                            },
                            render: function (data, type, full, meta) {
                                return data.USERNAME + ' - ' + data.ROLE_NAME + ' - ' + data.DESCRIPTION;
                            }
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(roleAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetModuleAccess = function (moduleAccessId) {
        $.post("../UserManager/GetModuleAccess", { moduleAccessId: moduleAccessId }, function (result) {
            var response = JSON.parse(result);

            if (response.Status === 1) {
                $('#cbxModuleAccess').selectpicker('val', response.Data[0].MODULE_ACTION_ID);
                $('#cbxUserRoleAccess').selectpicker('val', response.Data[0].USER_ACCESS_ID);

                $('#modalAddRoleAccess').addClass('update');
                $('#btnSubmitRoleAccess').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitRoleAccess').attr('btn-process', 'update');
            } else {
                MaterialNotify(response.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateUserAccess = function () {
        $.post("../UserManager/GetAllUserAccess", function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                var htmlString = '';

                if (userAccessObj.Status === 1) {
                    var accessObj = userAccessObj.Data;

                    $.each(accessObj, function (index) {
                        htmlString += '<option value="' + accessObj[index].USER_ACCESS_ID + '">' + accessObj[index].USERNAME + ' - ' + accessObj[index].ROLE_NAME + ' - ' + accessObj[index].DESCRIPTION + '</option>';
                    });

                    $('#cbxUserRoleAccess').append(htmlString);
                    $('#cbxUserRoleAccess').selectpicker('refresh');
                } else {
                    MaterialNotify(userAccessObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateModuleAction = function () {
        $.post("../UserManager/GetAllModuleActions", function (response) {
            var moduleAccessData = JSON.parse(response);

            if (moduleAccessData.Status === 1) {
                var htmlString = '';

                if (moduleAccessData.Status === 1) {
                    var moduleAccessObj = moduleAccessData.Data;

                    $.each(moduleAccessObj, function (index) {
                        htmlString += '<option value="' + moduleAccessObj[index].MODULE_ACTION_ID + '">' + moduleAccessObj[index].MODULE_NAME + ' - ' + moduleAccessObj[index].ACTION_NAME + '</option>';
                    });

                    $('#cbxModuleAccess').append(htmlString);
                    $('#cbxModuleAccess').selectpicker('refresh');
                } else {
                    MaterialNotify(moduleAccessData.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddModuleAccess = function (moduleAccessDetails) {
        $.post("../UserManager/AddModuleAccess", { moduleAccessModel: moduleAccessDetails }, function (response) {
            var roleAccessObj = JSON.parse(response);

            if (roleAccessObj.Status === 1) {
                MaterialNotify(roleAccessObj.Message, 'success');
                modAccessFnc.ClearFields();
                modAccessTbl.row.add(roleAccessObj.Data[0]).draw();
                $('#modalModuleAccess').modal('hide');
            } else {
                MaterialNotify(roleAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.RemoveModuleAccess = function (moduleAccessId) {
        $.post("../UserManager/RemoveModuleAccess", { moduleAccessId: moduleAccessId }, function (response) {
            var roleAccessObj = JSON.parse(response);

            if (roleAccessObj.Status === 1) {
                MaterialNotify(roleAccessObj.Message, 'success');
                modAccessFnc.ClearFields();
                $('#modalModuleAccess').modal('hide');
            } else {
                MaterialNotify(roleAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyModuleAccess = function (moduleAccessDetails) {
        $.post("../UserManager/ModifyModuleAccess", { modelModuleAccess: moduleAccessDetails }, function (response) {
            var roleAccessObj = JSON.parse(response);

            if (roleAccessObj.Status === 1) {
                MaterialNotify(roleAccessObj.Message, 'success');
                modAccessFnc.ClearFields();
                modAccessTbl.row(modAccessRow).data(roleAccessObj.Data[0]).draw();
                $('#modalModuleAccess').modal('hide');
            } else {
                MaterialNotify(roleAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#cbxModuleAccess').selectpicker('val', null);
        $('#cbxRoleAccessModAction').selectpicker('val', null);
        $('#cbxRoleAccessOrgHrchy').selectpicker('val', null);

        $('#modalModuleAccess').removeClass('update');
        $('#btnSubmitRoleAccess').html('<i aria-hidden="true" class="fa fa-plus"></i> Add');
        $('#btnSubmitRoleAccess').attr('btn-process', 'add');
    };
}

function UserAccess() {
    var usrAccessFnc = this;
    var usrAccessTbl = this;
    var usrAccessRow = this;

    this.InitComponents = function () {
        $('#tblUserAccess').on('click', '.nav-container .edit-user-access', function (e) {
            _userManagerGlobal.userAccessId = $(this).data('user-access-id');
            usrAccessFnc.GetUserAccess($(this).data('user-access-id'));

            var tblObj = $(this);
            usrAccessRow = usrAccessTbl.row(tblObj[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnUserAccessPageToggle').unbind().click(function () {
            if ($('#btnSubmitUserAccess').attr('btn-process') === 'update') {
                usrAccessFnc.ClearFields();
            }
        });  
        
        usrAccessFnc.PopulateRolePicker();
        usrAccessFnc.PopulateOrgHierarchyPicker();

        $('#btnSubmitUserAccess').unbind().click(function () {
            var process = $(this).attr('btn-process');

            var userAccessDetail = {
                UserId: $('#cbxUsers').val(),
                RoleId: $('#cbxUserAccessRole').val(),
                OrgId: $('#cbxUserAccessOrg').val()
            };

            var userId = $('#cbxUsers').val();


            if (!hasNull(userAccessDetail)) {

                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Grant user access?'
                    }).done(function () {
                        usrAccessFnc.AddUserAccess(userAccessDetail);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update user access details?'
                    }).done(function () {
                        userAccessDetail.UserAccessId = _userManagerGlobal.userAccessId;
                        usrAccessFnc.ModifyUserAccess(userAccessDetail);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });
    };

    this.GetAllUserAccess = function () {
        $.post("../UserManager/GetAllUserAccess", function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                usrAccessTbl = $('#tblUserAccess').DataTable({
                    info: false,
                    data: userAccessObj.Data,
                    columns: [
                        {
                            data: 'USERNAME',
                            render: function (data, type, full, meta) {
                                return '<span class="user-id">' + data + '</span><br/><div class="nav-container"><a class="edit-user-access active" data-user-access-id="' + full.USER_ACCESS_ID + '" href="#" data-toggle="modal" data-target="#modalAddUserAccess"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: {
                                first_name: 'FIRST_NAME',
                                middle_name: 'MIDDLE_NAME',
                                last_name: 'LAST_NAME',
                            },
                            render: function (data, type, full, meta) {
                                return data.LAST_NAME + ', ' + data.FIRST_NAME + ' ' + data.MIDDLE_NAME;
                            }
                        },
                        {
                            data: 'ROLE_NAME'
                        },
                        {
                            data: 'DESCRIPTION'
                        }
                    ],
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    paging: true,
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                    drawCallback: function () {

                    }
                });
            } else {
                MaterialNotify(userAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetUserAccess = function (userAccessId) {
        $.post("../UserManager/GetUserAccess", { userAccessId: userAccessId }, function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                $('#cbxUsers').selectpicker('val', userAccessObj.Data[0].USER_ID);
                $('#cbxUserAccessRole').selectpicker('val', userAccessObj.Data[0].ROLE_ID);
                $('#cbxUserAccessOrg').selectpicker('val', userAccessObj.Data[0].ORG_HRCHY_ID);

                $('#modalAddUserAccess').addClass('update');
                $('#btnSubmitUserAccess').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update');
                $('#btnSubmitUserAccess').attr('btn-process', 'update');
            } else {
                MaterialNotify(userAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllUsers = function () {
        $.post("../UserManager/GetAllUsers", function (response) {
            var htmlString = '';
            var usersObj = JSON.parse(response);

            if (usersObj.Status === 1) {
                var users = usersObj.Data;

                $.each(users, function (index) {
                    htmlString += '<option value="' + users[index].USER_ID + '" data-firstname="' + users[index].FIRST_NAME + '" data-middlename="' + users[index].MIDDLE_NAME + '" data-lastname="' + users[index].LAST_NAME + '">' + users[index].LAST_NAME + ', ' + users[index].FIRST_NAME + ' ' + users[index].MIDDLE_NAME + '</option>';
                });

                $('#cbxUsers').append(htmlString);
                $('#cbxUsers').selectpicker('refresh');
            } else {
                MaterialNotify(usersObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateRolePicker = function () {
        $.post("../UserManager/GetAllUserRoles", function (response) {
            var rolesObj = JSON.parse(response);

            if (rolesObj.Status === 1) {
                var htmlString = '';

                if (rolesObj.Status === 1) {
                    var roles = rolesObj.Data;

                    $.each(roles, function (index) {
                        htmlString += '<option value="' + roles[index].ROLE_ID + '">' + roles[index].ROLE_NAME + '</option>';
                    });

                    $('#cbxUserAccessRole').append(htmlString);
                    $('#cbxUserAccessRole').selectpicker('refresh');
                } else {
                    MaterialNotify(rolesObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.PopulateOrgHierarchyPicker = function () {
        $.post("../UserManager/GetAllOrgHierarchy", function (response) {
            var orgHierarchyObj = JSON.parse(response);

            if (orgHierarchyObj.Status === 1) {
                var htmlString = '';

                if (orgHierarchyObj.Status === 1) {
                    var orgHierarchy = orgHierarchyObj.Data;

                    $.each(orgHierarchy, function (index) {
                        htmlString += '<option value="' + orgHierarchy[index].ORG_HRCHY_ID + '">' + orgHierarchy[index].ORG_HRCHY_ID + ' - ' + orgHierarchy[index].HRCHY_IDN_DESC + '</option>';
                    });

                    $('#cbxUserAccessOrg').append(htmlString);
                    $('#cbxUserAccessOrg').selectpicker('refresh');
                } else {
                    MaterialNotify(orgHierarchyObj.Message, 'warning');
                }
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.AddUserAccess = function (userAccessDetail) {
        $.post("../UserManager/AddUserAccess", { modelUserAccess: userAccessDetail }, function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                MaterialNotify(userAccessObj.Message, 'success');
                usrAccessTbl.ClearFields();      
                usrAccessTbl.row.add(userAccessObj.Data[0]).draw();
                $('#modalAddUserAccess').modal('hide');
            } else {
                MaterialNotify(userAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyUserAccess = function (userAccessModel) {
        $.post("../UserManager/ModifyUserAccess", { modelUserAccess: userAccessModel }, function (response) {
            var userAccessObj = JSON.parse(response);

            if (userAccessObj.Status === 1) {
                MaterialNotify(userAccessObj.Message, 'success');
                usrAccessFnc.ClearFields();              
                usrAccessTbl.row(usrAccessRow).data(userAccessObj.Data[0]).draw();
                $('#modalAddUserAccess').modal('hide');
            } else {
                MaterialNotify(userAccessObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#cbxUsers').selectpicker('val', null);

        $('#modalAddUserAccess').removeClass('update');
        $('#btnSubmitUserAccess').html('<i aria-hidden="true" class="fa fa-check"></i> Grant');
        $('#btnSubmitUserAccess').attr('btn-process', 'add');
    };
}