﻿$(document).ready(function () {
    let componentMngr = new ComponentManager();
    componentMngr.InitComponentMngrObj();

    let _metaForm = new MetaForm();
    _metaForm.GetAllMetaForm();
    _metaForm.InitFormObjs();

    let _metaType = new MetaType();
    _metaType.GetAllMetaType();
    _metaType.InitTypeObjs();

    let _component = new Component();
    _component.GetAllComponent();
    _component.InitComponentObjs();

    let metadata = new Metadata();
    metadata.GetAllMetadata();
    metadata.InitMetadataObjs();
});

function ComponentManager() {
    this.InitComponentMngrObj = function () {
        $("[type='checkbox']").bootstrapSwitch();

        $('.selectpicker').selectpicker();   
           
    };
}

function MetaForm() {
    var _metaForm = this;
    var _metaFormId = null;
    var _tblMetaForm = null;
    var _rowMetaForm = null;

    this.InitFormObjs = function () {       
        $('#btnFormPageToggle').unbind().click(function () {
            if ($('#chkMetaFormStatus').prop('checked') === false) {
                $('#chkMetaFormStatus').click();
            }
        });

        $('#tblMetaForm').off('click').on('click', '.nav-container .edit-meta-form', function (e) { 
            
            _metaForm._metaFormId = $(this).data('metaform-id');
            _metaForm.GetMetaForm($(this).data('metaform-id'));
            
            //// set _rowMetaForm value
            var metaFormDtl = $(this);
            _metaForm._rowMetaForm = _metaForm._tblMetaForm.row(metaFormDtl[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnSubmitMetaForm').off('click').on('click', function () {
            var process = $(this).attr('btn-process');
            var status = null;

            if ($('#chkMetaFormStatus').prop('checked') === true) {
                status = '1';
            } else {
                status = '0';
            }

            var metaFormDtls = {
                MetaFormField: $('#txtMetaForm').val(),
                MetaFormTags: window.escape($('#txtMetaFormTags').val()),
                MetaFormValue: $('#txtMetaFormValue').val(),
                Status: status
            };

            if (hasNull(metaFormDtls) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new meta form?'
                    }).done(function () {
                        _metaForm.AddMetaForm(metaFormDtls);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update meta form details?'
                    }).done(function () {
                        metaFormDtls.MetaFormId = _metaForm._metaFormId;
                        _metaForm.ModifyMetaForm(metaFormDtls);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });

        $('#modalAddMetaForm').on('hidden.bs.modal', function () {
            _metaForm.ClearFields();
        });
    };

    this.AddMetaForm = function (metaFormDtls) {
        $.post("../ComponentManager/AddMetaForm", { metaForm: metaFormDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetaForm').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Add data to datatable
                _metaForm._tblMetaForm.row.add(responseObj.Data[0]).draw();
                _metaForm.ClearFields();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyMetaForm = function (metaFormDtls) {
        $.post("../ComponentManager/ModifyMetaForm", { metaForm: metaFormDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetaForm').modal('hide');
                MaterialNotify(responseObj.Message, 'success');
                
                //// Update row data from datatable
                _metaForm._tblMetaForm.row(_metaForm._rowMetaForm).data(responseObj.Data[0]).draw();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetActiveMetaForm = function () {
        $.post("../ComponentManager/GetActiveMetaForm", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                //// populate to dropdown
                var htmlString = '';

                responseData = responseObj.Data;

                $.each(responseData, function (index) {
                    htmlString += '<option value="' + responseData[index].META_FORM_ID + '">' + responseData[index].META_FORM + '</option>';
                });

                $('#cbxMetaForm').selectpicker('destroy');
                $('#cbxMetaForm').selectpicker();
                $('#cbxMetaForm').append(htmlString);
                $('#cbxMetaForm').selectpicker('refresh');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllMetaForm = function () {
        $.post("../ComponentManager/GetAllMetaForm", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metaForm._tblMetaForm = $('#tblMetaForm').DataTable({
                    info: false,
                    sort: false,
                    paging: true,
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    data: responseObj.Data,
                    columns: [
                        {
                            data: "META_FORM",
                            render: function (data, type, full, meta) {                                
                                return '<span class="meta-form" data-metaform-id="' + full.META_FORM_ID +'">' + data + '</span><br/><div class="nav-container"><a class="edit-meta-form active" data-metaform-id="' + full.META_FORM_ID + '" href="#" data-toggle="modal" data-target="#modalAddMetaForm"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: "META_UI_TAGS",
                            render: function (data, type, full, meta) {
                                return _metaForm.ConvertFormTags(data);
                            }
                        },
                        {
                            data: "META_FORM_VALUE"
                        },
                        {
                            data: "STATUS",
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Inactive</div>';
                                }
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },                   
                });
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetMetaForm = function (metaFormId) {
        $.post("../ComponentManager/GetMetaForm", { metaFormId: metaFormId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metaForm.ClearFields();
                
                $('#txtMetaForm').val(responseObj.Data[0].META_FORM);
                $('#txtMetaForm').attr('metaform-id', responseObj.Data[0].META_FORM_ID);
                $('#txtMetaFormTags').val(unescape(responseObj.Data[0].META_UI_TAGS));
                $('#txtMetaFormValue').val(responseObj.Data[0].META_FORM_VALUE);

                if (+responseObj.Data[0].STATUS === 1 && $('#chkMetaFormStatus').prop('checked') === false) {
                    $('#chkMetaFormStatus').click();
                } else if (+responseObj.Data[0].STATUS !== 1 && $('#chkMetaFormStatus').prop('checked') === true) {
                    $('#chkMetaFormStatus').click();
                }

                $('#modalAddMetaForm').addClass('update');
                $('#btnSubmitMetaForm').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update Meta Form');
                $('#btnSubmitMetaForm').attr('btn-process', 'update');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#txtMetaForm').attr('metaform-id', 0);
        $('#txtMetaForm').val('');
        $('#txtMetaFormTags').val('');
        $('#txtMetaFormValue').val('');

        if ($('#chkMetaFormStatus').prop('checked') === false) {
            $('#chkMetaFormStatus').click();
        }

        $('#modalAddMetaForm').removeClass('update');
        $('#btnSubmitMetaForm').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitMetaForm').attr('btn-process', 'add');
    };

    this.ConvertFormTags = function (formTag) {
        formTag = unescape(formTag);

        formTag = formTag.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
            return '&#' + i.charCodeAt(0) + ';';
        });       
        
        return formTag;
    };
}

function MetaType() {
    var _metaType = this;
    var _metaTypeId = null;
    var _tblMetaType = null;
    var _rowMetaType = null;

    this.InitTypeObjs = function () {
        $('#btnTypePageToggle').unbind().click(function () {
            if ($('#chkMetaTypeStatus').prop('checked') === false) {
                $('#chkMetaTypeStatus').click();
            }
        });

        $('#tblMetaType').off('click').on('click', '.nav-container .edit-meta-type', function (e) {

            _metaType._metaTypeId = $(this).data('metatype-id');
            _metaType.GetMetaType($(this).data('metatype-id'));

            //// set _rowMetaType value
            var metaTypeDtl = $(this);
            _metaType._rowMetaType = _metaType._tblMetaType.row(metaTypeDtl[0].offsetParent.parentElement);
            
            e.preventDefault();
        });

        $('#btnSubmitMetaType').off('click').on('click', function () {
            var process = $(this).attr('btn-process');
            var status = null;

            if ($('#chkMetaTypeStatus').prop('checked') === true) {
                status = '1';
            } else {
                status = '0';
            }

            var metaTypeDtls = {
                MetaTypeField: $('#txtMetaType').val(),
                Status: status
            };

            if (hasNull(metaTypeDtls) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new meta Type?'
                    }).done(function () {
                        _metaType.AddMetaType(metaTypeDtls);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update meta Type details?'
                    }).done(function () {
                        metaTypeDtls.MetaTypeId = _metaType._metaTypeId;
                        _metaType.ModifyMetaType(metaTypeDtls);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });

        $('#modalAddMetaType').on('hidden.bs.modal', function () {
            _metaType.ClearFields();
        });
    };

    this.AddMetaType = function (metaTypeDtls) {
        $.post("../ComponentManager/AddMetaType", { metaType: metaTypeDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetaType').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Add data to datatable
                _metaType._tblMetaType.row.add(responseObj.Data[0]).draw();
                _metaType.ClearFields();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyMetaType = function (metaTypeDtls) {
        $.post("../ComponentManager/ModifyMetaTypeStatus", { metaType: metaTypeDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetaType').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Update row data from datatable
                _metaType._tblMetaType.row(_metaType._rowMetaType).data(responseObj.Data[0]).draw();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetActiveMetaType = function () {
        $.post("../ComponentManager/GetActiveMetaType", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                //// populate to dropdown
                var htmlString = '';

                responseData = responseObj.Data;

                $.each(responseData, function (index) {
                    htmlString += '<option value="' + responseData[index].META_TYPE_ID + '">' + responseData[index].META_TYPE + '</option>';
                });

                $('#cbxMetaType').selectpicker('destroy');
                $('#cbxMetaType').selectpicker();
                $('#cbxMetaType').append(htmlString);
                $('#cbxMetaType').selectpicker('refresh');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllMetaType = function () {
        $.post("../ComponentManager/GetAllMetaType", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metaType._tblMetaType = $('#tblMetaType').DataTable({
                    info: false,
                    sort: false,
                    paging: true,
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    data: responseObj.Data,
                    columns: [
                        {
                            data: "META_TYPE",
                            render: function (data, type, full, meta) {
                                return '<span class="meta-Type" data-metatype-id="' + full.META_TYPE_ID + '">' + data + '</span><br/><div class="nav-container"><a class="edit-meta-type active" data-metatype-id="' + full.META_TYPE_ID + '" href="#" data-toggle="modal" data-target="#modalAddMetaType"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: "STATUS",
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Active</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Inactive</div>';
                                }
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    },
                });
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetMetaType = function (metaTypeId) {
        $.post("../ComponentManager/GetMetaType", { metaTypeId: metaTypeId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metaType.ClearFields();

                $('#txtMetaType').val(responseObj.Data[0].META_TYPE);
                $('#txtMetaType').attr('metatype-id', responseObj.Data[0].META_TYPE_ID);  
                $('#txtMetaType').prop('disabled', true);

                if (+responseObj.Data[0].STATUS === 1 && $('#chkMetaTypeStatus').prop('checked') === false) {
                    $('#chkMetaTypeStatus').click();
                } else if (+responseObj.Data[0].STATUS !== 1 && $('#chkMetaTypeStatus').prop('checked') === true) {
                    $('#chkMetaTypeStatus').click();
                }

                $('#modalAddMetaType').addClass('update');
                $('#btnSubmitMetaType').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update Meta Type');
                $('#btnSubmitMetaType').attr('btn-process', 'update');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#txtMetaType').attr('metatype-id', 0);
        $('#txtMetaType').val('');

        if ($('#chkMetaTypeStatus').prop('checked') === false) {
            $('#chkMetaTypeStatus').click();
        }

        $('#txtMetaType').prop('disabled', false);
        $('#modalAddMetaType').removeClass('update');
        $('#btnSubmitMetaType').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitMetaType').attr('btn-process', 'add');
    };
}

function Component() {
    var _component = this;
    var _componentId = null;
    var _tblComponent = null;
    var _rowComponent = null;

    this.InitComponentObjs = function () {
        $('#btnComponentPageToggle').unbind().click(function () {
            if ($('#chkComponentShown').prop('checked') === false) {
                $('#chkComponentShown').click();
            }
        });

        $('#tblComponent').off('click').on('click', '.nav-container .edit-component', function (e) {

            _component._componentId = $(this).data('component-id');
            _component.GetComponent($(this).data('component-id'));

            //// set _rowComponent value
            var componentDtl = $(this);
            _component._rowComponent = _component._tblComponent.row(componentDtl[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnSubmitComponent').off('click').on('click', function () {
            var process = $(this).attr('btn-process');
            var isShown = null;

            if ($('#chkComponentShown').prop('checked') === true) {
                isShown = '1';
            } else {
                isShown = '0';
            }

            var componentModel = {
                ComponentName: $('#txtComponent').val(),
                ComponentDesc: $('#txtComponentDesc').val(),
                ComponentIcon: $('#txtComponentIcon').val(),
                IsShown: isShown
            };

            if (hasNull(componentModel) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new meta component?'
                    }).done(function () {
                        _component.AddComponent(componentModel);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update meta component details?'
                    }).done(function () {
                        componentModel.ComponentId = _component._componentId;
                        _component.ModifyComponent(componentModel);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });

        $('#modalAddComponent').on('hidden.bs.modal', function () {
            _component.ClearFields();
        });
    };

    this.AddComponent = function (componentModel) {
        $.post("../ComponentManager/AddComponent", { componentModel: componentModel }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddComponent').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Add data to datatable
                _component._tblComponent.row.add(responseObj.Data[0]).draw();
                _component.ClearFields();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);

        });
    };

    this.ModifyComponent = function (componentModel) {
        $.post("../ComponentManager/ModifyComponent", { componentModel: componentModel }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddComponent').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Update row data from datatable
                _component._tblComponent.row(_component._rowComponent).data(responseObj.Data[0]).draw();                
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetShownComponent = function () {
        $.post("../ComponentManager/GetShownComponent", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                //// populate to dropdown
                var htmlString = '';

                responseData = responseObj.Data;

                $.each(responseData, function (index) {
                    htmlString += '<option value="' + responseData[index].COMPONENT_ID + '">' + responseData[index].COMPONENT_NAME + '</option>';
                });

                $('#cbxComponent').selectpicker('destroy');
                $('#cbxComponent').selectpicker();
                $('#cbxComponent').append(htmlString);
                $('#cbxComponent').selectpicker('refresh');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllComponent = function () {
        $.post("../ComponentManager/GetAllComponent", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _component._tblComponent = $('#tblComponent').DataTable({
                    info: false,
                    sort: false,
                    paging: true,
                    "lengthChange": false,
                    "pagingType": "simple_numbers",
                    data: responseObj.Data,
                    columns: [
                        {
                            data: "COMPONENT_NAME",
                            render: function (data, type, full, meta) {
                                return '<span class="component" data-component-id="' + full.COMPONENT_ID + '">' + data + '</span><br/><div class="nav-container"><a class="edit-component active" data-component-id="' + full.COMPONENT_ID + '" href="#" data-toggle="modal" data-target="#modalAddComponent"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: "COMPONENT_DESC"
                        },
                        {
                            data: "COMPONENT_ICON",
                            render: function (data, type, full, meta)  {
                                return '<span><i class="fa fa-fw ' + data + '"></i> &nbsp;|&nbsp;"' + data + '"</div>';
                            }
                        },
                        {
                            data: "IS_SHOWN",
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Shown</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Hidden</div>';
                                }
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    }
                });
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetComponent = function (componentId) {
        $.post("../ComponentManager/GetComponent", { componentId: componentId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _component.ClearFields();

                $('#txtComponent').val(responseObj.Data[0].COMPONENT_NAME);
                $('#txtComponent').attr('component-id', responseObj.Data[0].COMPONENT_ID);
                $('#txtComponent').prop('disabled', true);
                $('#txtComponentDesc').val(responseObj.Data[0].COMPONENT_DESC);
                $('#txtComponentIcon').val(responseObj.Data[0].COMPONENT_ICON);

                if (+responseObj.Data[0].IS_SHOWN === 1 && $('#chkComponentShown').prop('checked') === false) {
                    $('#chkComponentShown').click();
                } else if (+responseObj.Data[0].IS_SHOWN !== 1 && $('#chkComponentShown').prop('checked') === true) {
                    $('#chkComponentShown').click();
                }

                $('#modalAddComponent').addClass('update');
                $('#btnSubmitComponent').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update Component');
                $('#btnSubmitComponent').attr('btn-process', 'update');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#txtComponent').attr('component-id', 0);
        $('#txtComponent').val('');
        $('#txtComponentDesc').val('');
        $('#txtComponentIcon').val('');        

        if ($('#chkComponentShown').prop('checked') === false) {
            $('#chkComponentShown').click();
        }

        $('#txtComponent').prop('disabled', false);
        $('#modalAddComponent').removeClass('update');
        $('#btnSubmitComponent').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitComponent').attr('btn-process', 'add');
    };    
}

function Metadata() {
    var _metadata = this;
    var _metadataId = null;
    var _tblMetadata = null;
    var _rowMetadata = null;

    this.InitMetadataObjs = function () {
        $('#btndataPageToggle').unbind().click(function () {
            if ($('#chkMetadataStatus').prop('checked') === false) {
                $('#chkMetadataStatus').click();
            }
        });

        $('#tblMetadata').off('click').on('click', '.nav-container .edit-meta-data', function (e) {

            _metadata._metadataId = $(this).data('metadata-id');
            _metadata.GetMetadata($(this).data('metadata-id'));

            //// set _rowMetadata value
            var metadataDtl = $(this);
            _metadata._rowMetadata = _metadata._tblMetadata.row(metadataDtl[0].offsetParent.parentElement);

            e.preventDefault();
        });

        $('#btnSubmitMetadata').off('click').on('click', function () {
            var process = $(this).attr('btn-process');
            var isShown = null;

            if ($('#chkMetadataShown').prop('checked') === true) {
                isShown = '1';
            } else {
                isShown = '0';
            }

            var metadataDtls = {
                MetaDataField: $('#txtMetadata').val(),
                MetaDataLength: $('#txtMetadataLength').val(),
                MetaFormId: $('#cbxMetaForm').val(),
                MetaTypeId: $('#cbxMetaType').val(),
                ComponentId: $('#cbxComponent').val(),
                IsShown: isShown
            };

            if (hasNull(metadataDtls) !== true) {
                if (process === 'add') {
                    MaterialConfirm({
                        message: 'Add new meta form?'
                    }).done(function () {
                        _metadata.AddMetadata(metadataDtls);
                    });
                } else {
                    MaterialConfirm({
                        message: 'Update meta form details?'
                    }).done(function () {
                        metadataDtls.MetaDataId = _metadata._metadataId;
                        _metadata.ModifyMetadata(metadataDtls);
                    });
                }
            } else {
                MaterialNotify("Please fill in all fields", 'warning');
            }
        });

        $('#modalAddMetadata').on('hidden.bs.modal', function () {
            _metadata.ClearFields();
        });

        var metaForm = new MetaForm();
        metaForm.GetActiveMetaForm();

        var metaType = new MetaType();
        metaType.GetActiveMetaType();

        var component = new Component();
        component.GetShownComponent();
    };

    this.AddMetadata = function (metadataDtls) {
        $.post("../ComponentManager/AddMetadata", { metadata: metadataDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetadata').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Add data to datatable
                _metadata._tblMetadata.row.add(responseObj.Data[0]).draw();
                _metadata.ClearFields();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ModifyMetadata = function (metadataDtls) {
        $.post("../ComponentManager/ModifyMetadata", { metadata: metadataDtls }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                $('#modalAddMetadata').modal('hide');
                MaterialNotify(responseObj.Message, 'success');

                //// Update row data from datatable
                _metadata._tblMetadata.row(_metadata._rowMetadata).data(responseObj.Data[0]).draw();
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetAllMetadata = function () {
        $.post("../ComponentManager/GetAllMetadata", {}, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metadata._tblMetadata = $('#tblMetadata').DataTable({
                    info: false,
                    sort: false,
                    paging: true,
                    lengthChange: false,
                    pagingType: "simple_numbers",
                    data: responseObj.Data,
                    columns: [
                        {
                            data: "METADATA_FIELD",
                            render: function (data, type, full, meta) {
                                return '<span class="meta-data" data-metadata-id="' + full.METADATA_ID + '">' + data + '</span><br/><div class="nav-container"><a class="edit-meta-data active" data-metadata-id="' + full.METADATA_ID + '" href="#" data-toggle="modal" data-target="#modalAddMetadata"><i aria-hidden="true" class="fa fa-pencil"></i> Edit</a></div>';
                            }
                        },
                        {
                            data: "META_TYPE",
                            render: function (data, type, full, meta) {
                                return '<span>' + data + '&nbsp;(' + full.METADATA_LENGTH + ')</div>';
                            }
                        },
                        {
                            data: "META_FORM"
                        },
                        {
                            data: "COMPONENT_NAME",
                            render: function (data, type, full, meta) {
                                return '<span><i class="fa fa-fw ' + full.COMPONENT_ICON + '"></i>&nbsp;|&nbsp;' + data + '</div>';
                            }
                        },
                        {
                            data: "IS_SHOWN",
                            render: function (data, type, full, meta) {
                                if (+data === 1) {
                                    return '<div class="user-status badge badge-pill badge-success">Shown</div>';
                                } else {
                                    return '<div class="user-status badge badge-pill badge-danger">Hidden</div>';
                                }
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                                }).join('');

                                return data ?
                                    $('<table/>').append(data) :
                                    false;
                            }
                        }
                    }
                });
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.GetMetadata = function (metadataId) {
        $.post("../ComponentManager/GetMetadata", { metadataId: metadataId }, function (response) {
            var responseObj = JSON.parse(response);

            if (responseObj.Status === 1) {
                _metadata.ClearFields();

                $('#txtMetadata').val(responseObj.Data[0].METADATA_FIELD);
                $('#txtMetadata').attr('metadata-id', responseObj.Data[0].METADATA_ID);
                $('#txtMetadataLength').val(responseObj.Data[0].METADATA_LENGTH);
                $('#cbxComponent').selectpicker('val', responseObj.Data[0].COMPONENT_ID);
                $('#cbxMetaForm').selectpicker('val', responseObj.Data[0].META_FORM_ID);
                $('#cbxMetaType').selectpicker('val', responseObj.Data[0].META_TYPE_ID);

                if (+responseObj.Data[0].IS_SHOWN === 1 && $('#chkMetadataShown').prop('checked') === false) {
                    $('#chkMetadataShown').click();
                } else if (+responseObj.Data[0].IS_SHOWN !== 1 && $('#chkMetadataShown').prop('checked') === true) {
                    $('#chkMetadataShown').click();
                }

                $('#modalAddMetadata').addClass('update');
                $('#btnSubmitMetadata').html('<i aria-hidden="true" class="fa fa-pencil"></i> Update Meta data');
                $('#btnSubmitMetadata').attr('btn-process', 'update');
            } else {
                MaterialNotify(responseObj.Message, 'warning');
            }
        }).fail(function (ex) {
            MaterialNotify(ex.statusText, 'danger', true);
        });
    };

    this.ClearFields = function () {
        $('#txtMetadata').attr('metadata-id', 0);
        $('#txtMetadata').val('');
        $('#txtMetadataLength').val('');
        
        $('#cbxComponent').selectpicker('val', null);
        $('#cbxMetaForm').selectpicker('val', null);
        $('#cbxMetaType').selectpicker('val', null);

        if ($('#chkMetadataShown').prop('checked') === false) {
            $('#chkMetadataShown').click();
        }

        $('#modalAddMetadata').removeClass('update');
        $('#btnSubmitMetadata').html('<i aria-hidden="true" class="fa fa-pencil"></i> Add');
        $('#btnSubmitMetadata').attr('btn-process', 'add');
    };
}