/* ========================================================================
 * Validation JS
 * ========================================================================
 * This JS is dedicated for validation purposes like inputs checking, 
 * datatable manipulation and manipulation loading pop-ups
 * 
 * Compiled By: Solem Khan
 * ========================================================================*/

$(document).ready(function () {
    $('.numbers-only').keypress(function (e) {
        var verified = (e.which === 8 || e.which === undefined || e.which === 0) ? null : String.fromCharCode(e.which).match(/[^0-9\.]/);
        if (verified) {
            e.preventDefault();
        }
    });

    $('.alpha-only').keypress(function (e) {
        var verified = (e.which === 8 || e.which === undefined || e.which === 0) ? null : String.fromCharCode(e.which).match(/[^A-Za-z_\s]/);
        if (verified) { e.preventDefault(); }
    });

    let keyPress = new KeyPressRestrictions();
    keyPress.InitKeyRestrictObj();
});

function KeyPressRestrictions() {
    /// <summary>
    /// This method is in beta version and was partially check.
    /// Please feel free to edit the follow code segment in this function for your benefits.
    /// 
    /// Number = <input class="number" type="text"> 
    /// Alpha = <input class="alpha" type="text">  
    /// Decimal = <input class="decimal" type="text" data-decimal="4">   
    /// AlphaNum = <input class="alphanum" type="text">    
    /// 
    /// Author: Solem Abdusalam, March 2019
    /// 
    /// Changesets Log Details:
    /// [Name]              [Date]          [Changeset-Comment]
    /// - Solem Khan        March 2019      Created.
    /// -
    /// -
    /// </summary>
    /// <param name="ele">Element to be check</param>
    /// <param name="control">Restrictions to be implemented</param>

    var retrict = this;

    this.InitKeyRestrictObj = function () {
        var number = document.getElementsByClassName("number");
        retrict.KeyPressRestrict(number, /^\d+$/g);

        var alpha = document.getElementsByClassName("alpha");
        retrict.KeyPressRestrict(alpha, /^([a-zA-z\s]+)$/g);
        
        var decimal = document.getElementsByClassName("decimal");
        if (decimal.length > 0) {
            var count = decimal[0].getAttribute('data-decimal');
            var string = "^\\d+(\\.\\d{0," + count + "})?$";
            retrict.KeyPressRestrict(decimal, eval("/" + string + "/g"));
        }
        var alphanum = document.getElementsByClassName("alphanum");
        retrict.KeyPressRestrict(alphanum, /^[\w0-9]+(\s?[\w0-9]+)*\s?$/g);
    };

    this.KeyPressRestrict = function (ele, control) {
        for (var i = 0; i < ele.length; i++) {
            ele[i].addEventListener('keypress', function (e) {
                let keyValues;

                if (ele[0].textLength < 1) {
                    keyValues = String.fromCharCode(e.which);
                }
                else {
                    keyValues = ele[0].value + String.fromCharCode(e.which);
                }

                if (!keyValues.match(control)) {
                    e.preventDefault();
                }
            });
        }
    };    
}

function ToggleLoading() {
    if ($('.loader-bg, .loader-wrapper').length === 0) {
        $('body').prepend('<div class="loader-bg"></div><div class="loader-wrapper"><div class="loader"></div ></div >');
        setTimeout(function () {
            $('.loader-wrapper, .loader-bg').addClass('show');
        }, 10);

        return 'Loading...';
    } else {
        $('.loader-wrapper, .loader-bg').removeClass('show');
        setTimeout(function () {
            $('.loader-wrapper, .loader-bg').remove();
        }, 200);

        return 'Loading has ended';
    }
}

function OpenLoading() {
    $('body').prepend('<div class="loader-bg"></div><div class="loader-wrapper"><div class="loader"></div ></div >');
    setTimeout(function () {
        $('.loader-wrapper, .loader-bg').addClass('show');
    }, 10);

    return 'Loading...';
}

function CloseLoading() {
    $('.loader-wrapper, .loader-bg').removeClass('show');
    setTimeout(function () {
        $('.loader-wrapper, .loader-bg').remove();
    }, 200);

    return 'Loading has ended';
}

function hasNull(target) {
    /// <summary>
    /// Checking the object null value(s)
    /// </summary>
    /// <param name="target" type="object">Json Object Details</param>
    /// <returns type="boolean">Returns True or False</returns>

    for (var member in target) {
        if (target[member] === '' || target[member] === undefined || target[member] === null) {
            return true;
        }
    }
    return false;
}

function AddSingleDataRow(table, data) {
    table.row.add(data[0]).draw('false');
}

function AddMultipleDataRow(table, datas) {
    $.each(datas, function (index, value) {
        table.row.add(value).draw('false');
    });
}

function ModifySingleDataRow(closestTr, datatable, data) {
    console.log(datatable.row(closestTr).data());
    datatable.row(closestTr).data(data).draw(false);
}

function HtmlEntities(str) {
    if (IsEmpty(str) || str == 'null') {
        return '';
    } else
        return String(str).replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/!/g, '&#33;');
}

function GetOracleError(str) {
    var strStart = str.indexOf('[');
    var strEnd = str.indexOf(']');
    if (strStart > 0) {
        return str.substring(strStart + 1, strEnd);
    } else {
        return str;
    }
}

function IsEmpty(value) {
    if (value == null || value.toString().toLowerCase() == 'null' || value == '' || value.toString().toLowerCase() == undefined) {
        return true;
    } else {
        return false;
    }
}

// Plugins
(function ($) {
    // On Enter function
    $.fn.onEnter = function (func) {
        this.bind('keypress', function (e) {
            if (e.keyCode == 13) func.apply(this, [e]);
        });
        return this;
    }

    // Dynamic Modal
    $.fn.DynamicModal = function (options) {

        // Default variable values
        var settings = $.extend({
            title: 'Dynamic Modal',
            size: 'medium',
            content: 'Sample Content',
            okButtonId: ''
        }, options);


        /* Generates the html content using the value of settings.content
        ------------------------------------------------------------*/

        /*----------------------------------------------------------*/


        // Adds the corresponding attributes into the element
        $(this).attr({
            'role': 'dialog',
            'tabindex': '-1',
            'class': 'modal fade'
        });


        /* Inserts the dynamic html content into the main element
        -------------------------------------------------------*/
        if (settings.size === 'small') {
            var htmlString = '<div class="modal-dialog  modal-sm" role="document">';
        } else if (settings.size === 'medium') {
            var htmlString = '<div class="modal-dialog  modal-md" role="document">';
        } else if (settings.size === 'large') {
            var htmlString = '<div class="modal-dialog  modal-lg" role="document">';
        } else {
            var htmlString = '<div class="modal-dialog  modal-md" role="document">';
        }

        htmlString += '<div class="modal-content">';
        htmlString += '<div class="modal-header">';
        htmlString += '<h5 class="modal-title" id="exampleModalLabel">' + settings.title + '</h5>';
        htmlString += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        htmlString += '<span aria-hidden="true">&times;</span>';
        htmlString += '</button>';
        htmlString += '</div>';
        htmlString += '<div class="modal-body">' + settings.content;
        htmlString += '</div>';
        htmlString += '<div class="modal-footer">';
        htmlString += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
        htmlString += '<button type="button" id="' + settings.okButtonId + '" class="btn btn-primary">Save changes</button>';
        htmlString += '</div>';
        htmlString += '</div>';
        htmlString += '</div>';
        htmlString += '</div>';

        $(this).html(htmlString);
        /*-----------------------------------------------------*/

        return this;
    };
})(jQuery);