﻿/* ========================================================================
 * General JS
 * ======================================================================== 
 * This JS is dedicated for functions that is called more than 1x
 * Place all your Callbacks here as well.
 * ========================================================================
 */

$(document).ready(function () {
    $('.sidebar-menu[data-page-title="' + document.title + '"]').addClass('active');
    $('.sidebar-submenu[data-page-title="' + document.title + '"]').addClass('active');

    $('.list-group .list-group-item').unbind().click(function () {
        $(this).closest('.list-group').find('.list-group-item').removeClass('active');
        $(this).addClass('active');
    });

    $('#btnLogout').click(function () {
        window.location = '../Login/Logout';
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
});

function SetCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function GetCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function CheckCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}

var DeleteCookie = function (cname) {
    document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};