﻿namespace SCHOOL.MANAGEMENT.Portal
{
    using System;
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css"
                ).IncludeDirectory("~/Content/vendor", "*.css", true
                ).Include("~/Content/css/main.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Content/vendor/jquery/jquery*",
                "~/Content/vendor/popper/popper.min.js",
                "~/Content/vendor/bootstrap/js/bootstrap.min.js",
                "~/Content/vendor/bootstrap-select/js/bootstrap-select.js",
                "~/Content/vendor/datatable/js/jquery.dataTables.min.js",
                "~/Content/vendor/datatable/js/dataTables.bootstrap4.min.js",
                "~/Content/vendor/datatable/js/dataTables.responsive.min.js",
                "~/Content/vendor/datatable/js/dataTable.jumpToPage.js",
                "~/Content/vendor/sweetalert2/js/sweetalert2.all.min.js",
                "~/Content/vendor/moment/moment.js",
                "~/Content/vendor/material-pikaday/js/pikaday.js",
                "~/Content/vendor/keyboard-delimeter/js/keyboard-delimiter.min.js",
                "~/Content/vendor/material-timepicker/js/mdDateTimePicker.min.js",
                "~/Content/vendor/validation.js",
                "~/Content/vendor/general.js",
                "~/Content/vendor/animations.js",
                "~/Content/vendor/bootstrap-switch/js/bootstrap-switch.min.js",
                "~/Content/vendor/kcc-material-notify/js/kccmaterialnotify.min.js",
                "~/Content/vendor/fontawesome-iconpicker/js/fontawesome-iconpicker.min.js",
                "~/Content/vendor/jquery-scrollbar/js/jquery.scrollbar.js",
                "~/Content/vendor/typeahead/js/typeahead.bundle.js",
                "~/Content/vendor/cleave/cleave.js",
                "~/Content/vendor/mdbootstrap/js/mdb.min.js"
                ));

            BundleTable.EnableOptimizations = true;
        }
    }
}