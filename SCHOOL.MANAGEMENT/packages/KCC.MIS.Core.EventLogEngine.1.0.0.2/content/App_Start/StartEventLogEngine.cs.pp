﻿using System;

[assembly: WebActivatorEx.PreApplicationStartMethod(
    typeof($rootnamespace$.App_Start.StartEventLogEngine), "PreStart")]

namespace $rootnamespace$.App_Start {
    public static class StartEventLogEngine {
        public static void PreStart() {
            AppDomain.CurrentDomain.FirstChanceException += (s, eventArgs) =>
            {
                MIS.Core.EventLogEngine.Event.Log(s, eventArgs, System.Diagnostics.EventLogEntryType.Error);
            };

            AppDomain.CurrentDomain.UnhandledException += (s, eventArgs) =>
            {
                MIS.Core.EventLogEngine.Event.Log(s, eventArgs, System.Diagnostics.EventLogEntryType.Error);
            };
        }
    }
}